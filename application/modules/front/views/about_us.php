<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>About US</span></a>
		</div>
	</section>
<section class="word-aboutus">
			<div class="container">
				<h2>A FEW WORDS ABOUT US</h2>
				<p>We are professionals in the laundry and dry cleaning business, which means we always stay up to date on the latest technologies, cleaning methods, and solutions for dealing with stains or delicate fabrics. Plus, we maintain the highest standards of business integrity by following local and national regulations and environmental safety rules.</p>
			</div>
		</section>

		<section class="why-choose-us">
			<div class="container">
				<h2>WHY CHOOSE US</h2>
				<div class="row">
					<div class="col-md-4">
						<div class="type-content first-fade animate fadeLeft">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>towel.png">
							</div>
							<div class="type-desc">
								<a href="">Persionalized Experience</a>
								<p>We take utmost care of your clothes, segregating based on the cloth type and giving you instant clothes to make a statement.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="type-content">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>pen.png">
							</div>
							<div class="type-desc">
								<a href="">Affordable Pricing</a>
								<p>Prices that suits your pocket is one of our USP. An option of choosing between 2 types of pricing is available.p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="type-content last-fade animate fadeRight">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>gymnast.png">
							</div>
							<div class="type-desc">
								<a href="">Convenience</a>
								<p>With just a tap of a button, your laundry gets done, giving your leisure time to spend with family and friends.</p>
							</div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="type-content first-fade animate fadeLeft">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>towel.png">
							</div>
							<div class="type-desc">
								<a href="">Quality</a>
								<p>Forgot to wash your clothes for interview/business meeting, never mind. With our super express delivery, we would get your laundry done in less than 8 hours.p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="type-content">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>pen.png">
							</div>
							<div class="type-desc">
								<a href="">Express Delivery</a>
								<p>Forgot to wash your clothes for interview/business meeting, never mind. With our super express delivery, we would get your laundry done in less than 8 hours.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="type-content last-fade animate fadeRight">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>gymnast.png">
							</div>
							<div class="type-desc">
								<a href="">Instant Order Update</a>
								<p>Regular updates of your order, to help you keep a track of your laundry and plan accordingly.p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

		<section class="all-counter">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="content">
							<div class="icon">
								<i class="fas fa-tshirt coun-icon"></i>
							</div>
							<div class="title">
								<span class="counter">50000</span>+
							</div>
							<div class="description">Shirts Washed</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="content">
							<div class="icon">
								<i class="fas fa-tshirt coun-icon"></i>
							</div>
							<div class="title">
								<span class="counter">50</span>
							</div>
							<div class="description">Washing Machines Works</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="content">
							<div class="icon">
								<i class="fas fa-tshirt coun-icon"></i>
							</div>
							<div class="title">
								<span class="counter">10000</span>+
							</div>
							<div class="description">Dry Cleaned Items</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="content">
							<div class="icon">
								<i class="fas fa-tshirt coun-icon"></i>
							</div>
							<div class="title">
								<span class="counter">1000</span>+
							</div>
							<div class="description">Happy Customers</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	