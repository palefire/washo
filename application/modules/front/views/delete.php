


	<header>
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="header-top-item">
							<span class="fa fa-phone">&nbsp;&nbsp;&nbsp;Phone: +918348465124
							</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="fa fa-envelope-o">&nbsp;&nbsp;&nbsp;Email:
								<a href="#">washo@example.com</a>
							</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="top-header-social-item pull-right">
							<a href="" target="_blank" title="facebook"><span class="fa fa-facebook"></span></a>
							<a href="" target="_blank" title="twitter"><span class="fa fa-twitter"></span></a>
							<a href="" target="_blank" title="instagram"><span class="fa fa-instagram"></span></a>
							<a href="" target="_blank" title="pinterest"><span class="fa fa-pinterest"></span></a>
							<?php if( isset($this->session->userdata['user_id']) ){ ?>
								<a href="<?php echo base_url('front/my_basket')  ?>" class="btn btn-primary" style="padding:2px 12px"><i class="fa fa-shopping-basket"></i>&nbsp;Basket <span class="no-basket"><?php echo $this->data['no_of_basket'];  ?></span></a>
								<a href="<?php echo base_url('front/account/logout')  ?>" class="btn btn-danger" style="padding:2px 12px"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
							<?php }else{?>
							<a href="<?php echo base_url('front/account')  ?>" class="btn btn-primary" style="padding:5px 12px"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Login</a>
							<?php  } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-main">
			<div class="container">
				<div class="logo-part float-left">
					<a href="<?php echo base_url(); ?>">WASHO</a>
				</div>

				<nav class="float-left">
					<?php if( isset($this->session->userdata['user_name']) AND isset($this->session->userdata['user_slug']) ){ ?>
					<a href="#" style="margin-left:80px;">Welcome! <?php echo $this->session->userdata['user_name']  ?></a>
					<?php  } ?>
				</nav>
				<nav class="float-right">
					<a href="<?php echo base_url(); ?>" class="active">HOME</a>
					<a href="#">ABOUT</a>
					<a href="<?php echo base_url('front/service');?>">SERVICES</a>
					<a href="<?php echo base_url('front/franchise');?>">FRANCHISE</a>
					<a href="#">CONTACT</a>
					<!-- <a href="#">OFFERS</a> -->
					<?php if( isset($this->session->userdata['user_id']) AND isset($this->session->userdata['user_slug']) ){ ?>
					<a href="<?php echo base_url('front/account/my_orders/').$this->session->userdata['user_slug']  ?>">MY ODERS</a>
					<?php  } ?>
					
				</nav>
			</div>
		</div>
	</header>



