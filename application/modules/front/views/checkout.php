<section class="breadcump">
        <div class="container">
            <a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href="<?php echo site_url('/front/my_basket/')  ?>"><span>My Basket</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>Delivery Address</span></a>
        </div>
</section>
<section class="pf-cart-all-quantity">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<?php 
				
				 if($delivery_address ){ 

				?>
				<?php echo form_open_multipart('front/update_delivery_address', array('class'=>'') ); ?>
					<div class="card">
						<div class="card-header">
							<h5 class="card-title"><b>Use Prevoius Delivery Address or Update</b></h5>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="edit_name" placeholder="Name" class="form-control" value="<?php echo $delivery_address[0]['name']; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="edit_ph_no" placeholder="10-digit mobile number" class="form-control" value="<?php echo $delivery_address[0]['phone']; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="number" name="edit_pin" placeholder="Pincode" class="form-control" value="<?php echo $delivery_address[0]['pin']; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="edit_locality" placeholder="locality" class="form-control" value="<?php echo $delivery_address[0]['locality']; ?>">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control" rows="4" placeholder="Address (Area and Street)" name="edit_address" ><?php echo $delivery_address[0]['address']; ?></textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="edit_district" placeholder="City/District/Town" class="form-control" value="<?php echo $delivery_address[0]['district']; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<select class="form-control" name="edit_state" required>
											<option value="0" <?php if( $delivery_address[0]['state'] == "0" ){ echo 'selected'; } ?>>-Select State-</option>
											<option value="Andhra Pradesh" <?php if( $delivery_address[0]['state'] == "Andhra Pradesh" ){ echo 'selected'; } ?>>Andhra Pradesh</option>
											<option value="Andaman and Nicobar Islands" <?php if( $delivery_address[0]['state'] == "Andaman and Nicobar Islands" ){ echo 'selected'; } ?> >Andaman and Nicobar Islands</option>
											<option value="Arunachal Pradesh" <?php if( $delivery_address[0]['state'] == "Arunachal Pradesh" ){ echo 'selected'; } ?> >Arunachal Pradesh</option>
											<option value="Assam" <?php if( $delivery_address[0]['state'] == "Assam" ){ echo 'selected'; } ?> >Assam</option>
											<option value="Bihar" <?php if( $delivery_address[0]['state'] == "Bihar" ){ echo 'selected'; } ?>>Bihar</option>
											<option value="Chandigarh" <?php if( $delivery_address[0]['state'] == "Chandigarh" ){ echo 'selected'; } ?> >Chandigarh</option>
											<option value="Chhattisgarh" <?php if( $delivery_address[0]['state'] == "Chhattisgarh" ){ echo 'selected'; } ?> >Chhattisgarh</option>
											<option value="Dadar and Nagar Haveli" <?php if( $delivery_address[0]['state'] == "Dadar and Nagar Haveli" ){ echo 'selected'; } ?> >Dadar and Nagar Haveli</option>
											<option value="Daman and Diu" <?php if( $delivery_address[0]['state'] == "Daman and Diu" ){ echo 'selected'; } ?> >Daman and Diu</option>
											<option value="Delhi" <?php if( $delivery_address[0]['state'] == "Delhi" ){ echo 'selected'; } ?> >Delhi</option>
											<option value="Lakshadweep" <?php if( $delivery_address[0]['state'] == "Lakshadweep" ){ echo 'selected'; } ?> >Lakshadweep</option>
											<option value="Puducherry" <?php if( $delivery_address[0]['state'] == "Puducherry" ){ echo 'selected'; } ?> >Puducherry</option>
											<option value="Goa" <?php if( $delivery_address[0]['state'] == "Goa" ){ echo 'selected'; } ?> >Goa</option>
											<option value="Gujarat" <?php if( $delivery_address[0]['state'] == "Gujarat" ){ echo 'selected'; } ?> >Gujarat</option>
											<option value="Haryana" <?php if( $delivery_address[0]['state'] == "Haryana" ){ echo 'selected'; } ?> >Haryana</option>
											<option value="Himachal Pradesh" <?php if( $delivery_address[0]['state'] == "Himachal Pradesh" ){ echo 'selected'; } ?> >Himachal Pradesh</option>
											<option value="Jammu and Kashmir" <?php if( $delivery_address[0]['state'] == "Jammu and Kashmir" ){ echo 'selected'; } ?> >Jammu and Kashmir</option>
											<option value="Jharkhand" <?php if( $delivery_address[0]['state'] == "Jharkhand" ){ echo 'selected'; } ?> >Jharkhand</option>
											<option value="Karnataka" <?php if( $delivery_address[0]['state'] == "Karnataka" ){ echo 'selected'; } ?> >Karnataka</option>
											<option value="Kerala" <?php if( $delivery_address[0]['state'] == "Kerala" ){ echo 'selected'; } ?> >Kerala</option>
											<option value="Madhya Pradesh" <?php if( $delivery_address[0]['state'] == "Madhya Pradesh" ){ echo 'selected'; } ?> >Madhya Pradesh</option>
											<option value="Maharashtra" <?php if( $delivery_address[0]['state'] == "Maharashtra" ){ echo 'selected'; } ?> >Maharashtra</option>
											<option value="Manipur" <?php if( $delivery_address[0]['state'] == "Manipur" ){ echo 'selected'; } ?> >Manipur</option>
											<option value="Meghalaya" <?php if( $delivery_address[0]['state'] == "Meghalaya" ){ echo 'selected'; } ?> >Meghalaya</option>
											<option value="Mizoram" <?php if( $delivery_address[0]['state'] == "Mizoram" ){ echo 'selected'; } ?> >Mizoram</option>
											<option value="Nagaland" <?php if( $delivery_address[0]['state'] == "Nagaland" ){ echo 'selected'; } ?> >Nagaland</option>
											<option value="Odisha" <?php if( $delivery_address[0]['state'] == "Odisha" ){ echo 'selected'; } ?> >Odisha</option>
											<option value="Punjab" <?php if( $delivery_address[0]['state'] == "Punjab" ){ echo 'selected'; } ?> >Punjab</option>
											<option value="Rajasthan" <?php if( $delivery_address[0]['state'] == "Rajasthan" ){ echo 'selected'; } ?> >Rajasthan</option>
											<option value="Sikkim" <?php if( $delivery_address[0]['state'] == "Sikkim" ){ echo 'selected'; } ?> >Sikkim</option>
											<option value="Tamil Nadu" <?php if( $delivery_address[0]['state'] == "Tamil Nadu" ){ echo 'selected'; } ?> >Tamil Nadu</option>
											<option value="Telangana" <?php if( $delivery_address[0]['state'] == "Telangana" ){ echo 'selected'; } ?> >Telangana</option>
											<option value="Tripura" <?php if( $delivery_address[0]['state'] == "Tripura" ){ echo 'selected'; } ?> >Tripura</option>
											<option value="Uttar Pradesh" <?php if( $delivery_address[0]['state'] == "Uttar Pradesh" ){ echo 'selected'; } ?> >Uttar Pradesh</option>
											<option value="Uttarakhand" <?php if( $delivery_address[0]['state'] == "Uttarakhand" ){ echo 'selected'; } ?> >Uttarakhand</option>
											<option value="West Bengal" <?php if( $delivery_address[0]['state'] == "West Bengal" ){ echo 'selected'; } ?> >West Bengal</option>

										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="edit_landmark" placeholder="landmark" class="form-control" value="<?php echo $delivery_address[0]['landmark']; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="alt_phone" placeholder="Alternate Phone (Optional)" class="form-control" value="<?php echo $delivery_address[0]['alt_phone']; ?>">
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer text-muted">
							<button type="submit" class="save-record" style="background-color:#29ABE2; padding: 10px 20px; color: #ffffff; border: none; text-decoration: none; float:right"> SAVE AND DELIVER HERE</button>
						</div>
					</div>
				</form>
				 <?php }else{ ?>
				<?php echo form_open_multipart('front/add_delivery_address', array('class'=>'') ); ?>
					<div class="card">
						<div class="card-header">
							<h5 class="card-title"><b>New Delivery Address </b></h5>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">

										<input type="text" name="name" placeholder="Full Name" class="form-control" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="ph_no" placeholder="10-digit mobile number" class="form-control" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="number" name="pin" placeholder="Pincode" class="form-control" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="locality" placeholder="locality" class="form-control">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control" rows="4" placeholder="Address (Area and Street)" name="address" ></textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="district" placeholder="City/District/Town" class="form-control" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<select class="form-control" name="state">
											<option value="0">-Select State-</option>
											<option value="Andhra Pradesh">Andhra Pradesh</option>
											<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
											<option value="Arunachal Pradesh">Arunachal Pradesh</option>
											<option value="Assam">Assam</option>
											<option value="Bihar">Bihar</option>
											<option value="Chandigarh">Chandigarh</option>
											<option value="Chhattisgarh">Chhattisgarh</option>
											<option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
											<option value="Daman and Diu">Daman and Diu</option>
											<option value="Delhi">Delhi</option>
											<option value="Lakshadweep">Lakshadweep</option>
											<option value="Puducherry">Puducherry</option>
											<option value="Goa">Goa</option>
											<option value="Gujarat">Gujarat</option>
											<option value="Haryana">Haryana</option>
											<option value="Himachal Pradesh">Himachal Pradesh</option>
											<option value="Jammu and Kashmir">Jammu and Kashmir</option>
											<option value="Jharkhand">Jharkhand</option>
											<option value="Karnataka">Karnataka</option>
											<option value="Kerala">Kerala</option>
											<option value="Madhya Pradesh">Madhya Pradesh</option>
											<option value="Maharashtra">Maharashtra</option>
											<option value="Manipur">Manipur</option>
											<option value="Meghalaya">Meghalaya</option>
											<option value="Mizoram">Mizoram</option>
											<option value="Nagaland">Nagaland</option>
											<option value="Odisha">Odisha</option>
											<option value="Punjab">Punjab</option>
											<option value="Rajasthan">Rajasthan</option>
											<option value="Sikkim">Sikkim</option>
											<option value="Tamil Nadu">Tamil Nadu</option>
											<option value="Telangana">Telangana</option>
											<option value="Tripura">Tripura</option>
											<option value="Uttar Pradesh">Uttar Pradesh</option>
											<option value="Uttarakhand">Uttarakhand</option>
											<option value="West Bengal">West Bengal</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="landmark" placeholder="landmark" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="alt_phone" placeholder="Alternate Phone (Optional)" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer text-muted">
							<button type="submit" class="save-record" style="background-color:#CB986A; padding: 10px 20px; color: #ffffff; border: none; text-decoration: none;"> SAVE AND DELIVER HERE</button>
						</div>
					</div>
				</form>
				<?php }// if else ?>


			</div>
			<!-- address form section -->
			<?php  
			//Array ( [0] => Array ( [basket_id] => 1 [user_id] => 1 [cloth_id] => 7 [service] => 2 [cloth_type] => 1 [price] => 15 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-23 00:03:18 [modified_by] => ) [1] => Array ( [basket_id] => 2 [user_id] => 1 [cloth_id] => 3 [service] => 2 [cloth_type] => 3 [price] => 100 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-23 00:03:18 [modified_by] => ) )
			$total_item = count( $basket_details );
			$delivery_charge = 40;
			$all_total = 0;
			foreach( $basket_details as $basket ){
				$price = $basket['price'];
				$quantity = $basket['quantity'];
				$total = $quantity * $price;
				$all_total = $all_total + $total;
			}
			$grand_total = $all_total + $delivery_charge;
			?>
			
			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title text-secondary">Price Details</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12 pb-3">
								Price (<?php echo $total_item;  ?>)
								<span class="float-right"><?php echo number_format($all_total,2); ?></span>
							</div>
							<div class="col-md-12 pb-3">
								Delivery Fee
								<span class="float-right text-success"><?php echo number_format($delivery_charge,2); ?></span>
							</div>
							<div class="col-md-12 pt-3" style="border-top:1px solid #dee2e6!important">
								<h5>Total Payable
									<span class="float-right"><?php echo number_format($grand_total,2); ?></span>
								</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>