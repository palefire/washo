<section class="breadcump">
        <div class="container">
            <a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href="<?php echo site_url('/front/my_basket/')  ?>"><span>My Basket</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href="<?php echo site_url('/front/checkout/')  ?>"><span>Delivery Address</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>Confirm Order</span></a>
        </div>
</section>
<section class="pf-cart-all-quantity">
	<div class="container">
		<?php 
			$database_error = $this->session->flashdata('database_error');
			$contact_exist = $this->session->flashdata('contact_exist');
			$database_error = $this->session->flashdata('database_error');
			if( $database_error ){
		 ?>
			<div class="alert alert-dismissible alert-warning">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <h4 class="alert-heading">Check!</h4>
			  <p class="mb-0"><?php echo $database_error; ?></p>
			</div>
		<?php } 
			if( $contact_exist ){
		?>
			<div class="alert alert-dismissible alert-warning">
			  <button type="button" class="close" data-dismiss="alert">&times;</button>
			  <h4 class="alert-heading">Check!</h4>
			  <p class="mb-0"><?php echo $contact_exist; ?></p>
			</div>
		<?php } 
			if( $database_error ){
		?>
		<div class="alert alert-dismissible alert-warning">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <h4 class="alert-heading">Check!</h4>
		  <p class="mb-0"><?php echo $database_error; ?></p>
		</div>
		<?php 
			}
		?>
		<div class="row">

			
			<!-- address form section -->
			<div class="col-md-8">
				
				<?php echo form_open_multipart('front/place_order', array('class'=>'') ); ?>
					<div class="card">
						<div class="card-header">
							<h5 class="card-title"><b>Proceed To Confirm Order</b></h5>
						</div>
						<div class="card-body" style="display: none"> 
							<div class="input-group mb-3">
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <input type="radio" aria-label="Checkbox for following text input" name="payment_mode" value="1" checked>
							      
							    </div>
							  </div>
							  <input type="text" class="form-control" aria-label="Text input with checkbox" value="Cash On Delivery" readonly>

							</div>

							<!-- <div class="input-group">
							  <div class="input-group-prepend">
							    <div class="input-group-text">
							      <input type="radio" aria-label="Radio button for following text input" name="payment_mode" value="1">
							    </div>
							  </div>
							  <input type="text" class="form-control" aria-label="Text input with radio button" value="Online Paymnet" readonly>
							</div> -->
						</div>
						<div class="card-footer text-muted">
							<button type="submit" class="save-record" style="background-color:#29ABE2; padding: 10px 20px; color: #ffffff; border: none; text-decoration: none;">PROCEED AND CONFIRM ORDER</button>
						</div>
					</div>
				</form>

				
				 
			</div>
			<!-- address form section -->

			<!-- price card section -->

			<?php  
			//Array ( [0] => Array ( [basket_id] => 1 [user_id] => 1 [cloth_id] => 7 [service] => 2 [cloth_type] => 1 [price] => 15 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-23 00:03:18 [modified_by] => ) [1] => Array ( [basket_id] => 2 [user_id] => 1 [cloth_id] => 3 [service] => 2 [cloth_type] => 3 [price] => 100 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-23 00:03:18 [modified_by] => ) )
			$total_item = count( $basket_details );
			$delivery_charge = 40;
			$all_total = 0;
			foreach( $basket_details as $basket ){
				$price = $basket['price'];
				$quantity = $basket['quantity'];
				$total = $quantity * $price;
				$all_total = $all_total + $total;
			}
			$grand_total = $all_total + $delivery_charge;
			?>
			

			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title text-secondary">Price Details</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12 pb-3">
								Price (<?php  echo $total_item; ?>)
								<span class="float-right"><?php echo number_format($all_total,2); ?></span>
							</div>
							<div class="col-md-12 pb-3">
								Delivery Fee
								<span class="float-right text-success"><?php echo number_format($delivery_charge,2); ?></span>
							</div>
							<div class="col-md-12 pt-3" style="border-top:1px solid #dee2e6!important">
								<h5>Total Payable
									<span class="float-right"><?php echo number_format($grand_total, 2); ?></span>
								</h5>
							</div>
						</div>
					</div>
				</div>
			</div>





		</div>
	</div>
</section>