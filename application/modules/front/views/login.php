<!DOCTYPE html>
<html>
<head>
	<title>Washo </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front/css/style.css');  ?>">

</head>
<body>


<!------ Include the above in your HEAD tag ---------->
<div class="body">
	<?php
			$login_failed = $this->session->flashdata('login_failed');
			$email_exist = $this->session->flashdata('email_exist');
			$phone_exist = $this->session->flashdata('login_failed');
			
			
			if( $login_failed ){
	?>
				<div class="alert alert-dismissible alert-warning">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <h4 class="alert-heading">Incorrect Credential!</h4>
				  <p class="mb-0"><?php echo $login_failed; ?></p>
				</div>
	<?php
		} 
		if( $email_exist ){
	?>	
	
				<div class="alert alert-dismissible alert-warning">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <h4 class="alert-heading">Check!</h4>
				  <p class="mb-0"><?php echo $email_exist; ?></p>
				</div>
	<?php
		} 
		if( $phone_exist ){
	?>	
	?>
				<div class="alert alert-dismissible alert-warning">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <h4 class="alert-heading">Check!</h4>
				  <p class="mb-0"><?php echo $phone_exist; ?></p>
				</div>
	<?php
		} ?>		
	<div class="login-reg-panel">
		<div class="login-info-box">
			<h2>Have an account?</h2>
			<p>Welcome to <a href="<?php echo site_url();  ?>">Washo</a></p>
			<label id="label-register" for="log-reg-show">Login</label>
			<input type="radio" name="active-log-panel" id="log-reg-show"  checked="checked">
		</div>
							
		<div class="register-info-box">
			<h2>Don't have an account?</h2>
			<p>Become a part of <a href="<?php echo site_url();  ?>">Washo</a> and enjoy our deluxe service.</p>
			<label id="label-login" for="log-login-show">Register</label>
			<input type="radio" name="active-log-panel" id="log-login-show">
		</div>
							
		<div class="white-panel">
			<div class="login-show">
				<h2>LOGIN</h2>
				<?php echo form_open_multipart('front/account/login', array() ); ?> 
					<input type="text" placeholder="Email/Phone Number" name="username" required>
					<input type="password" placeholder="Password" name="password" required>
					<input type="submit" name="login" value="Login">
				</form>
				<a href="#">Forgot password?</a>
			</div>
			<div class="register-show">
				<h2>REGISTER</h2>
				<?php echo form_open_multipart('front/account/registration', array() ); ?> 
					<input type="email" placeholder="Email" name="email" required>
					<input type="text" placeholder="Name" name="name" required>
					<input type="password" placeholder="Password" name="password" required>
					<input type="text" placeholder="Phone Number" name="phone" required>
					
					<input type="submit" name="register" value="Register">
				</form>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/front/js/custom.js');  ?>"></script>
</body>
</html>