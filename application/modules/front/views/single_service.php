<?php   
	// print_r($mens_clothes);
	//Array ( [0] => Array ( [cloth_id] => 1 [cloth_name] => Shirt [cloth_slug] => shirt-0 [cloth_type] => 1 [service_type] => 1 [price] => 22 [time] => 96 [icon] => shirt.svg [created_by] => 1 [created_at] => 2020-03-03 21:44:53 [modified_by] => 1 ) [1] => Array ( [cloth_id] => 5 [cloth_name] => Jeans [cloth_slug] => jeans-4 [cloth_type] => 1 [service_type] => 1 [price] => 25 [time] => 96 [icon] => [created_by] => 1 [created_at] => 2020-03-04 21:02:15 [modified_by] => ) )
?>

<section class="dh-single-service-description">
	<div class="container">
		<ul class="nav nav-tabs service-all-tabs">
		    <li><a data-toggle="tab" href="#menu1"  class="active service-single-tab">Men</a></li>
		    <li><a data-toggle="tab" href="#menu2" class="service-single-tab">Women</a></li>
		    <li><a data-toggle="tab" href="#menu3" class="service-single-tab">Households</a></li>
		    <li><a data-toggle="tab" href="#menu4" class="service-single-tab">Kids</a></li>

		</ul>
		<div class="dh-single-all-tab">
			<div class="tab-content">
			    <div id="menu1" class="tab-pane fade in show active">
			    	<div class="table-responsive">
					    <table class="table ">
					    	<thead class="thead-dark">
						    	<tr>
						    		<th scope="col" >Item</th>
						    		<th scope="col" >Duration</th>
						    		<th scope="col" >Quantity</th>
						    		<th scope="col" >Price</th>
						    		<th scope="col" >Action</th>
						    	</tr>
					   	 	</thead>
					   	 	<tbody>
					   	 		<?php  
					   	 			foreach( $mens_clothes as $cloth){
					   	 		 ?>
						    	<tr>
						    		<td><?php echo $cloth['cloth_name']  ?></td>
						    		<td><?php echo $cloth['time'];  ?> Hours</td>
						    		<td class="quantity"><input type="number" class="input-quantity" value="1"></td>
						    		<td>Rs. <?php echo $cloth['price'];  ?></td>
						    		<td class="add-parent"><span class="btn btn-primary add-to-basket" data-id = "<?php echo $cloth['cloth_id'];  ?>" data-service = "<?php echo $cloth['service_type'];  ?>" data-clothtype="<?php echo $cloth['cloth_type'];  ?>" data-price="<?php echo $cloth['price'];  ?>">Add To Basket</span></td>
						    	</tr>
						    	<?php  } ?>
					    	</tbody>
					    </table>
					</div>
				</div>
				<div id="menu2" class="tab-pane fade">
				    <div class="table-responsive">
					    <table class="table ">
					    	<thead class="thead-dark">
						    	<tr>
						    		<th scope="col" >Item</th>
						    		<th scope="col" >Duration</th>
						    		<th scope="col" >Quantity</th>
						    		<th scope="col" >Price</th>
						    		<th scope="col" >Action</th>
						    	</tr>
					   	 	</thead>
					   	 	<tbody>
					   	 		<?php  
					   	 			foreach( $womens_clothes as $cloth){
					   	 		 ?>
						    	<tr>
						    		<td><?php echo $cloth['cloth_name']  ?></td>
						    		<td><?php echo $cloth['time'];  ?> Hours</td>
						    		<td class="quantity"><input type="number" class="input-quantity" value="1"></td>
						    		<td>Rs. <?php echo $cloth['price'];  ?></td>
						    		<td class="add-parent"><span class="btn btn-primary add-to-basket" data-id = "<?php echo $cloth['cloth_id'];  ?>" data-service = "<?php echo $cloth['service_type'];  ?>" data-clothtype="<?php echo $cloth['cloth_type'];  ?>" data-price="<?php echo $cloth['price'];  ?>">Add To Basket</span></td>
						    	</tr>
						    	<?php  } ?>
					    	</tbody>
					    </table>
					</div>
				</div>
				<div id="menu3" class="tab-pane fade">
				  	<div class="table-responsive">
					    <table class="table ">
					    	<thead class="thead-dark">
						    	<tr>
						    		<th scope="col" >Item</th>
						    		<th scope="col" >Duration</th>
						    		<th scope="col" >Quantity</th>
						    		<th scope="col" >Price</th>
						    		<th scope="col" >Action</th>
						    	</tr>
					   	 	</thead>
					   	 	<tbody>
					   	 		<?php  
					   	 			foreach( $household_clothes as $cloth){
					   	 		 ?>
						    	<tr>
						    		<td><?php echo $cloth['cloth_name']  ?></td>
						    		<td><?php echo $cloth['time'];  ?> Hours</td>
						    		<td class="quantity"><input type="number" class="input-quantity" value="1"></td>
						    		<td>Rs. <?php echo $cloth['price'];  ?></td>
						    		<td class="add-parent"><span class="btn btn-primary add-to-basket" data-id = "<?php echo $cloth['cloth_id'];  ?>" data-service = "<?php echo $cloth['service_type'];  ?>" data-clothtype="<?php echo $cloth['cloth_type'];  ?>" data-price="<?php echo $cloth['price'];  ?>">Add To Basket</span></td>
						    	</tr>
						    	<?php  } ?>
					    	</tbody>
					    </table>
					</div>
				</div>
				<div id="menu4" class="tab-pane fade">
				   	<div class="table-responsive">
					    <table class="table ">
					    	<thead class="thead-dark">
						    	<tr>
						    		<th scope="col" >Item</th>
						    		<th scope="col" >Duration</th>
						    		<th scope="col" >Quantity</th>
						    		<th scope="col" >Price</th>
						    		<th scope="col" >Action</th>
						    	</tr>
					   	 	</thead>
					   	 	<tbody>
					   	 		<?php  
					   	 			foreach( $kid_clothes as $cloth){
					   	 		 ?>
						    	<tr>
						    		<td><?php echo $cloth['cloth_name']  ?></td>
						    		<td><?php echo $cloth['time'];  ?> Hours</td>
						    		<td class="quantity"><input type="number" class="input-quantity" value="1"></td>
						    		<td>Rs. <?php echo $cloth['price'];  ?></td>
						    		<td class="add-parent"><span class="btn btn-primary add-to-basket" data-id = "<?php echo $cloth['cloth_id'];  ?>" data-service = "<?php echo $cloth['service_type'];  ?>" data-clothtype="<?php echo $cloth['cloth_type'];  ?>" data-price="<?php echo $cloth['price'];  ?>">Add To Basket</span></td>
						    	</tr>
						    	<?php  } ?>
					    	</tbody>
					    </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
  		$(document).ready( function(){
  			$('.add-to-basket').on( 'click', function(e){
  				e.preventDefault();
  				var prev_cat_val = parseInt( $('.no-basket').html() );
				var cloth_id = $(this).data('id');
				var service_id = $(this).data('service');
				var cloth_type = $(this).data('clothtype');
				var price = $(this).data('price');
				var quantity = $(this).parent('.add-parent').siblings('.quantity').children('.input-quantity').val();
				var user_id = 	<?php  
									if( isset($this->session->userdata['user_id']) ){
										echo $this->session->userdata['user_id'];
									}else{
										echo 0;
									}
				 				?>;
				// $('#add-to-basket-Modal').modal('show');
				if( user_id == 0 ){
					alert("Please Log In To Book A Service");
				}else{
					$.ajax({
						beforeSend : function(xhr){

						},
						url : "<?php echo site_url('front/add_to_basket') ?>",
						type : 'POST',
						data : {
							'cloth_id' : cloth_id,
							'quantity': quantity,
							'user_id': user_id,
							'service_id' : service_id,
							'cloth_type' : cloth_type,
							'price' : price,
						},
						success: function( data ){
							console.log(data);
							$('#add-to-basket-Modal').modal('show');
							if( data == 'inserted' ){
								$('.no-basket').html(prev_cat_val+1);
							}
							
							
						},
						error: function(response){
							console.log(response);
						}
					});//ajax
				}
  			});
  		});

  </script>