<style type="text/css">
    	.dh-contact{padding-bottom:80px}
.dh-contact .page-title{text-align: center; margin-bottom: 80px; margin-top: 80px;}
.dh-contact .page-title .lead{font-size: 30px; color: #0071BC; line-height: 34px; font-family: raleway, sans-serif; font-weight: 700;}
.dh-contact .page-title .sub-lead{font-size: 14px; color: #666666; line-height: 20px; font-family: raleway, sans-serif; font-weight: 400; width: 90%; margin: 0 auto;}
.dh-contact .contact .cont{text-align: center;}
.dh-contact .contact .cont img{margin-bottom:10px}
.dh-contact .contact .cont h3{font-size: 1.5rem; margin-bottom: .5rem; font-family: inherit; font-weight: 500; line-height: 1.2; color: inherit; }
.dh-contact .contact .cont p{margin:0 0 10px;font-size:15px;color:#666}
.dh-contact .contact .cont a{color:#666}

.pbot-main .btn-default{padding: 15px 25px; float: right; background-color: #29ABE2; border-color: #29ABE2; border-radius: 0; color: #fff; font-weight: bold;}
.pbot-main .maps-wraper{position: relative;}
.pbot-main .maps{width: 100%; height: 310px; display: block; background: #ccc; position: relative;}
.contact-address .contact-address-item .contact-address-heading{color: #0071BC; font-weight: 700; margin-bottom: 10px;margin-top:30px}
.contact-address .contact-address-item p{margin:0 0 10px;font-size:15px;color:#666}
</style>

<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>Contact</span></a>
		</div>
</section>
<section class="dh-contact">
	<div class="container">
		<!--  -->
        <div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="page-title" style="text-align: center; margin-bottom: 80px; margin-top: 80px;">
					<h2 class="lead">CONTACT US</h2>
					<p class="sublead">Get in touch with us</p>
				</div>
			</div>
		</div>
        <div class="row contact">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="cont">
                            <img src="http://washolandromat.com/images/contact-phone.png">
                            <h3>Customer Care</h3>
                            <p>+91 9330 134345</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="cont">
                            <img src="http://washolandromat.com/images/contact-wp.png">
                            <h3>WhatsApp</h3>
                            <p>+91 9330 131919</p>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="cont">
                            <img src="http://washolandromat.com/images/contact-mail.png">
                            <h3>Email</h3>
                            <p><a href="mailto:washo@washolandromat.com">washo@washolandromat.com</a></p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="cont">
                            <img src="http://washolandromat.com/images/contact-address.png">
                            <h3>Address</h3>
                            <p>1, Gobinda Niwas, Rajarhat Road, Kolkata 700 059</p>
                        </div>
                    </div>
                   
                </div>
            </div>
           
        </div>​<!-- end outer row -->
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="page-title" style="margin-bottom:80px;margin-top:80px;text-align: center;">
					<h2 class="lead">Send Your Query</h2>
                    <p>Service available in kolkata location only</p>
				</div>
			</div>
		</div>  
		<div class="row pbot-main">
			<div class="col-sm-12 col-md-6">
			    <form name="Contactform" action="contact.php" method="post" class="form-contact shake"  data-toggle="validator">
					<div class="form-group">
						<input type="text" class="form-control" name="Name" placeholder="Enter Name *" required>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<input type="email" class="form-control" name="Email" placeholder="Enter Email">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="Phone" placeholder="Enter Phone Number *" required>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="Address" placeholder="Enter Address">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						 <textarea class="form-control" name="Message" rows="5" placeholder="Enter Your Message"></textarea>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<div id="success"></div>
						<input type="submit" class="btn btn-default" value="SEND A MESSAGE">
					</div>
				</form>
			</div>
			<div class="col-sm-12 col-md-6">
				<div class="maps-wraper">
					<div id="maps" class="maps" ><iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d14730.966557091646!2d88.43972421591124!3d22.626121093878993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s1%2C+Gobinda+Niwas%2C+Rajarhat+Road%2C+Kolkata+700+059!5e0!3m2!1sen!2sin!4v1558334383356!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:2px solid #29abe2;" allowfullscreen></iframe></div>
				</div>
				<div class="row contact-address">
					<div class="col-sm-12 col-md-6">
						<div class="contact-address-item">
							<div class="contact-address-heading">OPENING HOURS</div>
							<p>
							Monday - Friday : 08:00 - 19:00 pm<br />
							Saturday : 08:00 - 17:00
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>