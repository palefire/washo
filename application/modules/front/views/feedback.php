<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href="<?php echo site_url('/front/account/my_orders/').$this->session->userdata['user_id']  ?>"><span>My Orders</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>Feedback</span></a>
		</div>
</section>

<div class="container">
	
	<div class="row">
		<div class="col-md-6 col-12">
			<?php echo form_open_multipart('front/account/add_feedback', array() ); ?>
				<div class="form-group">
					<label>Please Provide Your Valuable Feedback</label>
					<textarea name="feedback"class="form-control"  rows="3"><?php if( $feedback_details == null ){}else{ echo $feedback_details[0]['feedback']; }  ?></textarea>
					<input type="hidden" name="order_code" value="<?php echo $order_code;  ?>" />
					<input type="submit" name="feebback_submit" style="margin-top:20px;" value="Submit">
				</div>
			</form>
		</div>
		<div class="col-md-6 col-12">
			<table style="width:100%;margin-top:30px">
				<thead>
					<th>Name</th>
					<th>Service</th>
					<th>Complaint</th>
				</thead>
				<tbody>
				<?php  foreach($order_detail as $order){  ?>
					<?php  //[0] => Array ( [order_id] => 1 [order_code] => pws-1 [user_id] => 1 [name] => Shirt [service_id] => 2 [cloth_type] => 1 [franchise_id] => 1 [price_per_unit] => 15 [quantity] => 1 [total_price] => 15 [cloth_status] => 6 [payment_mode] => 1 [transaction_id] => [payment_status] => 2 [status] => 1 [order_date] => 2020-03-25 20:05:26 [delivery_date] => 2020-03-25 23:09:42 [expected_delivery_date] => 2020-04-20 23:48:05 [pick_up_biker_id] => 1 [warehouse_id] => 1 [delivery_biker_id] => 1 [address_id] => 1 [basket_id] => 10 [created_by] => 1 [created_at] => 2020-03-20 12:03:08 [modified_by] => [service_name] => Steam Iron [service_slug] => steam-iron-1 [svg] => iron.svg  ?>
					<tr>
						<td><?php echo $order['name'];  ?></td>
						<td><?php echo $order['service_name'];  ?></td>
						<td>
							<?php if( $order['complain_sent'] == '0' ){  ?>
								<span class="btn btn-info add-complain" data-id="<?php echo $order['order_id']; ?>" data-order="<?php echo $order['order_code']  ?>" data-name="<?php echo $order['name']  ?>" data-service="<?php echo $order['service_id']  ?>"  data-clothtype="<?php echo $order['cloth_type']  ?>"  data-franchise="<?php echo $order['franchise_id']  ?>"  data-price="<?php echo $order['price_per_unit']  ?>" data-quantity="<?php echo $order['quantity']  ?>">Order Again</span>
							<?php  } ?>
						</td>
					</tr>
				<?php }  ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
  		$(document).ready( function(){
  			$('.add-complain').on( 'click', function(e){
  				e.preventDefault();
  				
				var c_order_id = $(this).data('id');
				var c_order_code = $(this).data('order');
				var c_name = $(this).data('name');
				var c_service_id = $(this).data('service');
				var c_cloth_type = $(this).data('clothtype');
				var c_franchise_id = $(this).data('franchise');
				var c_price = $(this).data('price');
				var c_quantity = $(this).data('quantity');
				var c_user_id = 	<?php  
									if( isset($this->session->userdata['user_id']) ){
										echo $this->session->userdata['user_id'];
									}else{
										echo 0;
									}
				 				?>;
				if( c_user_id == 0 ){
					alert("Please Log In To Request A Complain");
				}else{
					// alert(c_order_id);
					$.ajax({
						beforeSend : function(xhr){

						},
						url : "<?php echo site_url('front/account/add_complain') ?>",
						type : 'POST',
						data : {
							'c_order_id' 		: c_order_id,
							'c_order_code'		: c_order_code,
							'c_name'			: c_name,
							'c_service_id'		: c_service_id,
							'c_cloth_type' 		: c_cloth_type,
							'c_franchise_id' 	: c_franchise_id,
							'c_price' 			: c_price,
							'c_quantity' 		: c_quantity,
							'c_user_id' 		: c_user_id,
						},
						success: function( data ){
							console.log(data);
							location.reload();
							
							
						},
						error: function(response){
							console.log(response);
						}
					});//ajax
				}
  			});
  		});






  	</script>






