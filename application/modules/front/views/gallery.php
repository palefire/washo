<style>
	.image-gallery{padding-top:80px;padding-bottom:80px}
	.image-gallery a{ background:#0C6; width:100%; height:100px; overflow:hidden;}
	.image-gallery a img{ width:100%;}
</style>
 <script src="<?php echo base_url('assets/front/js/lightbox-plus-jquery.min.js');  ?>"></script>	 
<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>Gallery</span></a>
		</div>
</section>


<section>
	<div class="container image-gallery">
		
		<div class="row">
			<?php  foreach( $galleries as $gallery ){ ?>
			<div class="col-xs-12 col-md-3">
                <a class="example-image-link" href="<?php echo site_url('uploads/gallery/').$gallery['gal_image'];  ?>" data-lightbox="example-set">
                	<img class="example-image" src="<?php echo site_url('uploads/gallery/').$gallery['gal_image'];  ?>" alt=""/>
                </a>
			</div>
			<?php  } ?>
            

			
		</div>
		
	</div>
</section>