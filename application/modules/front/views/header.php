<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta charset="UTF-8">
	<title>
		<?php 
			if( $this->router->fetch_method() == 'index' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo';
			}
			if( $this->router->fetch_method() == 'my_basket' AND $this->router->fetch_class() == 'front'){
				echo 'Washo | Basket';
			}
			if( $this->router->fetch_method() == 'check_pin' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | Pin';
			}
			if( $this->router->fetch_method() == 'checkout' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | Checkout';
			}
			if( $this->router->fetch_method() == 'payment' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | Checkout';
			}
			if( $this->router->fetch_method() == 'about_us' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | About Us';
			}
			if( $this->router->fetch_method() == 'contact' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | Contact';
			}
			if( $this->router->fetch_method() == 'faq' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | FAQ';
			}
			if( $this->router->fetch_method() == 'offers' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | Offers';
			}
			if( $this->router->fetch_method() == 'gallery' AND $this->router->fetch_class() == 'front' ){
				echo 'Washo | Gallery';
			}
			if( $this->router->fetch_method() == 'index' AND $this->router->fetch_class() == 'service' ){
				echo 'Washo | Services';
			}
			if( $this->router->fetch_method() == 'single_service' AND $this->router->fetch_class() == 'service' ){
				echo 'Washo | Single Service';
			}
			if( $this->router->fetch_method() == 'index' AND $this->router->fetch_class() == 'franchise'){
				echo 'Washo | Franchise';
			}
			if( $this->router->fetch_method() == 'login' AND $this->router->fetch_class() == 'account'){
				echo 'Washo | Login ';
			}
			if( $this->router->fetch_method() == 'my_orders' AND $this->router->fetch_class() == 'account'){
				echo 'Washo | My Orders ';
			}
			if( $this->router->fetch_method() == 'feedback' AND $this->router->fetch_class() == 'account'){
				echo 'Washo | Feedback ';
			}
			// if( $this->router->fetch_method() == 'login' ){
			// 	echo 'Palefirebooks | Login';
			// }
		?>
	</title>
	<link rel="icon" href="<?php echo $this->data['fronts_img_path']  ?>favicon.ico" type="image/png" sizes="16x16">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
	<link href="<?php echo $this->data['fronts_css_path'] ;  ?>lightbox.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->data['fronts_css_path'] ;  ?>style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->data['fronts_css_path'] ;  ?>responsive.css">
	<script type="text/javascript" src="<?php echo $this->data['fronts_js_path']  ?>jquery.min.js"></script>
</head>
<body>
	<div class="home">
		<div class="top-header">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-4">
						<div class="top-address">
							<i class="fas fa-map-marker-alt map-icon"></i>
							<address>1 Gobinda Niwas,<br> Rajarhat Road,<br> 700059</address>
						</div>
					</div>
					<div class="col-md-4 col-4">
						<a href="" class="logo">
							<img src="<?php echo $this->data['fronts_img_path']  ?>logo.png" class="img-responsive">
						</a>
					</div>
					<div class="col-md-4 col-4">
						<div class="top-telephone">
							<i class="fas fa-phone-alt phone-icon"></i>
							<address>+91 9330134345</address>
							<div class="top-time hidden-sm">Mon.-Fri. 7:00 AM to 7:00 PM</div>
						</div>
						<?php if( isset( $this->session->userdata['user_name'] ) ){  ?>
						<div class="top-user-name">
							<a href="">
								<span><i class="fas fa-user"></i>&nbsp;&nbsp;<?php echo $this->session->userdata['user_name'] ?></span>
							</a>
						</div>
					<?php  } ?>
					</div>
				</div>
			</div>
		</div>
		<header class="sticky">
			<div class="container">
				<nav class="navbar navbar-expand-lg navbar-light">
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>
				  <div class="collapse navbar-collapse" id="navbarNav">
				    <ul class="navbar-nav">
				      <li class="nav-item 
				      	<?php   
				      		if( $this->router->fetch_method() == 'index' AND $this->router->fetch_class() == 'front' ){
								echo 'active';
							}
				      	?>
				      ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/')  ?>">HOME</a>
				      </li>
				      <li class="nav-item
				      	<?php   
				      		if( $this->router->fetch_method() == 'about_us' AND $this->router->fetch_class() == 'front' ){
								echo 'active';
							}
				      	?>
				      ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/about_us')  ?>">ABOUT</a>
				      </li>
				      <li class="nav-item
				      	<?php   
				      		if( $this->router->fetch_method() == 'index' AND $this->router->fetch_class() == 'service' ){
								echo 'active';
							}
				      	?>
				      ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('service')  ?>">SERVICES</a>
				      </li>
				       <li class="nav-item
				       	<?php   
				      		if( $this->router->fetch_method() == 'prices' AND $this->router->fetch_class() == 'front' ){
								echo 'active';
							}
				      	?>
				       ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/prices')  ?>">PRICES</a>
				      </li>  
				      <li class="nav-item
				      	<?php   
				      		if( $this->router->fetch_method() == 'faq' AND $this->router->fetch_class() == 'front' ){
								echo 'active';
							}
				      	?>
				      ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/faq')  ?>">FAQ</a>
				      </li>
				       <li class="nav-item
				       	<?php   
				      		if( $this->router->fetch_method() == 'contact' AND $this->router->fetch_class() == 'front' ){
								echo 'active';
							}
				      	?>
				       ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/contact')  ?>">CONTACT</a>
				      </li>
				      <li class="nav-item
				      	<?php   
				      		if( $this->router->fetch_method() == 'gallery' AND $this->router->fetch_class() == 'front' ){
								echo 'active';
							}
				      	?>
				      ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/gallery')  ?>">GALLERY</a>
				      </li>

				      <li class="nav-item
				      	<?php   
				      		if( $this->router->fetch_method() == 'franchise_page' AND $this->router->fetch_class() == 'front' ){
								echo 'active';
							}
				      	?>
				      ">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/franchise_page')  ?>">FRANCHISE</a>
				      </li>

				      <?php if( !isset( $this->session->userdata['user_id'] ) ){  ?>

				      <li class="nav-item" style="margin-left:200px">
				        <a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/account')  ?>">Login</a>
				      </li>
				  	<?php  }else{ ?>
				  		<li class="nav-item
				  			<?php   
					      		if( $this->router->fetch_method() == 'my_orders' AND $this->router->fetch_class() == 'account' ){
									echo 'active';
								}
				      		?>
				  		" >
				       		<a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/account/my_orders/').$this->session->userdata['user_id']  ?>">Orders</a>
				      	</li>
				  		<li class="nav-item" style="">
				       		<a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/account/logout')  ?>">Logout</a>
				      	</li>
				      	<li class="nav-item
				      		<?php   
					      		if( $this->router->fetch_method() == 'my_basket' AND $this->router->fetch_class() == 'front' ){
									echo 'active';
								}
				      		?>
				      	" >
				       		<a class="nav-link text-white font-weight-normal" href="<?php echo site_url('/front/my_basket')  ?>"><i class="fa fa-shopping-basket" aria-hidden="true"><span class="no-basket"><?php  echo $this->data['no_of_basket'] ?></span></i></a>
				      	</li>
				  	<?php  } ?>
				    </ul>
				  </div>
				</nav>
			</div>
		</header>

		<div class="modal fade" id="add-to-basket-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index:99999">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Succsfully Added to Cart</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        
		      </div>
		    </div>
		  </div>
		</div>
		