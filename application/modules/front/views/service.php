	
	<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>SERVICES</span></a>
		</div>
	</section>
	<section class="our-service">
			<div class="container">
				<h2>OUR SERVICES</h2>
				<p>We strive to ensure quality laundry, on time delivery and reliable service for all linen, uniform and guest laundry needs. A dedicated in-plant quality assurance team is on hand to provide regular visual inspection to maintain quality standard and to seek for continuous improvements. The team would review and offer precise washing formula suitable for each type of linen.</p>
			</div>
		</section>
		<section class="all-service">
			<div class="container-fluid">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				  <ol class="carousel-indicators">
				    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				  </ol>
				  <div class="carousel-inner">

				  	<?php  
				  		$service_no = count($service_details);
				  		$car_counter = 0;
				  		while( $car_counter < $service_no){
				  			if( $car_counter == 0){
				  				$active = 'active home-caroitem';
				  			}else{
				  				$active = '';
				  			}
				  	 ?>

				    <div class="carousel-item <?php echo $active  ?>">
				    	<div class="container">
					      <div class="row">
					      	<?php if( array_key_exists( $car_counter, $service_details ) ) { ?>
					      	<div class="col-md-4">
					      		<a href="<?php echo site_url('front/service/single_service/').$service_details[$car_counter]['service_slug']  ?>" class="service-topic">
				      				<div class="service-img">
				      					<img src="<?php echo site_url('/uploads/services/').$service_details[$car_counter]['img']  ?>">
				      					<div class="service-img-bor"></div>
				      				</div>
				      				<div class="service-item">
				      					<h4><?php echo $service_details[$car_counter]['service_name']  ?></h4>
				      					<p>Let us pick up your dirty laundry, sort it, pre-treat stains, wash, dry, fold and deliver back to you in one neat, easy package.</p>
				      				</div>
					      		</a>
					      	</div>
					      	<?php  } ?>
					      	<?php if( array_key_exists( $car_counter+1, $service_details ) ) { ?>
					      	 <div class="col-md-4">
					      		<a href="<?php echo site_url('front/service/single_service/').$service_details[$car_counter+1]['service_slug']  ?>" class="service-topic">
				      				<div class="service-img">
				      					<img src="<?php echo site_url('/uploads/services/').$service_details[$car_counter+1]['img']  ?>">
				      					<div class="service-img-bor"></div>
				      				</div>
				      				<div class="service-item">
				      					<h4><?php echo $service_details[$car_counter+1]['service_name']  ?> </h4>
				      					<p>Let us pick up your dirty laundry, sort it, pre-treat stains, wash, dry, fold and deliver back to you in one neat, easy package.</p>
				      				</div>
					      		</a>
					      	</div>
					      	<?php  } ?>
					      	<?php if( array_key_exists( $car_counter+2, $service_details ) ) { ?>
					      	 <div class="col-md-4">
					      		<a href="<?php echo site_url('front/service/single_service/').$service_details[$car_counter+2]['service_slug']  ?>" class="service-topic">
				      				<div class="service-img">
				      					<img src="<?php echo site_url('/uploads/services/').$service_details[$car_counter+2]['img']  ?>">
				      					<div class="service-img-bor"></div>
				      				</div>
				      				<div class="service-item">
				      					<h4><?php echo $service_details[$car_counter+3]['service_name']  ?></h4>
				      					<p>Let us pick up your dirty laundry, sort it, pre-treat stains, wash, dry, fold and deliver back to you in one neat, easy package.</p>
				      				</div>
					      		</a>
					      	</div>
					      	<?php  } ?>
					      	
					      </div>
					    </div>
				    </div>

				    <?php  
				    $car_counter = $car_counter + 3; 
					}// end of while
					?>

				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				    <span aria-hidden="true">
				    	<img src="<?php echo $this->data['fronts_img_path']  ?>prev.png">
				    </span>
				    
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				    <span aria-hidden="true">
				    	<img src="<?php echo $this->data['fronts_img_path']  ?>next.png">
				    </span>
				  </a>
				</div>
			</div>
		</section>
		<section class="commercial-service">
			<div class="container">
				<h2>COMMERCIAL LAUNDRY SERVICE</h2>
				<div class="row no-row-margin">
					<div class="col-md-5">
						<div class="commerce-img">
							<img src="https://smartdata.tonytemplates.com/laundry/wp-content/uploads/2018/02/img-commercial-service-1-1.jpg">
						</div>
					</div>
					<div class="col-md-7">
						<div class="commerce-content">
							<p>Large corporations have determined that there is a financial benefit to outsourcing back office work because it saves money. Allowing us to do your laundry is cost effective and will allow you and your employees to focus on your core business. We offer smart solutions to meet your commercial laundry needs. Our pick up and delivery laundry service is fast, convenient, and saves you time and money.</p>
							<p>Laundry isn’t your main business, but it is ours and we love it! For more information about our commercial laundry services and pricing and to schedule your first pick up, call us at 1 (800) 123-45-67</p>
						</div>
					</div>
				</div>
			</div> 
		</section>
		<section class="service-client">
			<div class="container">
				<h4>OUR COMMERCIAL LAUNDRY CLIENTS INCLUDE:</h4>
				<div class="row">
					<div class="col-md-4">
						<div class="content">
							<ul>
								<li>Automotive Detail and Repair Shops</li>
								<li>Assisted Living / Nursing Homes</li>
								<li>Religious Organizations</li>
								<li>Hotels & Motels</li>
								<li>Salons & Spas</li>
								<li>House Cleaning Companies</li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="content">
							<ul>
								<li>Automotive Detail and Repair Shops</li>
								<li>Assisted Living / Nursing Homes</li>
								<li>Religious Organizations</li>
								<li>Hotels & Motels</li>
								<li>Salons & Spas</li>
								<li>House Cleaning Companies</li>
							</ul>
						</div>
					</div>
					<div class="col-md-4">
						<div class="content">
							<ul>
								<li>Automotive Detail and Repair Shops</li>
								<li>Assisted Living / Nursing Homes</li>
								<li>Religious Organizations</li>
								<li>Hotels & Motels</li>
								<li>Salons & Spas</li>
								<li>House Cleaning Companies</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="commercial-type">
			<div class="container">
				<h2>COMMERCIAL LAUNDRY</h2>
				<p class="type-sub">From an owner-operated hair salon to a government hospital and everything in between, we are the most responsive and cost-competitive laundry and linen provider in your city.</p>
				<div class="row">
					<div class="col-md-4">
						<div class="type-content first-fade animate fadeLeft">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>towel.png">
							</div>
							<div class="type-desc">
								<a href="">Sheets & Towels</a>
								<p>Our linen program covers a range of products appropriate for use in businesses of all types and sizes. From the bedding for a 100 bed hospital to the hand towels at a local coffee shop.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="type-content">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>pen.png">
							</div>
							<div class="type-desc">
								<a href="">Janitorial Supplies</a>
								<p>Our linen program covers a range of products appropriate for use in businesses of all types and sizes. From the bedding for a 100 bed hospital to the hand towels at a local coffee shop.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="type-content last-fade animate fadeRight">
							<div class="content-icon">
								<img src="<?php echo $this->data['fronts_img_path']  ?>gymnast.png">
							</div>
							<div class="type-desc">
								<a href="">Entryway and Floor Mats</a>
								<p>Our linen program covers a range of products appropriate for use in businesses of all types and sizes. From the bedding for a 100 bed hospital to the hand towels at a local coffee shop.</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>
		<section class="services-premium">
			<a href="" class="premium-topic">24/7 premium customer services</a>
		</section>
		<section class="service-feature">
			<div class="container">
				<h2>OUR FEATURES</h2>
				<div class="row">
					<div class="col-md-6">
						<div class="content">
							<h4><a href="">Eco-Friendly Dry Cleaning</a></h4>
							<p>Place your dirty clothes in a secure Laundry Locker, lock the locker with any four digit code, and we’ll do your dry cleaning for you. Items are packaged just like any other dry cleaning, but you pick them up at your convenience, not theirs.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="content">
							<h4><a href="">Wash & Fold</a></h4>
							<p>Place your dirty clothes in a secure Laundry Locker, lock the locker with any four digit code, and we’ll do your dry cleaning for you. Items are packaged just like any other dry cleaning, but you pick them up at your convenience, not theirs.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="content">
							<h4><a href="">Bag & Shoe Repairs & Shines</a></h4>
							<p>Place your dirty clothes in a secure Laundry Locker, lock the locker with any four digit code, and we’ll do your dry cleaning for you. Items are packaged just like any other dry cleaning, but you pick them up at your convenience, not theirs.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="content">
							<h4><a href="">Wash & Fold</a></h4>
							<p>Place your dirty clothes in a secure Laundry Locker, lock the locker with any four digit code, and we’ll do your dry cleaning for you. Items are packaged just like any other dry cleaning, but you pick them up at your convenience, not theirs.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="service-coupon">
			<div class="container">
				<h2>OUR COUPONS</h2>
				<div class="row">
					<div class="col-md-4">
						<div class="content">
							<div class="inside" style="background-image:url(https://smartdata.tonytemplates.com/laundry/wp-content/themes/laundry/images/coupon-bg-1.jpg);">
								<div class="coupon-logo">
									<img src="img/logo.jpeg">
								</div>
								<div class="text1">5604 Willow Crossing Ct, Clifton, VA, 20124</div>
								<div class="text2">Expires: 5/31/2017</div>
								<div class="text3">5$ OFF</div>
								<div class="text4">Wash & Fold $25 Order</div>
								<div class="text5">Don't Spend ALL DAY. Doing Laundry!</div>
								<div class="print-coupon">
									<a href=""><i class="fas fa-print print-icon"></i>Print Coupon</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="content">
							<div class="inside" style="background-image:url(https://smartdata.tonytemplates.com/laundry/wp-content/themes/laundry/images/coupon-bg-2.jpg);">
								<div class="coupon-logo">
									<img src="img/logo.jpeg">
								</div>
								<div class="text1">5604 Willow Crossing Ct, Clifton, VA, 20124</div>
								<div class="text2">Expires: 5/31/2017</div>
								<div class="text3">5$ OFF</div>
								<div class="text4">Wash & Fold $25 Order</div>
								<div class="text5">Don't Spend ALL DAY. Doing Laundry!</div>
								<div class="print-coupon">
									<a href=""><i class="fas fa-print print-icon"></i>Print Coupon</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="content">
							<div class="inside" style="background-image: url(https://smartdata.tonytemplates.com/laundry/wp-content/themes/laundry/images/coupon-bg-3.jpg);">
								<div class="coupon-logo">
									<img src="img/logo.jpeg">
								</div>
								<div class="text1">5604 Willow Crossing Ct, Clifton, VA, 20124</div>
								<div class="text2">Expires: 5/31/2017</div>
								<div class="text3">5$ OFF</div>
								<div class="text4">Wash & Fold $25 Order</div>
								<div class="text5">Don't Spend ALL DAY. Doing Laundry!</div>
								<div class="print-coupon">
									<a href=""><i class="fas fa-print print-icon"></i>Print Coupon</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>