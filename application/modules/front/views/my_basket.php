<?php  
	// print_r($basket_details);
	// [0] => Array ( [basket_id] => 1 [user_id] => 1 [cloth_id] => 1 [service] => 1 [cloth_type] => 1 [price] => 22 [quantity] => 5 [status] => 1 [created_by] => 1 [created_at] => 2020-03-03 21:44:53 [modified_by] => 1 [cloth_name] => Shirt [cloth_slug] => shirt-0 [service_type] => 1 [time] => 96 [icon] => shirt.svg )
 ?>
<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href="<?php echo site_url('/front/account/my_orders/').$this->session->userdata['user_id']  ?>"><span>My Basket</span></a>
		</div>
</section>
<div class="pf-cart-all-quantity">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">
							<b>My Basket</b>
						</h5>
					</div>
					<div class="card-body">
						<?php foreach( $basket_details as $basket_detail ){  ?>
						<div class="row" style="margin-bottom: 30px;">
							<div class="col-md-3">
								<div style="height: 120px; width: 100%; background: url(<?php echo $this->data['svg_path'].$basket_detail['icon']  ?>); background-size: cover;">
								</div>
							</div>
							<div class="col-md-9">
								<table>


									<tbody>
										<tr>
											<td>
												<h6><?php echo $basket_detail['cloth_name']  ?></h6>
											</td>
										</tr>
										<tr>
											<td>
												<h5 class="py-2">
													<span>Rs. <?php echo $basket_detail['price']  ?></span>
												</h5>
											</td>
										</tr>
										<tr>
											<td>
												<span class="pr-3">
													<input type="number" name="" style="width: 50px; padding-left: 10px;" min="1" max="9" value="<?php echo $basket_detail['quantity']  ?>" data-id="<?php echo $basket_detail['basket_id']  ?>" class="change-quantity" data-stock="2">
												</span>
												<a href="#" style="text-decoration: none; color: rgb(238, 1, 6);" class="delete-basket" data-id="<?php echo $basket_detail['basket_id']  ?>">REMOVE</a>
											</td>
										</tr>
									</tbody>



								</table>
							</div>
						</div>
						<?php }  ?>



					</div>
					<div class="card-footer text-muted proceed-checkout">
						<a href="<?php echo base_url('front/check_pin')  ?>" style="background-color:#29ABE2; padding: 10px 20px; color: #ffffff; border: none; text-decoration: none; float:right" class="check-hover">PROCEED TO ORDER</a>
					</div>
				</div>
			</div>

			<?php  
			//Array ( [0] => Array ( [basket_id] => 1 [user_id] => 1 [cloth_id] => 7 [service] => 2 [cloth_type] => 1 [price] => 15 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-23 00:03:18 [modified_by] => ) [1] => Array ( [basket_id] => 2 [user_id] => 1 [cloth_id] => 3 [service] => 2 [cloth_type] => 3 [price] => 100 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-23 00:03:18 [modified_by] => ) )
			$total_item = count( $basket_details );
			$delivery_charge = 40;
			$all_total = 0;
			foreach( $basket_details as $basket ){
				$price = $basket['price'];
				$quantity = $basket['quantity'];
				$total = $quantity * $price;
				$all_total = $all_total + $total;
			}
			$grand_total = $all_total + $delivery_charge;
			?>

			<div class="col-md-4">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title text-secondary">Price Details</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12 pb-3">
								Price (<?php echo $total_item;  ?>)
								<span class="float-right"><?php echo number_format($all_total,2); ?></span>
							</div>
							<div class="col-md-12 pb-3">
								Delivery Fee
								<span class="float-right text-success"><?php echo number_format($delivery_charge,2); ?></span>
							</div>
							<div class="col-md-12 pt-3" style="border-top:1px solid #dee2e6!important">
								<h5>Total Payable
									<span class="float-right"><?php echo number_format($grand_total,2); ?></span>
								</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- row -->

		
	</div>
</div>



<script>
  		$(document).ready( function(){
  			$('.delete-basket').on( 'click', function(e){
  				e.preventDefault();
  				
				var basket_id = $(this).data('id');
				var user_id = 	<?php  
									if( isset($this->session->userdata['user_id']) ){
										echo $this->session->userdata['user_id'];
									}else{
										echo 0;
									}
				 				?>;
				if( user_id == 0 ){
					alert("Please Log In To Book A Service");
				}else{
					$.ajax({
						beforeSend : function(xhr){

						},
						url : "<?php echo site_url('front/delete_basket') ?>",
						type : 'POST',
						data : {
							'basket_id' : basket_id,
						},
						success: function( data ){
							console.log(data);
							location.reload();
							
							
						},
						error: function(response){
							console.log(response);
						}
					});//ajax
				}
  			});



  			// Update quantity
  			$('.change-quantity').on( 'change', function(e){
  				var basket_id = $(this).data('id');
  				var quantity = $(this).val();
  				$.ajax({
						beforeSend : function(xhr){
							$('#ajax-loader').modal('show');
						},
						url : "<?php echo site_url('front/update_basket_quantity') ?>",
						type : 'POST',
						data : {
							'basket_id' : basket_id,
							'quantity' : quantity,
						},
						success: function( data ){
							console.log(data);
							$('#ajax-loader').modal('hide');
							location.reload();
							
							
						},
						error: function(response){
							console.log(response);
							$('#ajax-loader').modal('hide');
							$('#ajax-error').modal('how');
						}
					});//ajax
  			});



  		});
  	</script>