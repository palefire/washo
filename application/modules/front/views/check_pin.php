<section class="breadcump">
        <div class="container">
            <a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href="<?php echo site_url('/front/my_basket/')  ?>"><span>My Basket</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>Choose Franchise</span></a>
        </div>
</section>
<div class="container" style="margin-top:30px; margin-bottom:30px;">
    <div class="row">
        <div class="col-md-6">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Enter Pin/Zip</label>
                    <input type="email" class="form-control form-control-lg user-pin" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <small id="emailHelp" class="form-text text-muted">Please Enter Your Pin .</small>
                </div>
               
            </form>
        </div>
       
            <div class="col-md-6 ">
                <?php echo form_open_multipart('front/update_franchise', array() ); ?>
                <div class="append-franchise">
                    
                </div>
                 </form>
            </div>
       
    </div>
    
</div>
<script>
    $(document).ready( function(){
        $('.user-pin').on( 'keyup', function(e){
            // e.preventDefault();
            
            var pin = $(this).val();

            // console.log(pin);
            
            $.ajax({
                beforeSend : function(xhr){

                },
                url : "<?php echo site_url('front/fetch_franchise') ?>",
                type : 'POST',
                data : {
                    'pin' : pin,
                },
                success: function( data ){
                    var data_array =  jQuery.parseJSON( data );
                    var option = ``;
                    if( data_array.code == 1 ){
                       $('.append-franchise').html('');
                       var data_count  = data_array.data.length;
                       var option = option +`<div class="form-group">
                                                <label for="exampleFormControlSelect1">Select Franchise</label>
                                                <select class="form-control form-control-lg" id="exampleFormControlSelect1" name="franchise">`;
                       for(var i=0; i<data_count;i++){
                            var option = option + `<option value="${data_array.data[i].id}">${data_array.data[i].name}</option>`;
                       }
                       var option = option + `  </select>
                                              </div>
                                              <button type="submit" class="btn btn-primary proceed-focus">Proceed With This Franchise</button>`;
                      $('.append-franchise').append(option);
                      $('.proceed-focus').focus();
                    }else{
                        $('.append-franchise').html('');
                        var option = option + ` <div class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Service Not Available</h5>
                                                        <p class="card-text">We Are Sorry to inform that our service is not available at the pin mentioned.!</p>
                                                        <span class="btn btn-danger try-another" >Try Another Pin</span>
                                                    </div>
                                                </div>`
                        $('.append-franchise').append(option);
                    }
                    
                    
                },
                error: function(response){
                    console.log(response);
                }
            });//ajax
            
        });

        $('.append-franchise').on( 'click','.try-another', function(e){
            // alert('saarasij');
            $('.user-pin').val('');
            $('.append-franchise').html('');
             $('.user-pin').focus();
        });




    });
</script>

