<section class="dh-subbanner" style="background:url('img/slide_page.jpg') no-repeat center center;   -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="dh-caption">
						<h3>ABOUT US</h3>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="dh-about-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="page-title">
					</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="dh-about-wrap">
						<p>Pleased to inform you that we are <strong>WASHO</strong>. A Brand of Express  Laundromat Laundry Solutions LLP. the premium Laundry  & Dry Cleaning service in  Kolkata, the one stop solution for your all hotel & guest laundry /cleaning needs.</p>
                        <p>We have newly installed the state of the art modern washing systems, we are using all the eco-friendly chemicals and expertise to give you the best quality & hygienic service at your door step.</p>
                        <p>"Washo – The Complete LAUNDRY Solution" understands your hygiene needs & are dedicated to provide you with the best of our services.</p>
                        <p>Washo provides you affordable & easiest way to Washing, Ironing & Dry Clean your clothes with superb quality at your doorstep via mobile app & website.</p>
                        
                        <h5>The process we follow :</h5>
                        <ul>
                        	<li>The processing of washing, laundry and dry cleaning is done in best-class setups with Italian equipment and eco friendly chemicals.<li>
                            <li>We do laundry with filtered water & use fabric softener and Eco-friendly detergents to wash your clothes.</li>
                            <li>We do not mix your clothes with others, this makes your wash very hygienic.</li>
                            <li>We do Cuff & Collar cleaning before loading into washing machine.</li>
                            <li>We provide Steam Ironing on Vacuum ironing boards which give natural shine & extra life to the clothes.</li>
                        </ul>
                        
                        <p>We at Washo are a dedicated team of experienced professionals working to ensure that your garments are cleaned & returned to you with greatest care, quality & exceptional service.</p>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="why-item-wrap">
				</div>
			</div>
			
		</div>
	</section>