<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<a href="" class="footer-logo">
							<img src="<?php echo $this->data['fronts_img_path']  ?>logo.png" class="img-responsive">
						</a>
					</div>
					<div class="col-md-9">
						<div class="row footer-separate">
							<div class="col-md-8">
								<div class="prom-footer">
									<span class="text1">Get Started Today!</span>
									<span class="text2">+91 9330134345</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="social-icon-footer">
									<ul>
										<li>
											<a href="" class="fab fa-facebook-f totsoc-icon"></a>
										</li>
										<li>
											<a href="" class="fab fa-twitter totsoc-icon"></a>
										</li>
										<li>
											<a href="" class="fab fa-instagram-square totsoc-icon"></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-last-content">
					<div class="row">
						<div class="col-md-3">
							<div class="footer-reserved">
								<p>© 2020 Washo.. All Rights Reserved.</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="box-location">
								<div class="footer-location">
									<i class="fas fa-map-marker-alt map-icon"></i>
									<p>1 Gobinda Niwas,<br> Rajarhat Road,<br> 700059</p>
								</div>
								<div class="footer-mail">
									<i class="fa fa-envelope icon-mail"></i>
									<p>washo@washolandromat.com</p>
								</div>
							</div>
						</div>
						<div class="time-table">
							<i class="far fa-clock icon-clock"></i>
							<p>Mon-Fri: 7:00am-7:00pm<br>
							   Sat-Sun: 10:00am-5:00pm					
							</p>
						</div>
						<div class="col-md-3">
							<div class="footer-link">
								<ul>
									<li>
										<a href="">Terms and Conditions</a>
									</li>
									<li>
										<a href="">Privacy Policy</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="bubbleContainer">
					<div class="bubble-1"></div>
					<div class="bubble-2"></div>
					<div class="bubble-3"></div>
					<div class="bubble-4"></div>
					<div class="bubble-5"></div>
					<div class="bubble-6"></div>
					<div class="bubble-7"></div>
					<div class="bubble-8"></div>
					<div class="bubble-9"></div>
					<div class="bubble-10"></div>
				</div>

			</div>
		</footer>
	</div>





	
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo $this->data['fronts_js_path']  ?>lightbox-plus-jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo $this->data['fronts_js_path']  ?>custom.js"></script>
	
</body>
</html>

<script type="text/javascript" src="<?php echo $this->data['fronts_js_path']  ?>jquery.waypoints.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->data['fronts_js_path']  ?>jquery.counterup.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function(){
		$('.counter').counterUp({
			    delay: 10,
			    time: 1000
			});
		});
</script>






















	



<div class="modal fade" id="ajax-loader" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Please Wait</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ajax-error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Oops!! Something Went Wrong.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Please Try After Some Time.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>



