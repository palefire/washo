<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>PRICES</span></a>
		</div>
	</section>
<section class="affordable-price">
	<div class="container">
		<h2>AFFORDABLE PRICES</h2>
		<div class="row">
			<div class="col-md-4">
				<a href="" class="affor-content">
					<div class="affor-icon">
						<i class="fas fa-tshirt item-icon"></i>
					</div>
					<h3>From <span>$2</span> per shirt</h3>
					<p>Shirt service</p>
				</a>
			</div>
			<div class="col-md-4">
				<a href="" class="affor-content">
					<div class="affor-icon">
						<i class="fas fa-tshirt item-icon"></i>
					</div>
					<h3><span>$2.50</span> per kg</h3>
					<p>Laundry service</p>
				</a>
			</div>
			<div class="col-md-4">
				<a href="" class="affor-content">
					<div class="affor-icon">
						<i class="fas fa-tshirt item-icon"></i>
					</div>
					<h3>From <span>$2</span> per item</h3>
					<p>Dry Cleaning</p>
				</a>
			</div>
		</div>
	</div>
</section>