<style type="text/css">
    .dhf-subbanner{background-color: #29ABE2; height:150px;font-family: raleway, sans-serif; } 
	.dhf-subbanner .dh-caption{ margin-top: 50px; float: right; color:#fff; text-align:right; } 
	.dhf-subbanner .dh-caption a{ color:#fff; text-decoration:none; } 
	.dhf-subbanner .dh-caption h3{ font-size: 30px; color: #fff; line-height: 42px; font-weight: 900; text-transform:uppercase; margin-bottom: 0; } 

	.faq-service{}
	.faq-service .page-title{text-align: center; margin-bottom: 80px; margin-top: 80px;}
	.faq-service .page-title .lead{font-size: 30px; color: #0071BC; line-height: 34px; font-family: raleway, sans-serif; font-weight: 700;}
	.faq-service .page-title .sublead{font-size: 14px; color: #666666; line-height: 20px; font-family: raleway, sans-serif; font-weight: 400; width: 90%; margin: 0 auto;}


	.faq-service .faq-item-image{position:relative;width:100%;text-align:center;}
	.faq-service .faq-item-image img{display:block;width:auto;}
	.faq-service .content{margin-bottom:10px;}
	.faq-service .content .content-head{background-color:#29ABE2;width:100%;border:1px solid #ddd;border-radius:0}
	.faq-service .content .content-head button{font-size:14px;color:#ffffff;text-decoration:none;}
	.faq-service .content .content-item{background-color:#F8F8F8;padding:20px;margin-top:4px;border:1px solid #F6f6f6}
	.faq-service .content .content-item p{color:#666;font-size: 14px}
	.faq-service .content-head.active{background-color: #0071BC;}
</style>


<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>FAQ</span></a>
		</div>
</section>

<section class="faq-service">
	<div class="container">
		<!--  -->
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="page-title">
					<h2 class="lead">FREQUENTLY ASKED QUESTIONS</h2>
					<p class="sublead">We make doing your laundry simple. We can save your time, so you can enjoy doing the things you love. We can save you money on soap, water, heating and electricity. So you can enjoy even more of the things you love. Our prices are simple and affordable.</p>
				</div>
			</div>
		</div>
		
		<!--  -->
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="page-title">
					<h2 class="lead">Your Solution is Here</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-4">
				<div class="faq-item-image">
					<img src="https://washolandromat.com/images/why-us.png" alt="" />
				</div>
			</div>
			<div class="col-sm-12 col-md-8">
			<div id="accordion">
			<?php 
				$faq_counter = 1;
				foreach( $faqs as $faq ){
					$heading = 'heading'.$faq_counter;
					$collapse = 'collapse'.$faq_counter;
					if( $faq_counter == 1 ){
						$active = 'active';
						$show = 'show';
					}else{
						$active = '';
						$show = '';
					}
			?>
					<div class="content">
					  	<div class="content-head <?php echo $active;  ?>" id="<?php echo $heading  ?>">
					    	<button class="btn btn-link" data-toggle="collapse" data-target="#<?php echo $collapse ; ?>" aria-expanded="true" aria-controls="<?php echo $collapse ; ?>">
					           <?php echo $faq_counter  ?>. <?php echo $faq['faq_question']  ?>?
					        </button>
					    </div>

					    <div id="<?php echo $collapse ; ?>" class="collapse <?php echo $show  ?>" aria-labelledby="<?php echo $heading  ?>" data-parent="#accordion">
					      <div class="content-item">
					        <p><?php echo $faq['faq_answer']  ?></p>
					      </div>
					    </div>
					 </div>
			<?php 
					$faq_counter++;
				}
			?>
			 

			</div>
			</div>
		</div>
	</div>
</section>