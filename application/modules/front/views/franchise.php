

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700,900' rel='stylesheet' type='text/css'>




<style type="text/css">
.dh-subbanner{background-color: #29ABE2; height:150px;font-family: raleway, sans-serif; } 
.dh-subbanner .dh-caption{ margin-top: 50px; float: right; color:#fff; text-align:right; } 
.dh-subbanner .dh-caption a{ color:#fff; text-decoration:none; } 
.dh-subbanner .dh-caption h3{ font-size: 30px; color: #fff; line-height: 42px; font-weight: 900; text-transform:uppercase; margin-bottom: 0; }

.franchise-form{background-image:url(http://washolandromat.com/images/bg_1.jpg);height:auto;width:100%;padding:100px 0px;background-repeat: no-repeat; background-attachment: fixed; background-position: center;}
 .franchise-form .frn-form{border: 1px solid #ccc; border-radius: 15px; width: 100%; overflow: hidden; background: #ffffffe0;}
 .franchise-form .frn-form .frm-head{background: #29ABE2; padding: 15px 20px;}
 .franchise-form .frn-form .frm-head h3{color: #fff; text-align: center; font-size: 25px; line-height: 30px; font-weight: bold}
 .franchise-form .frn-form .frm-area{padding:15px}
 .franchise-form .frn-form .frm-area h4{margin-bottom: 15px;font-size: 17px;font-weight: bold;color:#666}
 .franchise-form .frn-form .frm-area form label{font-size:13px;color:#666}
 .franchise-form .frn-form .frm-area input[type=text],[type=email],[type=number]{font-size:13px;color:#666}
 .franchise-form .frn-form .frm-area textarea{font-size:13px;color:#666} 
</style>

    <section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>FRANCHISE</span></a>
		</div>
	</section>
    <section class="franchise-form">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="frn-form">
                        <div class="frm-head">
                            <h3>Business franchisee form</h3>
                        </div>
                        <div class="frm-area">
                            <form>
                                <h4>Franchisee Applicant's details</h4>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Name :</label>
                                <div class="col-sm-9">
                                 <input type="text" class="form-control" placeholder="Enter Name">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Email :</label>
                                <div class="col-sm-9">
                                  <input type="email" class="form-control" placeholder="Enter Email">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Phone No :</label>
                                <div class="col-sm-9">
                                  <input type="number" class="form-control" id="inputPassword" placeholder="Enter Phone No">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">What are you currently doing?</label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>Employed</label>
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>Own Self Business</label>
                                                </div>
                                             </div>
                                               <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>Other</label>
                                                </div>
                                             </div>
                                        </div>                              
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Brief on the nature of your current business*</label>
                                    <div class="col-sm-9">
                                      <textarea class="form-control" placeholder="Enter Your Current Address"></textarea>
                                    </div>
                                </div>
                                <h4>Your Franchisee Plans</h4>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Location :</label>
                                    <div class="col-sm-9">
                                     <input type="text" class="form-control" placeholder="Enter Your Current Location">
                                    </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">What are you currently doing?</label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>Yes</label>
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>No</label>
                                                </div>
                                             </div>
                                        </div>                                       
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Investment capital (Indian Rupees)</label>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>500,000 - 1,500,000</label>
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>1,500,001 - 2,500,000</label>
                                                </div>
                                             </div>
                                               <div class="col-md-4">
                                                <div class="">
                                                    <input type="radio">
                                                    <label>Greater than 2,500,000</label>
                                                </div>
                                             </div>
                                        </div>                              
                                    </div>                              
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Why do you think Laundry Franchisee will be right Business option for You?</label>
                                    <div class="col-sm-9">
                                      <textarea class="form-control" placeholder="Why do you think Laundry Franchisee will be right Business"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>                        
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

