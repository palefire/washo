<?php
	// echo '<pre>';
	// print_r($service_details);  
	// echo '</pre>';
?>
<section class="home-slide">
	<div id="carouselExampleControls" class="carousel slide " data-ride="carousel">
	  <div class="carousel-inner homecaro-inner">

	  	<?php  
	  		$service_no = count($service_details);
	  		$car_counter = 0;
	  		while( $car_counter < $service_no){
	  			if( $car_counter == 0){
	  				$active = 'active home-caroitem';
	  			}else{
	  				$active = '';
	  			}
	  	 ?>

	    <div class="carousel-item <?php echo $active  ?>">
	  		<div class="left-align" style="background-image: url(<?php echo site_url('/uploads/services/').$service_details[$car_counter]['img']  ?>);">
	  			<div class="content">
	  				<h2><?php echo $service_details[$car_counter]['service_name']  ?></h2>
	  				<p>We care for the clothes you wear</p>
	  				<div class="slide-btn">
	  					<a href="<?php echo site_url('front/service/single_service/').$service_details[$car_counter]['service_slug']  ?>"> ORDER NOW</a>
	  				</div>
	  			</div>
	  		</div>
	  		<div class="right-align" style="background-image: url(<?php echo site_url('/uploads/services/').$service_details[$car_counter+1]['img']  ?>);">
	  			<div class="content">
	  				<h2><?php echo $service_details[$car_counter+1]['service_name']  ?></h2>
	  				<p>We care for the clothes you wear</p>
	  				<div class="slide-btn">
	  					<a href="<?php echo site_url('front/service/single_service/').$service_details[$car_counter+1]['service_slug']  ?>"> ORDER NOW</a>
	  				</div>
	  			</div>
	  		</div>
	    </div>
	    <?php  
	    $car_counter = $car_counter + 2; 
		}// end of while
		?>
	   
	   
	  </div>
	  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	    <span  aria-hidden="true">
	    	<img src="<?php echo $this->data['fronts_img_path']  ?>back.png" style="width:40px">
	    </span>
	 
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	     <span aria-hidden="true">
		    	<img src="<?php echo $this->data['fronts_img_path']  ?>next-white.png" style="width:40px">		 
		  </span>
	  </a>
	</div>
	<div class="bubbleContainer">
			<div class="bubble-1"></div>
			<div class="bubble-2"></div>
			<div class="bubble-3"></div>
			<div class="bubble-4"></div>
			<div class="bubble-5"></div>
			<div class="bubble-6"></div>
			<div class="bubble-7"></div>
			<div class="bubble-8"></div>
			<div class="bubble-9"></div>
			<div class="bubble-10"></div>
	</div>
</section>

<section class="home-passionate">
	<div class="container">
		<h2>OUR SERVICES</h2>
		<p>We strive to ensure quality laundry, on time delivery and reliable service for all linen, uniform and guest laundry needs. A dedicated in-plant quality assurance team is on hand to provide regular visual inspection to maintain quality standard and to seek for continuous improvements. The team would review and offer precise washing formula suitable for each type of linen.</p>
	</div>
</section>

<section class="home-workstep">
	<div class="container">
		<h2>HOW IT WORKS: IN 4 EASY STEPS</h2>
		<div class="row">
			<div class="col-md-3 work-item">
				<a href="" class="content">
					<div class="work-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>img_icon_01.png">
					</div>
					<h6 class="description">Step 1</h6>
					<h4 class="title"><span>Bag up</span> all your dirty clothes</h4>
				</a>
			</div>
			<div class="col-md-3 work-item">
				<a href="" class="content">
					<div class="work-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>img_icon_02.png">
					</div>
					<h6 class="description">Step 2</h6>
					<h4 class="title">We <span>Pick up</span> all your dirty clothes</h4>
				</a>
			</div>
			<div class="col-md-3 work-item">
				<a href="" class="content">
					<div class="work-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>img_icon_03.png">
					</div>
					<h6 class="description">Step 3</h6>
					<h4 class="title">We <span>Clean</span> clothes</h4>
				</a>
			</div>
			<div class="col-md-3 work-item">
				<a href="" class="content">
					<div class="work-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>img_icon_04.png">
					</div>
					<h6 class="description">Step 4</h6>
					<h4 class="title">We <span>deliver</span> clean, folded clothes</h4>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="commercial-service">
	<div class="container">
		<h2>COMMERCIAL LAUNDRY SERVICE</h2>
		<div class="row no-row-margin">
			<div class="col-md-5">
				<div class="commerce-img">
					<img src="<?php echo $this->data['fronts_img_path']  ?>img-commercial-service-1-1.jpg">
				</div>
			</div>
			<div class="col-md-7">
				<div class="commerce-content">
					<p>Large corporations have determined that there is a financial benefit to outsourcing back office work because it saves money. Allowing us to do your laundry is cost effective and will allow you and your employees to focus on your core business. We offer smart solutions to meet your commercial laundry needs. Our pick up and delivery laundry service is fast, convenient, and saves you time and money.</p>
					<p>Laundry isn’t your main business, but it is ours and we love it! For more information about our commercial laundry services and pricing and to schedule your first pick up, call us at 1 (800) 123-45-67</p>
				</div>
			</div>
		</div>
	</div> 
</section>


<section class="affordable-price">
	<div class="container">
		<h2>AFFORDABLE PRICES</h2>
		<div class="row">
			<div class="col-md-4">
				<a href="" class="affor-content">
					<div class="affor-icon">
						<i class="fas fa-tshirt item-icon"></i>
					</div>
					<h3>From <span>$2</span> per shirt</h3>
					<p>Shirt service</p>
				</a>
			</div>
			<div class="col-md-4">
				<a href="" class="affor-content">
					<div class="affor-icon">
						<i class="fas fa-tshirt item-icon"></i>
					</div>
					<h3><span>$2.50</span> per kg</h3>
					<p>Laundry service</p>
				</a>
			</div>
			<div class="col-md-4">
				<a href="" class="affor-content">
					<div class="affor-icon">
						<i class="fas fa-tshirt item-icon"></i>
					</div>
					<h3>From <span>$2</span> per item</h3>
					<p>Dry Cleaning</p>
				</a>
			</div>
		</div>
		<div class="affor-btnspace">
			<a href="<?php echo site_url('/front/prices')  ?>">SEE ALL PRICES HERE</a>
		</div>
	</div>
</section>

<section class="google-paylink">
	<div class="content">
		<div class="text1">Tap. Clean. Deliver.</div>
		<div class="text2">Order even faster</div>
		<div class="apps-btn">
			<a href="">
				<img src="https://smartdata.tonytemplates.com/laundry/wp-content/uploads/2011/05/btn-google-play.png">
			</a>
			
		</div>
	</div>
</section>

<section class="why-choose-us">
	<div class="container">
		<h2>WHY CHOOSE US</h2>
		<div class="row">
			<div class="col-md-4">
				<div class="type-content first-fade animate fadeLeft">
					<div class="content-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>towel.png">
					</div>
					<div class="type-desc">
						<a href="">Persionalized Experience</a>
						<p>We take utmost care of your clothes, segregating based on the cloth type and giving you instant clothes to make a statement.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="type-content">
					<div class="content-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>pen.png">
					</div>
					<div class="type-desc">
						<a href="">Affordable Pricing</a>
						<p>Prices that suits your pocket is one of our USP. An option of choosing between 2 types of pricing is available.p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="type-content last-fade animate fadeRight">
					<div class="content-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>gymnast.png">
					</div>
					<div class="type-desc">
						<a href="">Convenience</a>
						<p>With just a tap of a button, your laundry gets done, giving your leisure time to spend with family and friends.</p>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="type-content first-fade animate fadeLeft">
					<div class="content-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>towel.png">
					</div>
					<div class="type-desc">
						<a href="">Quality</a>
						<p>Forgot to wash your clothes for interview/business meeting, never mind. With our super express delivery, we would get your laundry done in less than 8 hours.p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="type-content">
					<div class="content-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>pen.png">
					</div>
					<div class="type-desc">
						<a href="">Express Delivery</a>
						<p>Forgot to wash your clothes for interview/business meeting, never mind. With our super express delivery, we would get your laundry done in less than 8 hours.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="type-content last-fade animate fadeRight">
					<div class="content-icon">
						<img src="<?php echo $this->data['fronts_img_path']  ?>gymnast.png">
					</div>
					<div class="type-desc">
						<a href="">Instant Order Update</a>
						<p>Regular updates of your order, to help you keep a track of your laundry and plan accordingly.p>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<!-- <section class="home-pricelist">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="price-block first-pricing">
					<div class="price-block-title">Standard</div>
					<div class="price-block-price">$349</div>
					<div class="price-block-details">
						<span>50</span>Clothes Per Month:
					</div>
					<ul>
						<li>4 t-shirts</li>
						<li>1 pairs of jeans</li>
						<li>3 button-down shirts</li>
						<li>1 pair of shorts</li>
						<li>7 pairs of underwear</li>
						<li>6 pairs of socks</li>
						<li>1 towel</li>
						<li>1 set of sheets</li>
						<li>1 set of sheets (every other week)</li>
					</ul>
					<p>
						<a href="" class="btn btn-default">Choose plan</a>
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="price-block second-pricing">
					<div class="price-block-title">Standard</div>
					<div class="price-block-price">$349</div>
					<div class="price-block-details">
						<span>50</span>Clothes Per Month:
					</div>
					<ul>
						<li>4 t-shirts</li>
						<li>1 pairs of jeans</li>
						<li>3 button-down shirts</li>
						<li>1 pair of shorts</li>
						<li>7 pairs of underwear</li>
						<li>6 pairs of socks</li>
						<li>1 towel</li>
						<li>1 set of sheets</li>
						<li>1 set of sheets (every other week)</li>
					</ul>
					<p>
						<a href="" class="btn btn-default">Choose plan</a>
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="price-block third-pricing">
					<div class="price-block-title">Standard
						<span class="sale-label" style="background-image:url(https://smartdata.tonytemplates.com/laundry/wp-content/uploads/2011/05/sale-label.png);">-10%</span>
					</div>
					<div class="price-block-price"><span class="price-old">$499</span>$349</div>
					<div class="price-block-details">
						<span>50</span>Clothes Per Month:
					</div>
					<ul>
						<li>4 t-shirts</li>
						<li>1 pairs of jeans</li>
						<li>3 button-down shirts</li>
						<li>1 pair of shorts</li>
						<li>7 pairs of underwear</li>
						<li>6 pairs of socks</li>
						<li>1 towel</li>
						<li>1 set of sheets</li>
						<li>1 set of sheets (every other week)</li>
					</ul>
					<p>
						<a href="" class="btn btn-default">Choose plan</a>
					</p>
				</div>
			</div>
		</div>
		<div class="package-deal">
			<a href="">View Package Details</a>
		</div>
	</div>
</section> -->

		