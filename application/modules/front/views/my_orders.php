

<?php   
	// print_r($pending_order);
	//Array ( [order_id] => 1 [order_code] => washo-20200228-1 [user_id] => 1 [name] => Shirt [service_id] => 1 [cloth_type] => 1 [franchise_id] => 1 [price_per_unit] => 20 [quantity] => 2 [total_price] => 40 [cloth_status] => 0 [payment_mode] => 1 [transaction_id] => [payment_status] => 1 [status] => 1 [order_date] => 2020-03-09 14:33:22 [delivery_date] => [pick_up_biker_id] => [warehouse_id] => [delivery_biker_id] => [address_id] => 0 [basket_id] => 0 [created_by] => 1 [created_at] => 2020-03-20 12:02:00 [modified_by] => [service_name] => Wash & Iron [service_slug] => wash-iron-0 [svg] => wash_iron.svg )
?>
<section class="breadcump">
		<div class="container">
			<a href="<?php echo site_url('/')  ?>"><span>HOME</span></a><i class="fa fa-angle-right" style="font-size:12px;color:#b6bdc0;margin-right:5px"></i><a href=""><span>My Orders</span></a>
		</div>
	</section>
<section class="dh-single-service-description">
	<div class="container">

		<?php
			$order_placed = $this->session->flashdata('order_placed');
			$feedback_submitted = $this->session->flashdata('feedback_submitted');
			$database_error = $this->session->flashdata('database_error');
			
			if( $order_placed ){
		?>
				<div class="alert alert-dismissible alert-success">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <h4 class="alert-heading">Thank You !!</h4>
				  <p class="mb-0"><?php echo $order_placed; ?></p>
				</div>
		<?php
			} 
			if( $feedback_submitted ){
		?>
				<div class="alert alert-dismissible alert-success">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <h4 class="alert-heading">Thank You !</h4>
				  <p class="mb-0"><?php echo $feedback_submitted; ?></p>
				</div>
		<?php   }
		if( $database_error ){
		?>
			<div class="alert alert-dismissible alert-alert">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <h4 class="alert-heading">Alert!</h4>
				  <p class="mb-0"><?php echo $feedback_submitted; ?></p>
				</div>
		<?php  } ?>
		<ul class="nav nav-tabs service-all-tabs">
		    <li><a data-toggle="tab" href="#menu1"  class="active service-single-tab">Pending</a></li>
		    <li><a data-toggle="tab" href="#menu2" class="service-single-tab">In Wash</a></li>
		    <li><a data-toggle="tab" href="#menu3" class="service-single-tab">Complete</a></li>

		</ul>
		<div class="dh-single-all-tab">
			<div class="tab-content">

			    <div id="menu1" class="tab-pane fade in show active">
			    	<div class="table-responsive">
					    <table class="table ">
					    	<thead class="thead-dark">
						    	<tr>
						    		<th scope="col" >Code</th>
						    		<th scope="col" >Name</th>
						    		<th scope="col" >Quantity</th>
						    		<th scope="col" >Service</th>
						    		<th scope="col" >Price</th>
						    		<th scope="col" >Status</th>
						    	</tr>
					   	 	</thead>
					   	 	<tbody>
					   	 		<?php  
					   	 			foreach( $pending_order as $order){
					   	 		 ?>
						    	<tr>
						    		<td><?php  echo $order['order_code']  ?></td>
						    		<td><?php  echo $order['name'];  ?> </td>
						    		<td><?php  echo $order['quantity'];  ?></td>
						    		<td><?php  echo $order['service_name'];  ?></td>
						    		<td><?php  echo $order['price_per_unit'];  ?></td>
						    		<td><span class="btn btn-warning">Pending</span></td>
						    	</tr>
						    	<?php  } ?>
					    	</tbody>
					    </table>
					</div>
				</div>
				<div id="menu2" class="tab-pane fade">
				    <div class="table-responsive">
					    <table class="table ">
					    	<thead class="thead-dark">
						    	<tr>
						    		<th scope="col" >Code</th>
						    		<th scope="col" >Name</th>
						    		<th scope="col" >Quantity</th>
						    		<th scope="col" >Service</th>
						    		<th scope="col" >Price</th>
						    		<th scope="col" >Status</th>
						    	</tr>
					   	 	</thead>
					   	 	<tbody>
					   	 		<?php  
					   	 			foreach( $washing_order as $order){
					   	 		 ?>
						    	<tr>
						    		<td><?php  echo $order['order_code']  ?></td>
						    		<td><?php  echo $order['name'];  ?> </td>
						    		<td><?php  echo $order['quantity'];  ?></td>
						    		<td><?php  echo $order['service_name'];  ?></td>
						    		<td><?php  echo $order['price_per_unit'];  ?></td>
						    		<td><span class="btn btn-secondary">In Wash</span></td>
						    	</tr>
						    	<?php  } ?>
					    	</tbody>
					    </table>
					</div>
				</div>
				<div id="menu3" class="tab-pane fade">
				  	<div class="table-responsive">
					    <table class="table ">
					    	<thead class="thead-dark">
						    	<tr>
						    		<th scope="col" >Code</th>
						    		
						    		<th scope="col" >Service</th>
						    		
						    		<th scope="col" >Feedback</th>
						    	</tr>
					   	 	</thead>
					   	 	<tbody>
					   	 		<?php  
					   	 			foreach( $complete_order as $order){
					   	 		 ?>
						    	<tr>
						    		<td><?php  echo $order['order_code']  ?></td>
						    		
						    		<td><?php  echo $order['service_name'];  ?></td>
						    	
						    		<td><a href="<?php echo site_url('front/account/feedback/'.$order['order_code'])  ?>"class="btn btn-success text-center">Feedback</a></td>
						    	</tr>
						    	<?php  } ?>
					    	</tbody>
					    </table>
					</div>
				</div>
				
			</div>
		</div>


		
	</div>
</section>