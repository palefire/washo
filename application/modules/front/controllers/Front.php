<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
	}

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		$query =  $this->db
					->order_by('service_id','ASC')
					->get('service');
		$service_details = $query->result_array();
		$service_no = array();
		$count = 1;
		foreach( $service_details as $service){
			  $service['service_id'];
			$q1= $this->db
					->where(array('service_id' =>$service['service_id']))
					->get('pf_order');
			$no_of_service = $q1->num_rows();
			$service_no[$count] = $no_of_service;
			$count++;
		}

		// print_r($service_no);		
		$this->load->view('header', array());
		$this->load->view('home', array(
			'service_details'=>$service_details,
			'service_no'	=> $service_no,
		));
		$this->load->view('footer', array());
	} //function


		/*
	|--------------------------------------------------------------------------
	| Add To Cart function
	|--------------------------------------------------------------------------
	*/

	public function add_to_basket(){
		$cloth_id = $this->input->post('cloth_id');
		$quantity = $this->input->post('quantity');
		$service_id = $this->input->post('service_id');
		$cloth_type = $this->input->post('cloth_type');
		$price = $this->input->post('price');
		$user_id = $this->input->post('user_id');

		$query1 = $this->db->where( array('user_id'=>$user_id, 'cloth_id'=>$cloth_id, 'status'=>'1') )->get('basket');
		$basket_exist = $query1->num_rows();


		if( $basket_exist >0 ){
			
			$basket_details = $query1->result_array();
			$basket_id = $basket_details[0]['basket_id'];
			$old_quantity = $basket_details[0]['quantity'];
			$new_quantity = $old_quantity +1; 

			$data = array('quantity'=>$new_quantity);

			$return = $this->db->update('basket', $data, array('basket_id' => $basket_id));
			if( $return ){
				echo $basket_id.'update';
			}else{
				echo 0;
			}

		}else{
			$data = array(
				'user_id'  		=> $user_id,
				'service'  		=> $service_id,
				'cloth_id'		=> $cloth_id,
				'cloth_type'  	=> $cloth_type,
				'price'  		=> $price,
				'quantity'  	=> $quantity,
				'status'  		=> "1",
				'created_by'	=> $user_id
			);

			$return = $this->db->insert('basket', $data);
			$cart_id = $this->db->insert_id();
			if( $return ){
				echo 'inserted';
			}else{
				echo 'Insert Failed';
			}
			
		}


		
	}// function


		/*
	|--------------------------------------------------------------------------
	| Basket and checkout function
	|--------------------------------------------------------------------------
	*/

	public function my_basket(){
		if( ! $this->session->userdata['user_id'] ){
			return redirect();
			die();
		}
		$user_id = $this->session->userdata['user_id'];
		$query = $this->db
					->join('clothes','basket.cloth_id = clothes.cloth_id')
					->where( array('user_id'=>$user_id, 'status'=>'1') )
					->get('basket');
		$basket_details = $query->result_array();

		$this->load->view('header', array());
		$this->load->view('my_basket', array(
			'basket_details'	=> $basket_details
		));
		$this->load->view('footer', array());
	} //function

	public function update_basket_quantity(){
		$basket_id = $this->input->post('basket_id');
		$quantity = $this->input->post('quantity');

		$data = array(
			'quantity' => $quantity
		);
		$return = $this->db->update('basket', $data, array('basket_id' => $basket_id));
		if( $return ){
			echo 'Quantity Updated';
		}else{
			echo 'Quantity Cannot be Updated';
		}	
	}

	public function delete_basket(){
		$basket_id = $this->input->post('basket_id');

		$delete = $this->db->delete('basket', array('basket_id' => $basket_id));

		if( $delete ){
			echo $delete;
		}else{
			echo 0;
		}
	}// function

	public function check_pin(){
		if( ! $this->session->userdata['user_id'] ){
			return redirect();
			die();
		}
		$this->load->view('header', array());
		$this->load->view('check_pin', array(
			// 'basket_details'	=> $basket_details
		));
		$this->load->view('footer', array());
	}//function

	public function fetch_franchise(){
		$pin = $this->input->post('pin');
		$query = $this->db
					 ->where( array('franchise_pin' => $pin ) )
					 ->get('franchise');
		$franchises = $query->result_array();

		
		if( $franchises == NULL ){
			$response = array(
				'code' => 0,
				'data' => 'No Franchise',
			);
			echo json_encode($response);
		}else{
			
			$counter = 0;
			foreach( $franchises as $franchise ){
				$array[$counter] = array(
					'name' => $franchise['franchise_name'], 
					'id' => $franchise['franchise_id']
				);
				$counter++;
			}//foreach

			$response = array(
				'code' => 1,
				'data' => $array,
			);
			echo json_encode($response);
		}
	}//function

	public function update_franchise(){
		$user_id = $this->session->userdata['user_id'];
		$franchise_id = $this->input->post('franchise');
		$data = array(
			'franchise_id' => $franchise_id
		);
		$return = $this->db->update('basket', $data, array('user_id' => $user_id));
		if($return){
			return redirect('front/checkout');
		}else{
			return redirect('front/fetch_franchise');
		}
	}
	
	public function checkout(){
		if( ! $this->session->userdata['user_id'] ){
			return redirect('front');
			die();
		}
		$user_id = $this->session->userdata['user_id'];
		$query = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
		$delivery_address = $query->result_array();

		$query1 = $this->db->where( array( 'user_id'=>$user_id, 'status'=>'1' ) )->get('basket');
		$basket_details = $query1->result_array();

		$this->load->view('header', array());
		$this->load->view('checkout', array(
			'delivery_address'		=> $delivery_address,
			'basket_details'	=> $basket_details,
		));
		$this->load->view('footer', array());
	}// function


	public function add_delivery_address(){
		$user_id = $this->session->userdata['user_id'];
		$name = $this->input->post('name');
		$ph_no = $this->input->post('ph_no');
		$pin = $this->input->post('pin');
		$locality = $this->input->post('locality');
		$address = $this->input->post('address');
		$district = $this->input->post('district');
		$state = $this->input->post('state');
		$landmark = $this->input->post('landmark');
		$alt_phone = $this->input->post('alt_phone');


		$data = array(
			'user_id' 		=> $user_id,
			'name'			=> $name,
			'phone'			=> $ph_no,
			'pin'			=> $pin,
			'locality'		=> $locality,
			'address'		=> $address,
			'district'		=> $district,
			'state'			=> $state,
			'landmark'		=> $landmark,
			'alt_phone'		=> $alt_phone,
			'created_by'	=> $user_id,
		);

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';

		$return = $this->db->insert('delivery_address', $data);
		if( $return ){
			return redirect('front/payment');
		}else{
			$this->session->set_flashdata( 'database_error' , 'Sorry! There was some unexpected error!' );
			return redirect('front/checkout');
		}
	}//function
	public function update_delivery_address(){
		$user_id = $this->session->userdata['user_id'];
		$name = $this->input->post('edit_name');
		$ph_no = $this->input->post('edit_ph_no');
		$pin = $this->input->post('edit_pin');
		$locality = $this->input->post('edit_locality');
		$address = $this->input->post('edit_address');
		$district = $this->input->post('edit_district');
		$state = $this->input->post('edit_state');
		$landmark = $this->input->post('edit_landmark');
		$alt_phone = $this->input->post('edit_alt_phone');


		$data = array(
			'name'		=> $name,
			'phone'		=> $ph_no,
			'pin'		=> $pin,
			'locality'		=> $locality,
			'address'		=> $address,
			'district'		=> $district,
			'state'		=> $state,
			'landmark'		=> $landmark,
			'alt_phone'		=> $alt_phone,
			'modified_by'	=> $user_id,
		);
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		$return = $this->db->update('delivery_address', $data, array('user_id' => $user_id));

		if( $return ){
			return redirect('front/payment');
			
		}else{
			$this->session->set_flashdata( 'database_error' , 'Sorry! There was some unexpected error!' );
			return redirect('front/checkout');
		}
	}


	public function payment(){
		if( ! $this->session->userdata['user_id'] ){
			return redirect('front');
			die();
		}

		$user_id = $this->session->userdata['user_id'];
		// $query = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
		// $delivery_address = $query->result_array();

		$query1 = $this->db->where( array( 'user_id'=>$user_id, 'status'=>'1' ) )->get('basket');
		$basket_details = $query1->result_array();


		$this->load->view('header', array());
		$this->load->view('front/payment', array(
			'basket_details'			=> $basket_details,
			//'delivery_address'		=> $delivery_address,
			)
		);

		$this->load->view('footer', array());
	}

	public function place_order(){
		$payment_mode = $this->input->post('payment_mode');
		$user_id = $this->session->userdata['user_id'];
		$query1 = $this->db
					->join('clothes','basket.cloth_id = clothes.cloth_id')
					->where( array('user_id'=>$user_id, 'status'=>'1') )
					->get('basket');
		$basket_details = $query1->result_array();
		$payment_mode = $this->input->post('payment_mode');

		$query = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
		$delivery_address_array = $query->result_array();
		$del_addr_id = $delivery_address_array[0]['del_add_id'];
		// print_r($basket_details);
		//Array ( [0] => Array ( [basket_id] => 1 [user_id] => 1 [cloth_id] => 7 [service] => 2 [cloth_type] => 1 [price] => 15 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-20 20:04:40 [modified_by] => [cloth_name] => Shirt [cloth_slug] => shirt-6 [service_type] => 2 [time] => 92 [icon] => ) [1] => Array ( [basket_id] => 2 [user_id] => 1 [cloth_id] => 3 [service] => 2 [cloth_type] => 3 [price] => 100 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-03 21:46:01 [modified_by] => 1 [cloth_name] => Bed Sheet [cloth_slug] => bed-sheet-2 [service_type] => 2 [time] => 96 [icon] => bed_sheet.svg ) )

		$query2 = $this->db->get('pf_order');
		$no_of_order = $query2->num_rows();
		$order_no = $no_of_order+1;
		$order_code = 'pws-'.$order_no;


		$data = array();
		$counter = 0;
		foreach( $basket_details as $basket ){
			$data[$counter] = array(
				'order_code' =>$order_code,
				'user_id' => $user_id,
				'name'=>$basket['cloth_name'],
				'service_id' => $basket['service'],
				'cloth_type' => $basket['cloth_type'],
				'franchise_id' => $basket['franchise_id'],
				'price_per_unit' =>$basket['price'],
				'quantity'	=>$basket['quantity'],
				'total_price' => $basket['price'] * $basket['quantity'],
				'cloth_status' => '0',
				'payment_mode' => '1',
				'payment_status' => $payment_mode,
				'status' => '1',
				'expected_delivery_date'=> date("Y/m/d", strtotime("+ 3 days")),
				'address_id' => $del_addr_id,
				'basket_id'	=> $basket['basket_id'],
				'created_by'=> $user_id

			);


			$counter++;
		} //foreach

		$return = $this->db->insert_batch('pf_order', $data);
		if( $return ){
			$query3 = $this->db->where( array( 'user_id'=>$user_id) )->get('user');
			$user_details = $query3->result_array();
			$user_slug = $user_details[0]['user_slug']; 

			$data_basket['status'] ='2';
			foreach( $basket_details as $basket ){
				$return1 = $this->db->update('basket', $data_basket, array('basket_id' => $basket['basket_id']));
			}

			$this->session->set_flashdata( 'order_placed' , 'Your Order Has been Placed! ' );
			return redirect("front/account/my_orders/$user_slug");
		}

		 // print_r($data);
		// on succesful order placement redirect to my order page and change the status in cart
	}//function

	// public function place_order_Upay(){
	// 	if( ! $this->session->userdata['user_id'] ){
	// 		return redirect('front');
	// 		die();
	// 	}
	// 	$user_id = $this->session->userdata['user_id'];
	// 	$query1 = $this->db
	// 				->join('clothes','basket.cloth_id = clothes.cloth_id')
	// 				->where( array('user_id'=>$user_id, 'status'=>'1') )
	// 				->get('basket');
	// 	$basket_details = $query1->result_array();


	// 	$total_item = count( $basket_details );
	// 	$delivery_charge = 40;
	// 	$all_total = 0;
	// 	$product_info = '';
	// 	foreach( $basket_details as $basket ){
	// 		$price = $basket['price'];
	// 		$quantity = $basket['quantity'];
	// 		$total = $quantity * $price;
	// 		$all_total = $all_total + $total;
	// 		$product_names= $product_info.' '.$basket['cloth_name']; 

	// 	}
	// 	$grand_total = $all_total + $delivery_charge;

	// 	$query = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
	// 	$delivery_address_array = $query->result_array();
	// 	$del_addr_id = $delivery_address_array[0]['del_add_id'];
	// 	$c_name = $delivery_address_array[0]['name'];
	// 	$c_phone = $delivery_address_array[0]['phone'];
	// 	$c_address = $delivery_address_array[0]['address'].' '.$delivery_address_array[0]['district'].' '.$delivery_address_array[0]['state'].' '.$delivery_address_array[0]['pin'];

	// 	$query = $this->db->where( array('user_id'=>$user_id) )->get('user');
	// 	$user_details = $query->result_array();
	// 	$c_email = $user_details[0]['email'];


	// 	$amount =  $grand_total;
	//     $product_info = $product_names;
	//     $customer_name = $c_name;
	//     $customer_email = $c_email;
	//     $customer_mobile = $c_phone;
	//     $customer_address = $c_address;
	    
 //    	//payumoney details
    
    
 //        $MERCHANT_KEY = "HsJAK1oI"; //change  merchant with yours
 //        $SALT = "ugcVXqkFWl";  //change salt with yours 

 //        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
 //        //optional udf values 
 //        $udf1 = '';
 //        $udf2 = '';
 //        $udf3 = '';
 //        $udf4 = '';
 //        $udf5 = '';
        
 //         $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_email . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
 //         $hash = strtolower(hash('sha512', $hashstring));
         
 //       $success = base_url('front/payment_status') ;  
 //        $fail = base_url('front/payment_status');
 //        $cancel = base_url('front/payment_status');
        
        
 //         $data = array(
 //            'mkey' => $MERCHANT_KEY,
 //            'tid' => $txnid,
 //            'hash' => $hash,
 //            'amount' => $amount,           
 //            'name' => $customer_name,
 //            'productinfo' => $product_info,
 //            'mailid' => $customer_email,
 //            'phoneno' => $customer_mobile,
 //            'address' => $customer_address,
 //            'action' => "https://sandboxsecure.payu.in/", //for live change action  https://secure.payu.in
 //            'sucess' => $success,
 //            'failure' => $fail,
 //            'cancel' => $cancel            
 //        );


	// 	$this->load->view('header', array());
	// 	$this->load->view('front/payumoneyform', $data);

	// 	$this->load->view('footer', array());



	// }

	// public function payment_status(){
	// 	$status = $this->input->post('status');
	// 	$firstname = $this->input->post('firstname');
 //        $amount = $this->input->post('amount');
 //        $txnid = $this->input->post('txnid');
 //        $posted_hash = $this->input->post('hash');
 //        $key = $this->input->post('key');
 //        $productinfo = $this->input->post('productinfo');
 //        $email = $this->input->post('email');
 //        $salt = "dxmk9SZZ9y"; //  Your salt
 //        $add = $this->input->post('additionalCharges');

 //        if (empty($status)) {
 //            redirect('front/my_basket');
 //        }
 //        if( $status == "success" ){
 //        	$payment_mode = $this->input->post('payment_mode');
	// 		$user_id = $this->session->userdata['user_id'];
	// 		$query1 = $this->db
	// 					->join('clothes','basket.cloth_id = clothes.cloth_id')
	// 					->where( array('user_id'=>$user_id, 'status'=>'1') )
	// 					->get('basket');
	// 		$basket_details = $query1->result_array();
	// 		$payment_mode = $this->input->post('payment_mode');

	// 		$query = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
	// 		$delivery_address_array = $query->result_array();
	// 		$del_addr_id = $delivery_address_array[0]['del_add_id'];
	// 		// print_r($basket_details);
	// 		//Array ( [0] => Array ( [basket_id] => 1 [user_id] => 1 [cloth_id] => 7 [service] => 2 [cloth_type] => 1 [price] => 15 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-20 20:04:40 [modified_by] => [cloth_name] => Shirt [cloth_slug] => shirt-6 [service_type] => 2 [time] => 92 [icon] => ) [1] => Array ( [basket_id] => 2 [user_id] => 1 [cloth_id] => 3 [service] => 2 [cloth_type] => 3 [price] => 100 [quantity] => 1 [status] => 1 [franchise_id] => 5 [created_by] => 1 [created_at] => 2020-03-03 21:46:01 [modified_by] => 1 [cloth_name] => Bed Sheet [cloth_slug] => bed-sheet-2 [service_type] => 2 [time] => 96 [icon] => bed_sheet.svg ) )

	// 		$query2 = $this->db->get('pf_order');
	// 		$no_of_order = $query2->num_rows();
	// 		$order_no = $no_of_order+1;
	// 		$order_code = 'pws-'.$order_no;


	// 		$data = array();
	// 		$counter = 0;
	// 		foreach( $basket_details as $basket ){
	// 			$data[$counter] = array(
	// 				'order_code' =>$order_code,
	// 				'user_id' => $user_id,
	// 				'name'=>$basket['cloth_name'],
	// 				'service_id' => $basket['service'],
	// 				'cloth_type' => $basket['cloth_type'],
	// 				'franchise_id' => $basket['franchise_id'],
	// 				'price_per_unit' =>$basket['price'],
	// 				'quantity'	=>$basket['quantity'],
	// 				'total_price' => $basket['price'] * $basket['quantity'],
	// 				'cloth_status' => '0',
	// 				'payment_mode' => '1',
	// 				'payment_status' => $payment_mode,
	// 				'status' => '1',
	// 				'address_id' => $del_addr_id,
	// 				'basket_id'	=> $basket['basket_id'],
	// 				'created_by'=> $user_id

	// 			);


	// 			$counter++;
	// 		} //foreach

	// 		$return = $this->db->insert_batch('pf_order', $data);
	// 		if( $return ){
	// 			$query3 = $this->db->where( array( 'user_id'=>$user_id) )->get('user');
	// 			$user_details = $query3->result_array();
	// 			$user_slug = $user_details[0]['user_slug']; 

	// 			$data_basket['status'] ='2';
	// 			foreach( $basket_details as $basket ){
	// 				$return1 = $this->db->update('basket', $data_basket, array('basket_id' => $basket['basket_id']));
	// 			}

	// 			$this->session->set_flashdata( 'order_placed' , 'Your Order Has been Placed! ' );
	// 			return redirect("front/account/my_orders/$user_slug");
	// 		}
 //        	// update database
 //        	//load success message
 //        }else{
 //        	//load failure message
 //        }
	// } // function
	public function about_us(){
		$query =  $this->db->get('about_us');
		$about_us = $query->result_array()[0];
		$this->load->view('header', array());
		$this->load->view('front/about_us', array(
			'about_us' =>$about_us['content'],
			)
		);

		$this->load->view('footer', array());
	}// function

	public function contact(){
		
		
		$this->load->view('header', array());
		$this->load->view('front/contact', array(
			// 'about_us' =>$about_us['content'],
			)
		);

		$this->load->view('footer', array());
	}// function

	public function faq(){
		$query =  $this->db->get('faq');
		$faqs = $query->result_array();

		$this->load->view('header', array());
		$this->load->view('front/faq', array(
			'faqs' =>$faqs,
			)
		);

		$this->load->view('footer', array());
	}// function

	public function gallery(){
		$query =  $this->db->get('gallery');
		$galleries = $query->result_array();

		$this->load->view('header', array());
		$this->load->view('front/gallery', array(
			'galleries' =>$galleries,
			)
		);

		$this->load->view('footer', array());
	}// function

	public function prices(){
		
		
		$this->load->view('header', array());
		$this->load->view('front/price', array(
			
			)
		);

		$this->load->view('footer', array());
	}// function

	public function franchise_page(){
		$this->load->view('header', array());
		$this->load->view('front/franchise', array(
			
			)
		);

		$this->load->view('footer', array());
	}
}//class