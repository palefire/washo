<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
	}

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){

		$no_of_service = $this->data['no_of_service'];
		$config['base_url'] = site_url('/admin/services');
		$config['total_rows'] = $no_of_service;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		// $query =  $this->db->order_by('service_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('service');
		$query =  $this->db->order_by('service_id','ASC')->get('service');
		$service_details = $query->result_array();
		$pagination = $this->pagination->create_links();

		$this->load->view('header', array());
		$this->load->view('service', array(
			'service_details'=>$service_details,
			'pagination'=>$pagination
		));
		$this->load->view('footer', array());
	} //function


	public function single_service( $service_slug ){
		// echo $service_slug;

		$query = $this->db
				->where( array('service_slug' => $service_slug))
				->get('service');
		$result = $query->result_array();
		$service_id = $result[0]['service_id'];

		$men_query = $this->db
					 ->where( array('service_type' => $service_id, 'cloth_type' => '1') )
					 ->get('clothes');
		$mens_clothes = $men_query->result_array();

		$women_query = $this->db
					 ->where( array('service_type' => $service_id, 'cloth_type' => '2') )
					 ->get('clothes');
		$womens_clothes = $women_query->result_array();

		$household_query = $this->db
					 ->where( array('service_type' => $service_id, 'cloth_type' => '3') )
					 ->get('clothes');
		$household_clothes = $household_query->result_array();

		$kid_query = $this->db
					 ->where( array('service_type' => $service_id, 'cloth_type' => '4') )
					 ->get('clothes');
		$kid_clothes = $kid_query->result_array();

		// echo '<pre>';
		// print_r($kid_clothes);
		// echo '</pre>';


		$this->load->view('header', array());
		$this->load->view('single_service', array(
			'mens_clothes'		=>$mens_clothes,
			'womens_clothes'	=>$womens_clothes,
			'household_clothes'	=>$household_clothes,
			'kid_clothes'		=>$kid_clothes,
			
		));
		$this->load->view('footer', array());
	}
}