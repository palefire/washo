<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
	}

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		$this->load->view('login');
	}

	public function login(){
		$user_name = $this->input->post('username');
		$password =  base64_encode( $this->input->post('password') );

		$q= $this->db
    		->select('*')
    		->from('user')
    		->where("(user.email = '$user_name' OR user.phone = '$user_name')")
    		// ->group_start()
    		// 	->or_where(['user_email'=>$user_name, 'user_phone'=>$user_name])
    		// ->group_end()
			->where(['password'=>$password])
    		->get();

    	if( $q->num_rows() ){
			$user_details =  $q->row();
			// echo $user_details->user_id;
			//print_r($user_details);
			$this->session->set_userdata( 'user_id', $user_details->user_id );
			$this->session->set_userdata( 'user_slug', $user_details->user_slug );
			$this->session->set_userdata( 'user_name', $user_details->name );
			return redirect('');
		}else{
			$this->session->set_flashdata( 'login_failed' , 'User Name or Password does not match!' );
			return redirect('front/account');
		}
	}//function

	public function registration(){
		$email = $this->input->post('email');
		$password = base64_encode( $this->input->post('password') );
		$phone = $this->input->post('phone');
		$name = $this->input->post('name');

		$q = $this->db->where( array('email'=>$email) )->get('user');
		$q1 = $this->db->where( array('phone'=>$phone) )->get('user');
		if( $q->num_rows() >0 ){
			$this->session->set_flashdata( 'email_exist' , 'This Email is Already Registered!' );
			return redirect('front/account');
		}elseif($q1->num_rows() >0){
			$this->session->set_flashdata( 'phone_exist' , 'This Phone Number Is already Registerd !' );
			return redirect('front/account');
		}else{
			$user_slug = strtolower($name); 
			$user_slug = str_replace(' ', '-', $user_slug); // Replaces all spaces with hyphens.
			$user_slug = preg_replace('/[^A-Za-z0-9\-]/', '', $user_slug); // Removes special chars.
			$user_slug = preg_replace('/-+/', '-', $user_slug); // Replaces multiple hyphens with single one.

			$q2 = $this->db->where( array('user_slug'=>$user_slug) )->get('user');
			if( $q2->num_rows() >0 ){
				$user_slug = $user_slug.'-'.$q2->num_rows();
			}
			$data = array(
				'email'	=> $email,
				'password' => $password,
				'user_slug'=>$user_slug,
				'phone'	=> $phone,
				'name'	=> $name,
				'created_by' => 0
			);

			// print_r($data);
			$return = $this->db->insert('user', $data);
			$user_id = $this->db->insert_id();
			if( $return ){
				$this->session->set_userdata( 'user_id', $user_id );
				$this->session->set_userdata( 'user_slug', $user_slug );
				$this->session->set_userdata( 'user_name', $name );
				return redirect('');
			}else{
				$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
				return redirect('front/account');
			}
		}

		

	}//function


	public function logout(){
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_slug');
		$this->session->unset_userdata('user_name');
		
		return redirect( );
	}


	public function my_orders( $user_slug ){
		if( ! $this->session->userdata['user_id'] ){
			return redirect();
			die();
		}

		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'user_id' => $this->session->userdata['user_id'] ) )
		->where("(cloth_status='0' OR cloth_status='1')")
		->order_by('order_id','ASC')
		->get();
		$pending_order = $query->result_array();

		$query1 =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'user_id' => $this->session->userdata['user_id'] ) )
		->where("(cloth_status='2' OR cloth_status='3' OR cloth_status='4' OR cloth_status='5')")
		->order_by('order_id','ASC')
		->get();
		$washing_order = $query1->result_array();

		$query2 =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'user_id' => $this->session->userdata['user_id'] ) )
		->where("(cloth_status='6' )")
		->group_by("order_code")
		->order_by('order_id','ASC')
		->get();
		$complete_order = $query2->result_array();





		$this->load->view('header', array());
		$this->load->view('front/my_orders', array(
			'pending_order' => $pending_order,
			'washing_order' => $washing_order,
			'complete_order' => $complete_order,
		));

		$this->load->view('footer', array());
	}//function


	public function feedback( $order_code ){
		if( ! $this->session->userdata['user_id'] ){
			return redirect();
			die();
		}
		$q = $this->db->where( array('order_code' => $order_code) )->get('feedback');
		$feedback_deatails = $q->result_array();

		$q1 = $this->db
					->select('*')
			        ->from('pf_order')
			        ->join('service','pf_order.service_id = service.service_id')
			        ->where( array( 'order_code' => $order_code ) )
			        ->get();

		$order_detail = $q1->result_array();

		$this->load->view('header', array());
		$this->load->view('front/feedback', array(
			'order_code' 		=> $order_code,
			'feedback_details' 	=> $feedback_deatails,
			'order_detail' 		=> $order_detail,
		));

		$this->load->view('footer', array());
	}

	public function add_feedback(){
		if( ! $this->session->userdata['user_id'] ){
			return redirect();
			die();
		}
		$feedback = $this->input->post('feedback');
		$order_code = $this->input->post('order_code');
		$q1 = $this->db->where( array('user_id' => $this->session->userdata('user_id')) )->get('user');
		$user_details =  $q1->result_array();
		$user_name = $user_details[0]['name'];
		$q = $this->db->where( array('order_code' => $order_code) )->get('feedback');
		if( $q->num_rows() > 0 ){
			//update
			$data = array(
				'feedback' => $feedback,
			);
			$return = $this->db->update('feedback', $data, array('order_code' => $order_code));
			if( $return ){
				$this->session->set_flashdata( 'feedback_submitted' , 'Thank You For submitting Your Valuable Feedback!' );
				return redirect('front/account/my_orders/'.$this->session->userdata('user_slug'));
			}else{
				$this->session->set_flashdata( 'database_error' , 'Sorry! There was some unexpected error!' );
				return redirect('front/account/my_orders/'.$this->session->userdata('user_slug'));
			}
		}else{
			$data = array(
				'order_code' 	=> $order_code,
				'feedback' 		=> $feedback,
				'user_id' 		=> $this->session->userdata('user_id'),
				'customer_name'	=> $user_name,
			);

			$return = $this->db->insert('feedback', $data);
			if( $return ){
				$this->session->set_flashdata( 'feedback_submitted' , 'Thank You For submitting Your Valuable Feedback!' );
				return redirect('front/account/my_orders/'.$this->session->userdata('user_slug'));
			}else{
				$this->session->set_flashdata( 'database_error' , 'Sorry! There was some unexpected error!' );
				return redirect('front/account/my_orders/'.$this->session->userdata('user_slug'));
			}	
		}
		


	}//function

	public function add_complain(){
		if( ! $this->session->userdata['user_id'] ){
			return redirect();
			die();
		}

		$c_order_id = $this->input->post('c_order_id');
		$c_order_code = $this->input->post('c_order_code');
		$c_name = $this->input->post('c_name');
		$c_service_id = $this->input->post('c_service_id');
		$c_cloth_type = $this->input->post('c_cloth_type');
		$c_franchise_id = $this->input->post('c_franchise_id');
		$c_price = $this->input->post('c_price');
		$c_quantity = $this->input->post('c_quantity');
		$c_user_id = $this->input->post('c_user_id');

		$q = $this->db->where( array('c_order_id' => $c_order_id)  )->get('complain');

		if( $q->num_rows()>0 ){
			echo 0;
		}else{
			$data = array(
				'c_order_id' 		=> $c_order_id,
				'c_order_code' 		=> $c_order_code,
				'c_name' 			=> $c_name,
				'c_service_id' 		=> $c_service_id,
				'c_cloth_type' 		=> $c_cloth_type,
				'c_franchise_id' 	=> $c_franchise_id,
				'c_price' 			=> $c_price,
				'c_quantity' 		=> $c_quantity,
				'c_user_id' 		=> $c_user_id,
				'created_by' 		=> $c_user_id,
			);

			$return = $this->db->insert('complain', $data);
			if( $return ){
				$data1['complain_sent'] = '1';
				$return1 = $this->db->update('pf_order', $data1, array('order_id' => $c_order_id));
				echo 1;
			}else{
				echo 9;
			}
		}

		

	}//function
	
}//class