<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Franchise extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
	}

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){

		
		$query =  $this->db->order_by('franchise_id','ASC')->get('franchise');
		$franchise_details = $query->result_array();


		$this->load->view('header', array());
		$this->load->view('franchise', array(
			'franchise_details'=>$franchise_details,
			// 'pagination'=>$pagination
		));
		$this->load->view('footer', array());
	} //function


	// public function single_service( $service_slug ){
	// 	// echo $service_slug;

	// 	$query = $this->db
	// 			->where( array('service_slug' => $service_slug))
	// 			->get('service');
	// 	$result = $query->result_array();
	// 	$service_id = $result[0]['service_id'];

	// 	$men_query = $this->db
	// 				 ->where( array('service_type' => $service_id, 'cloth_type' => '1') )
	// 				 ->get('clothes');
	// 	$mens_clothes = $men_query->result_array();

	// 	$women_query = $this->db
	// 				 ->where( array('service_type' => $service_id, 'cloth_type' => '2') )
	// 				 ->get('clothes');
	// 	$womens_clothes = $women_query->result_array();

	// 	$household_query = $this->db
	// 				 ->where( array('service_type' => $service_id, 'cloth_type' => '3') )
	// 				 ->get('clothes');
	// 	$household_clothes = $household_query->result_array();

	// 	$kid_query = $this->db
	// 				 ->where( array('service_type' => $service_id, 'cloth_type' => '4') )
	// 				 ->get('clothes');
	// 	$kid_clothes = $kid_query->result_array();

	// 	// echo '<pre>';
	// 	// print_r($kid_clothes);
	// 	// echo '</pre>';


	// 	$this->load->view('header', array());
	// 	$this->load->view('single_service', array(
	// 		'mens_clothes'		=>$mens_clothes,
	// 		'womens_clothes'	=>$womens_clothes,
	// 		'household_clothes'	=>$household_clothes,
	// 		'kid_clothes'		=>$kid_clothes,
			
	// 	));
	// 	$this->load->view('footer', array());
	// }
}//class