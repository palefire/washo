<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        $this->apiuser = 'sarasij94';
        $this->apipass = '123';
        $this->response_text = array(0 => 'Fail', 1 => 'Success', 9 => 'Authentication Error', 404 => 'Page Not Found!');
      
	}//constructor

	/*
	|--------------------------------------------------------------------------
	| Login Api
	|--------------------------------------------------------------------------
	*/
	public function login(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
		// if( $all_data['apicredential']['apiuser'] == $this->apiuser AND $all_data['apicredential']['apipass'] == $this->apipass ){
			$username = $all_data->username;
			$password = base64_encode( $all_data->password );
			
			$q= $this->db
	    		->select('*')
	    		->from('user')
	    		->where("(user.email = '$username' OR user.phone = '$username')")
	    		// ->group_start()
	    		// 	->or_where(['user_email'=>$user_name, 'user_phone'=>$user_name])
	    		// ->group_end()
				->where(['password'=>$password])
	    		->get();

	    	if( $q->num_rows() ){
				$user_details =  $q->row();
				
				$array = array(
					"code" => 1,
					"message" => $this->response_text[1],
					'data' => $user_details
				);
				echo json_encode($array);
			}else{
				
				$array = array(
					"code" => 0,
					"message" => $this->response_text[0],
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//function


	/*
	|--------------------------------------------------------------------------
	| Login Api
	|--------------------------------------------------------------------------
	*/
	public function registration(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
		// if( $all_data['apicredential']['apiuser'] == $this->apiuser AND $all_data['apicredential']['apipass'] == $this->apipass ){
			$email = $all_data->email;
			$password = base64_encode( $all_data->password );
			$phone = $all_data->phone;
			$name = $all_data->name;

			$user_slug = strtolower($name); 
			$user_slug = str_replace(' ', '-', $user_slug); // Replaces all spaces with hyphens.
			$user_slug = preg_replace('/[^A-Za-z0-9\-]/', '', $user_slug); // Removes special chars.
			$user_slug = preg_replace('/-+/', '-', $user_slug); // Replaces multiple hyphens with single one.

			$data = array(
				'email'	=> $email,
				'password' => $password,
				'user_slug'=>$user_slug,
				'phone'	=> $phone,
				'name'	=> $name,
				'created_by' => 1
			);

			// print_r($data);
			$return = $this->db->insert('user', $data);
			$user_id = $this->db->insert_id();

	    	if( $return ){
				
				
				$array = array(
					"code" => 1,
					"message" => $this->response_text[1],
					'user_id' => $user_id
				);
				echo json_encode($array);
			}else{
				
				$array = array(
					"code" => 0,
					"message" => $this->response_text[0],
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
		echo json_encode($array);
		}
	}//function




	/*
	|--------------------------------------------------------------------------
	| Service Api
	|--------------------------------------------------------------------------
	*/
	public function service(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
			$user_id = $all_data->user_id;
			$q3 = $this->db->where( array('user_id'=>$user_id, 'status'=>'1' ) )->get('basket');
	        $no_of_basket = $q3->num_rows();			

			$q1 = $this->db->get('service');
			$service_details = $q1->result_array();
			// $data = array();
			$array_counter = 0;
			foreach ($service_details as $service_detail) {
				$data[$array_counter] = array(
					'service_id'	=> $service_detail['service_id'],
					'service_name'	=> $service_detail['service_name'],
					'service_svg'	=> $this->data['svg_path'].$service_detail['svg'],
					'service_image'	=> site_url('/uploads/services/').$service_detail['img'],
				);
				$array_counter++;
			}
			$data1 = array();
			$query =  $this->db
			->select('*')
			->from('offers')
			->get();
			$offers = $query->result_array();

			$array_counter1 = 0;
			foreach ($offers as $offer) {
				
				$data1[$array_counter1] = array(
					'offer_id' => $offer['offers_id'],
					'offer_title' => $offer['offer_title'],
					'offer_image' => site_url('uploads/offer/').$offer['offer_image'],

				);
				$array_counter1++;
			}//foreach
			$array = array(
				"code" => 1,
				"message" => $this->response_text[1],
				'data' => $data,
				"basket" =>intval($no_of_basket),
				"offer"	 =>$data1,
			);

			echo json_encode($array);

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
		echo json_encode($array);
		}
	}//function


	/*
	|--------------------------------------------------------------------------
	| Clothes Api
	|--------------------------------------------------------------------------
	*/
	public function clothes(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
			
			
			if( isset($all_data->service_id) ){
				$service_id = $all_data->service_id;
				$query = $this->db
					 ->where( array('service_type' => $service_id) )
					 ->get('clothes');

				$results = $query->result_array();
				$data = array();
				$array_counter = 0;
					foreach ($results as $result) {
						$data[$array_counter] = array(
							'cloth_id'		=> $result['cloth_id'],
							'cloth_name'	=> $result['cloth_name'],
							'cloth_type'	=> $result['cloth_type'],
							'service_id'	=> $result['service_type'],
							'price'			=> $result['price'],
							'time'			=> $result['time'],
							'icon'	=> $this->data['svg_path'].$result['icon'],
						);
						$array_counter++;
					}



				$cloth_type = array( ['id'=>1,'name'=>'Man'], ['id'=>2,'name'=>'Woman'],['id'=>3,'name'=>'Households'],['id'=>4,'name'=>'Kids'] );

				$array = array(
					"code" => 1,
					"message" => $this->response_text[1],
					'data' => $data,
					'cloth_type' => $cloth_type 
				);

				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => $this->response_text[0]
				);
				echo json_encode($array);
			}
			

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//function




	/*
	|--------------------------------------------------------------------------
	| Basketi
	|--------------------------------------------------------------------------
	*/
	public function add_to_basket(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){

			$user_id = $all_data->user_id;
			$service_id = $all_data->service_id;
			// $clothes
			// $cloth_id = $all_data->clothes->cloth_id;
			// $cloth_name = $all_data->clothes->cloth_name;
			// $cloth_type = $all_data->clothes->cloth_type;
			// $price = $all_data->clothes->price;
			// $time = $all_data->clothes->time;
			// $quantity = $all_data->clothes->quantity;
			// $icon = $all_data->clothes->icon;
			$all_clothes = $all_data->clothes;
			foreach( $all_clothes as $clothes ){
				$data = array(
					'user_id'  		=> $user_id,
					'service'  		=> $service_id,
					'cloth_id'		=> $clothes->cloth_id,
					'cloth_type'  	=> $clothes->cloth_type,
					'price'  		=> $clothes->price,
					'quantity'  	=> $clothes->quantity,
					'status'  		=> "1",
					'created_by'	=> $user_id
				);

				 $return = $this->db->insert('basket', $data);
			}
				
			if( $return ){
	            $q3 = $this->db->where( array('user_id'=>$user_id, 'status'=>'1' ) )->get('basket');
	            $no_of_basket = $q3->num_rows();
	            $array = array(
					"code" => 1,
					"message" => $this->response_text[1],
					"data"	=> intval($no_of_basket)
				);
				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => $this->response_text[0]
				);
				echo json_encode($array);
			}
			
			
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//function

	public function basket(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){

			$user_id = $all_data->user_id;
			$query = $this->db
					->join('clothes','basket.cloth_id = clothes.cloth_id')
					->join('service','basket.service = service.service_id')
					->where( array('user_id'=>$user_id, 'status'=>'1') )
					->get('basket');
			$basket_details = $query->result_array();
			$data = array();
			$count = 0;
			foreach( $basket_details as $basket_detail ){
				$data[$count] = array(
					'cloth_id'		=> $basket_detail['cloth_id'],
					'basket_id' => $basket_detail['basket_id'],
					'name'	=>$basket_detail['cloth_name'],
					'cloth_type_id' => $basket_detail['cloth_type'],
					'cloth_type_name'	=> $this->data['cloth_type'][$basket_detail['cloth_type']],
					'service_id'	=> $basket_detail['service'],
					'service_name'	=> $basket_detail['service_name'],
					'time'=>$basket_detail['time'],
					'price'=>$basket_detail['price'],
					'quantity'=>$basket_detail['quantity'],
					'icon'	=> $this->data['svg_path'].$basket_detail['icon']
				);

				$count++;
			}

			$query1 = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
			$delivery_address_result = $query1->result_array();
			if($delivery_address_result == NULL){
				$delivery_address = array();
			}else{
				$delivery_address = $delivery_address_result;
			}

			$array = array(
						"code" => 1,
						"message" => $this->response_text[1],
						"data"	=> $data,
						"address" => $delivery_address,
						"state" =>$this->data['state']
					);
			echo json_encode($array);
			
			
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//function

	public function delete_basket(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){

			$basket_id = $all_data->basket_id;
			$user_id = $all_data->user_id;
			$delete = $this->db->delete('basket', array('basket_id' => $basket_id));
			if( $delete ){
				$q3 = $this->db->where( array('user_id'=>$user_id, 'status'=>'1' ) )->get('basket');
	            $no_of_basket = $q3->num_rows();
	            $array = array(
					"code" => 1,
					"message" => $this->response_text[1],
					"data"	=> intval($no_of_basket)
				);
				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => $this->response_text[0]
				);
				echo json_encode($array);	
			}
			
			
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//function


	/*
	|--------------------------------------------------------------------------
	| Franchise Api
	|--------------------------------------------------------------------------
	*/
	public function franchise(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
			$pin = $all_data->pin;
			$q3 = $this->db->where( array('franchise_pin'=>$pin ) )->get('franchise');
	        $franchise_list = $q3->result_array();			

			
			// $data = array();
			$data=array();
			$array_counter = 0;
			foreach ($franchise_list as $franchise) {
				$data[$array_counter] = array(
					'franchise_id'	=> $franchise['franchise_id'],
					'franchise_name'	=> $franchise['franchise_name'],
				);
				$array_counter++;
			}
			$array = array(
				"code" => 1,
				"message" => $this->response_text[1],
				'data' => $data,
				
			);

			echo json_encode($array);

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
		echo json_encode($array);
		}
	}//function


	/*
	|--------------------------------------------------------------------------
	| Franchise Api
	|--------------------------------------------------------------------------
	*/
	public function place_order(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){

			$user_id = $all_data->user_id;
			$name = $all_data->address->name;
			$ph_no = $all_data->address->phone;
			$pin = $all_data->address->pin;
			$locality = $all_data->address->locality;
			$address = $all_data->address->address;
			$district = $all_data->address->district;
			$state = $all_data->address->state;
			$landmark = $all_data->address->landmark;
			$alt_phone = $all_data->address->altPhone;
			$alt_phone = $all_data->address->altPhone;

			
			$query = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
			$delivery_address = $query->result_array();

			if( $delivery_address == null ){
				$data = array(
					'user_id' 		=> $user_id,
					'name'			=> $name,
					'phone'			=> $ph_no,
					'pin'			=> $pin,
					'locality'		=> $locality,
					'address'		=> $address,
					'district'		=> $district,
					'state'			=> $state,
					'landmark'		=> $landmark,
					'alt_phone'		=> $alt_phone,
					'created_by'	=> $user_id,
				);
				$return = $this->db->insert('delivery_address', $data);
				
			}else{
				$data = array(
					'user_id' 		=> $user_id,
					'name'			=> $name,
					'phone'			=> $ph_no,
					'pin'			=> $pin,
					'locality'		=> $locality,
					'address'		=> $address,
					'district'		=> $district,
					'state'			=> $state,
					'landmark'		=> $landmark,
					'alt_phone'		=> $alt_phone,
					'modified_by'	=> $user_id,
				);
				$return = $this->db->update('delivery_address', $data, array('user_id' => $user_id));
				
			}// if 


			$query2 = $this->db->get('pf_order');
			$no_of_order = $query2->num_rows();
			$order_no = $no_of_order+1;
			$order_code = 'pws-'.$order_no;

			$query3 = $this->db->where( array('user_id'=>$user_id) )->get('delivery_address');
			$delivery_address_array = $query3->result_array();
			$del_addr_id = $delivery_address_array[0]['del_add_id'];

			$basket_details =  $all_data->cloths;			
			// print_r($basket_details);
			$order = array();
			$counter = 0;
			foreach( $basket_details as $basket ){
				$order[$counter] = array(
					'order_code' =>$order_code,
					'user_id' => $user_id,
					'name'=>$basket->cloth_name,
					'service_id' => $basket->service_id,
					'cloth_type' => $basket->cloth_type_id,
					'franchise_id' => $basket->franchise_id,
					'price_per_unit' =>$basket->price,
					'quantity'	=>$basket->quantity,
					'total_price' => $basket->price * $basket->quantity,
					'cloth_status' => '0',
					'payment_mode' => '1',
					'payment_status' => '0',
					'status' => '1',
					'expected_delivery_date'=> date("Y/m/d", strtotime("+ 3 days")),
					'address_id' => $del_addr_id,
					'basket_id'	=> $basket->basket_id,
					'created_by'=> $user_id

				);


				$counter++;
			} //foreach
			// print_r($order);
			$return1 = $this->db->insert_batch('pf_order', $order);

			if( $return && $return1 ){
				$data_basket['status'] ='2';
				$data_basket['franchise_id'] = $basket->franchise_id;
				foreach( $basket_details as $basket ){
					$return1 = $this->db->update('basket', $data_basket, array('basket_id' => $basket->basket_id));
				}
				$array = array(
					"code" => 1,
					"message" => $this->response_text[1],
					
				);
			}else{
				$array = array(
					"code" => 0,
					"message" => $this->response_text[0],
					
				);
			}

			echo json_encode($array);
			

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
		echo json_encode($array);
		}
	}//function

		/*
	|--------------------------------------------------------------------------
	| Franchise Api
	|--------------------------------------------------------------------------
	*/
	public function my_orders(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
			$user_id = $all_data->user_id;


			$query =  $this->db
			->select('*')
			->from('pf_order')
			->join('service','pf_order.service_id = service.service_id')
			->where( array( 'user_id' => $user_id ) )
			->order_by('order_id','ASC')
			->get();
			$my_orders = $query->result_array();

			//"order_id":"1","order_code":"pws-1","user_id":"1","name":"Shirt","service_id":"2","cloth_type":"1","franchise_id":"1","price_per_unit":"15","quantity":"1","total_price":"15","cloth_status":"6","payment_mode":"1","transaction_id":null,"payment_status":"2","status":"1","order_date":"2020-03-2520:05:26","delivery_date":"2020-03-25 23:09:42","expected_delivery_date":"2020-04-2023:48:05","pick_up_biker_id":"1","warehouse_id":"1","delivery_biker_id":"1","address_id":"1","basket_id":"10","complain":"0","complain_sent":"1","created_by":"1","created_at":"2020-03-2012:03:08","modified_by":"1","service_name":"SteamIron","service_slug":"steam-iron-1","svg":"iron.svg","img":"slide1-2.jpg"
			$data=array();
			$array_counter = 0;
			foreach ($my_orders as $my_order) {
				$query =  $this->db
					->select('*')
					->from('clothes')
					->where( array( 'cloth_name' => $my_order['name'], 'service_type' => $my_order['service_id'] , 'cloth_type' => $my_order['cloth_type']) )
					->get();
				$clothes_details = $query->result_array();	

				$data[$array_counter] = array(
					'order_id' => $my_order['order_id'],
					'name' => $my_order['name'],
					'quantity' => $my_order['quantity'],
					'price_per_unit' => $my_order['price_per_unit'],
					'total_price' => $my_order['total_price'],
					'order_date' => $my_order['order_date'],
					'delivery_date' => $my_order['delivery_date'],
					'service_name' => $my_order['service_name'],
					'status' => $my_order['cloth_status'],
					'cloth_type' => $this->data['order_cloth_type'][$my_order['cloth_type']],
					'icon' => $this->data['svg_path'].$clothes_details[0]['icon'],

				);
				$array_counter++;
			}//foreach

			$array = array(
				"code" => 1,
				"message" => $this->response_text[1],
				'data' => $data,
			);

			echo json_encode($array);

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
		echo json_encode($array);
		}//if
	}//function

	public function offers(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
			


			$query =  $this->db
			->select('*')
			->from('offers')
			->get();
			$offers = $query->result_array();

			$array_counter = 0;
			foreach ($offers as $offer) {
				
				$data[$array_counter] = array(
					'offer_id' => $offer['offers_id'],
					'offer_title' => $offer['offer_title'],
					'offer_image' => site_url('uploads/offer/').$offer['offer_image'],

				);
				$array_counter++;
			}//foreach

			$array = array(
				"code" => 1,
				"message" => $this->response_text[1],
				'data' => $data,
			);

			echo json_encode($array);

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
		echo json_encode($array);
		}//if
	}//function


	public function about_us(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";

		if( $all_data->apicredential->apiuser == $this->apiuser AND $all_data->apicredential->apipass == $this->apipass ){
			
			$query =  $this->db->get('about_us');
			$about_us = $query->result_array()[0];

			$array = array(
				"code" => 1,
				"message" => $this->response_text[1],
				'data' => $about_us,
			);

			echo json_encode($array);

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
		echo json_encode($array);
		}//if
	}//function
	/*
	|--------------------------------------------------------------------------
	| The End
	|--------------------------------------------------------------------------
	*/

}//class