<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends MY_Controller{
	function __construct(){
        parent::__construct();
    }//contructor

    public function assign_pickup(){
    	$order_code = $this->input->post('order_code');
    	$biker_id = $this->input->post('biker_id');

    	$order_data = array(
    		'cloth_status' => '1',
    		'pick_up_biker_id' => $biker_id,
    	);

    	

    	$return = $this->db->update('pf_order', $order_data, array( 'order_code' => $order_code, 'franchise_id' => $this->session->userdata['franchise_id'] ));
    	if( $return ){
			$this->session->set_flashdata( 'pickup_assigned' , 'Biker Assigned! ' );
			return redirect("franchise/pickup_assigned");
    		
    	}else{
    		$this->session->set_flashdata( 'assign_failed' , 'Cannot Upadate the Order! Check After Some Time' );
    		return redirect("franchise/view_pending/$order_code");
    	}
    }//function

    public function assign_warehouse(){
        $order_code = $this->input->post('order_code');
        $warehouse_id = $this->input->post('warehouse_id');

        $order_data = array(
            'cloth_status' => '3',
            'warehouse_id' => $warehouse_id,
        );

        

        $return = $this->db->update('pf_order', $order_data, array( 'order_code' => $order_code, 'franchise_id' => $this->session->userdata['franchise_id'] ));
        if( $return ){
            $this->session->set_flashdata( 'warehouse_assigned' , 'Warehouse Assigned! ' );
            return redirect("franchise/in_warehouse");
        }else{
            $this->session->set_flashdata( 'warehouse_assign_failed' , 'Cannot Upadate the Order! Check After Some Time' );
            return redirect("franchise/view_picked_up/$order_code");
        }
    }//function

    public function wash_complete(){
        $order_code = $this->input->post('order_code');
        

        $order_data = array(
            'cloth_status' => '4',
        );

        $return = $this->db->update('pf_order', $order_data, array( 'order_code' => $order_code, 'franchise_id' => $this->session->userdata['franchise_id'] ));
        if( $return ){
            $this->session->set_flashdata( 'wash_complete' , 'Wash Complete! ' );
            return redirect("franchise/washed");
        }else{
            $this->session->set_flashdata( 'wash_failed' , 'Cannot Upadate the Order! Check After Some Time' );
            return redirect("franchise/view_in_warehouse/$order_code");
        }
    }//function


    public function assign_delivery(){
        $order_code = $this->input->post('order_code');
        $biker_id = $this->input->post('biker_id');

        $order_data = array(
            'cloth_status' => '5',
            'delivery_biker_id' => $biker_id,
        );

       

        $return = $this->db->update('pf_order', $order_data, array( 'order_code' => $order_code, 'franchise_id' => $this->session->userdata['franchise_id'] ));
        if( $return ){
            $this->session->set_flashdata( 'delivery_assigned' , 'Biker Assigned! ' );
            return redirect("franchise/delivery_assigned");
            
        }else{
            $this->session->set_flashdata( 'assign_failed' , 'Cannot Upadate the Order! Check After Some Time' );
            return redirect("franchise/view_pending/$order_code");
        }
    }//function

}//class