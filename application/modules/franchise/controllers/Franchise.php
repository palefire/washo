<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Franchise extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        $this->load->model('ordermodel', 'Order');


        if( isset($this->session->userdata['franchise_id']) ){

        	$q4 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id']) )->get('biker'); 
	        $this->data['no_of_biker_by_franchise'] = $q4->num_rows();

        	$q5 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '2') )->get('pf_order');
	        $this->data['no_of_picked_up_orders'] = $q5->num_rows();

	        $q9 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '3') )->get('pf_order');
	        $this->data['no_of_orders_with_warehouse'] = $q9->num_rows();

	        $q10 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '4') )->get('pf_order');
	        $this->data['no_of_washed_clothes'] = $q10->num_rows();

	        $q11 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '5') )->get('pf_order');
	        $this->data['no_of_delivery_assigned_orders'] = $q11->num_rows();

        	$q6 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '1') )->get('pf_order');
	        $this->data['no_of_pickup_assigned_orders'] = $q6->num_rows();

        	$q7 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id']) )->get('pf_order');
	        $no_of_orders = $q7->num_rows();
	        $this->data['no_of_orders'] = $no_of_orders;

	        $q8 = $this->db->where( array('franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '0') )->get('pf_order');
	        $no_of_pending_orders = $q8->num_rows();
	        $this->data['no_of_pending_orders'] = $no_of_pending_orders;
        }
        
      
	}//constructor

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		if( isset($this->session->userdata['franchise_id']) ){
			return redirect('franchise/dashboard');
			
		}
		$this->load->view('login');
	}//function

	public function verify_login(){
		$username = $this->input->post('username');
		$password_decode = $this->input->post('password');
		$password = base64_encode( $password_decode );

		$q = $this->db->where( array('username'=>$username, 'password'=>$password) )->get('franchise');
		if( $q->num_rows() ){
			$franchise_details =  $q->row();
			// echo $admin_details->admin_id;
			//print_r($admin_details);
			$this->session->set_userdata( 'franchise_id', $franchise_details->franchise_id );
			$this->session->set_userdata( 'franchise_pin', $franchise_details->franchise_pin );
			$this->session->set_userdata( 'franchise_name', $franchise_details->franchise_name );
			return redirect('franchise/dashboard');
		}else{
			$this->session->set_flashdata( 'login_failed' , 'User Name or Password does not match!' );
			return redirect('franchise');
		}
	}//function

	public function logout(){
		$this->session->unset_userdata('franchise_id');
		$this->session->unset_userdata('franchise_pin');
		$this->session->unset_userdata('franchise_name');
		
		return redirect('franchise' );
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function dashboard(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}

		$q1 = $this->db->where(array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '2'))->get('pf_order');
        $assign_warehouse = $q1->num_rows();

        $q2 = $this->db->where(array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '3'))->get('pf_order');
        $collect_from_warehouse = $q2->num_rows();

        $q3 = $this->db->where(array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '4'))->get('pf_order');
        $assign_delivery = $q3->num_rows();

        $q4 = $this->db->where("`cloth_status` <> '6'")->where(array('franchise_id'=>$this->session->userdata['franchise_id'], 'expected_delivery_date' => date('Y/m/d')))->get('pf_order');
        $today_orders = $q4->result_array();

        $q5 = $this->db->where("`cloth_status` <> '6'")->where(array('franchise_id'=>$this->session->userdata['franchise_id'], 'expected_delivery_date' => date("Y/m/d", strtotime("+ 1 days")) ))->get('pf_order');
        $tomorrow_orders = $q5->result_array();

        $q6 = $this->db->where("`cloth_status` <> '6'")->where(array('franchise_id'=>$this->session->userdata['franchise_id'], 'expected_delivery_date' => date("Y/m/d", strtotime("+ 2 days")) ))->get('pf_order');
        $dayafter_orders = $q6->result_array();
        $date = date("Y-m-d H:i:s");
        $date_sql = "`expected_delivery_date` < '".$date."'";

        $q7 = $this->db->where("`cloth_status` <> '6'")->where(array('franchise_id'=>$this->session->userdata['franchise_id'] ))->where($date_sql)->get('pf_order');
        $due_orders = $q7->result_array();

        // print_r($due_orders);
        
		$this->load->view('franchise', array(
			'pending_order' 	 		=> $this->data['no_of_pending_orders'],
			'assign_warehouse' 			=> $assign_warehouse,
			'collect_from_warehouse' 	=> $collect_from_warehouse,
			'assign_delivery' 			=> $assign_delivery,
			'today_orders'				=> $today_orders,
			'tomorrow_orders'			=> $tomorrow_orders,
			'dayafter_orders'			=> $dayafter_orders,	
			'due_orders'				=> $due_orders,	

		));
	}//function
	/*
	|--------------------------------------------------------------------------
	| Order function
	|--------------------------------------------------------------------------
	*/

	public function order_list(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_orders = $this->data['no_of_orders'];
		$config['base_url'] = site_url('/franchise/order_list');
		$config['total_rows'] = $no_of_orders;
		$config['per_page'] = 4;

		$this->pagination->initialize($config);
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'franchise_id' => $this->session->userdata['franchise_id'] ) )
		->order_by('order_id','ASC')
		->limit($config['per_page'],$this->uri->segment(3))
		->get();
		$order_details = $query->result_array();
		$pagination = $this->pagination->create_links();


		$query1 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id']) )->get('pf_order');
		
		$no_of_order = $query1->num_rows();

		$this->load->view('order', array(
				'order_details'=>$order_details,
				'pagination'=>$pagination,
				'no_of_order'	=> $no_of_order
			)
		);
	}//function

	public function view_order( $order_id ){
		$order_detail = $this->Order->get_order_by_id( $order_id );

		$query = $this->db->where( array( 'biker_status'=> '0' ) )->get('biker');
		$bikers = $query->result_array();

		$this->load->view('view_order', array(
				'order_detail'	=>$order_detail,
				'bikers'			=> $bikers
			)
		);
		

	}//function

	public function pending_order(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_pending_orders = $this->data['no_of_pending_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->join('delivery_address','pf_order.user_id = delivery_address.user_id')
		->where( array( 'franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '0' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('pending', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_pending_orders
			)
		);
	}//function

	public function view_pending( $order_code ){
		$order_detail = $this->Order->get_order_by_code( $order_code );

		$query = $this->db->where( array( 'franchise_id'=> $this->session->userdata['franchise_id']) )->get('biker');
		$bikers = $query->result_array();
		// print_r($order_detail);
		$this->load->view('view_pending', array(
				'order_detail'	=>$order_detail,
				'bikers'			=> $bikers
			)
		);
		

	}//function
	public function print(){
		$order_code = $this->input->post('order_code');
		$order_details = $this->Order->get_order_by_code( $order_code );

		$this->load->library('pdflib');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nicola Asuni');
		$pdf->SetTitle('TCPDF Example 001');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('dejavusans', '', 14, '', true);
		$pdf->AddPage();
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
		$html = "";
		foreach( $order_details as $order ){
			$html .= '
						<p>Order Id : '.$order["order_id"].'</p>
						<p>Order Date : '.date( "d-m-Y" ,strtotime( $order["order_date"] ) ).'</p>
						<p>DOP : '.date( "d-m-Y" ).'</p>
						<p>G Name : '.$order["name"].' ('.$this->data['order_cloth_type'][ $order['cloth_type'] ].')</p>
						<p>S Code : '.$order["service_id"].' </p>
						<p>Ph No : '.$order["phone"].' </p>
						<p>Service : '.$order["service_name"].' </p>
						<p>Order Code : '.$order["order_code"].' </p>
						<p style="text-align:center"> ----------------------------------------</p>
						
			';
		}
		$html .= '
		<p style="text-align:center"> -----------------------XXXXXXX---------------------------</p>
		';
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
		$pdf->Output('token.pdf', 'I');
	}//fn


	public function pickup_assigned(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_pickup_assigned_orders = $this->data['no_of_pickup_assigned_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
		->where( array( 'pf_order.franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '1' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('assigned_pickup', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_pickup_assigned_orders
			)
		);
	}//function

	public function view_pickup_assigned( $order_code ){

		 $query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('view_pickup_assigned', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function

	public function picked_up(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_picked_up_orders = $this->data['no_of_picked_up_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '2' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('picked_up', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_picked_up_orders
			)
		);
	}//function

	public function view_picked_up( $order_code ){

		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );
		$query1 = $this->db->get('warehouse');
		$warehouses = $query1->result_array();

		$this->load->view('view_picked_up', array(
				'order_detail'	=>$order_detail,
				'warehouses'		=> $warehouses
				
			)
		);
		

	}//function


	public function in_warehouse(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_picked_up_orders = $this->data['no_of_picked_up_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '3' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('in_warehouse', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_picked_up_orders
			)
		);
	}//function

	public function view_in_warehouse( $order_code ){

		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('warehouse','pf_order.warehouse_id = warehouse.warehouse_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('view_in_warehouse', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function


	public function washed(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_picked_up_orders = $this->data['no_of_picked_up_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '4' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('washed', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_picked_up_orders
			)
		);
	}//function

	public function view_washed( $order_code ){

		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->join('warehouse','pf_order.warehouse_id = warehouse.warehouse_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );

		$query1 = $this->db->where( array('franchise_id'=> $this->session->userdata['franchise_id']) )->get('biker');
		$bikers = $query1->result_array();


		$this->load->view('view_washed', array(
				'order_detail'	=>$order_detail,
				'bikers'			=> $bikers
			)
		);
		

	}//function


	public function print_bill(){
		$order_code = $this->input->post('order_code');
		$this->load->library('pdflib');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Palefire');
		$pdf->SetTitle('Washo Bill');
		$pdf->SetSubject('Washo Bill Details');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('dejavusans', '', 14, '', true);
		$pdf->AddPage();
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

		
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        // ->join('delivery_address','pf_order.address_id = delivery_address.del_add_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();
        $order_detail = $query->result_array();

        $user_id = $order_detail[0]['user_id'];
        $address_id = $order_detail[0]['address_id'];

        $query1 =  $this->db
        ->select('*')
        ->from('delivery_address')
        // ->join('service','pf_order.service_id = service.service_id')
        // ->join('delivery_address','pf_order.address_id = delivery_address.del_add_id')
        ->where( array( 'del_add_id' => $address_id ) )
        ->get();

        $address_details = $query1->result_array();
        $user_name = $address_details[0]['name'];
        $address = $address_details[0]['address'];
        $pin = $address_details[0]['pin'];
        $district = $address_details[0]['district'];

        $order_details = '';
        $total = 0;
        foreach( $order_detail as $order ){

        	$cloth_name = $order['name'];
			$quantity = $order['quantity'];
			$amount = $order['total_price'];
			$rate =  $amount / $quantity; 
			$order_details .= '<tr>
								<td>'.$cloth_name.'</td>
								<td>'.$quantity.'</td>
								<td>'.$rate.'</td>
								<td>'.$amount.'</td>
							 </tr>';

			$total = $total + $amount;
        }
        $html = '
		<p style="text-align:center">Bill</p>
		<p style="text-align:center"> -----</p>
			
		';
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

		$tbl = '<table cellspacing="0" cellpadding="1">
				    <tr>
				        <td >From, <br />Washo <br />1, Gobinda Niwas, <br /> Kolkata 700089</td>
				        <td></td>
				        <td>Order Code -'.$order_code.'</td>
				    </tr>   
				</table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');

		$tbl = '<table cellspacing="0" cellpadding="1" style="margin-bottom:30px">
				    <tr>
				        <td >To,  <br />'.$user_name.'<br />'.$address.',<br />'.$district.' - '.$pin.'</td>
				        <td ></td>
				        <td > Date : Apr 20, 2020</td>
				        
				    </tr>   
				</table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');

		$html = '
		
		<p style="text-align:center">   </p>
			
		';
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



		$tbl = '<table cellspacing="0" cellpadding="1" border="0" >
				    <tr>
				        <th>Description</th>
				        <th>Quantity</th>
				        <th>Rate</th>
				        <th>Amount</th>
				    </tr>     
				'.$order_details.'</table>';

				

		$pdf->writeHTML($tbl, true, false, false, false, '');
	


		$tbl = '<table cellspacing="0" cellpadding="1" >
				    <tr>
				        <th></th>
				        <th></th>
				        <th>Total</th>
				        <th>'.$total.'</th>
				    </tr>     
				</table>
				
				';

		$pdf->writeHTML($tbl, true, false, false, false, '');


		$pdf->Output('bill.pdf', 'I');
	}//fn



	public function delivery_assigned(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_pickup_assigned_orders = $this->data['no_of_pickup_assigned_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '5' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('assigned_delivery', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_pickup_assigned_orders
			)
		);
	}//function

	public function view_delivery_assigned( $order_code ){

		 $query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('view_delivery_assigned', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function






	public function complete(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_picked_up_orders = $this->data['no_of_picked_up_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'franchise_id' => $this->session->userdata['franchise_id'], 'cloth_status' => '6' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('complete', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_picked_up_orders
			)
		);
	}//function

	public function view_complete( $order_code ){

		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('view_complete', array(
				'order_detail'	=>$order_detail,
				
			)
		);		

	}//function

	public function complain(){

		$q = $this->db
				->select('*')
				->from('complain')
				->join('franchise','complain.c_franchise_id = franchise.franchise_id')
				->where( array( 'c_status' => '0', 'c_franchise_id'=>$this->session->userdata['franchise_id'] ) )
				->get();
		$complain_details = $q->result_array();

		$this->load->view('complain', array(
				'complain_details'=>$complain_details,
			)
		);
	}//function


	public function accept_complain(){
		$complain_id = $this->input->post('complain_id');
		$query =  $this->db
					->select('*')
					->from('complain')
					->where(array( "complain_id"=>$complain_id ))
					->get();
		$complain_details =$query->result_array();
		$order_id = $complain_details[0]['c_order_id'];

		$query2 = $this->db->get('pf_order');
		$no_of_order = $query2->num_rows();
		$order_no = $no_of_order+1;
		$order_code = 'pws-'.$order_no;

		$query1 =  $this->db
						->select('*')
						->from('pf_order')
						->where( array('order_id'=>$order_id))
						->get();
		$order_details = $query1->result_array();

		$order_data = array(
			'order_code'				=> $order_code,
			'user_id'	 				=> $order_details[0]['user_id'],
			'name'						=> $order_details[0]['name'],
			'service_id'				=> $order_details[0]['service_id'],
			'cloth_type'				=> $order_details[0]['cloth_type'],
			'franchise_id'				=> $order_details[0]['franchise_id'],
			'price_per_unit'			=> 0,
			'quantity'	 				=> $order_details[0]['quantity'],
			'total_price'				=> 0,
			'cloth_status'				=> '0',
			'payment_mode'				=> $order_details[0]['payment_mode'],
			'payment_status'			=> '2',
			'status	'	  				=> '1',
			'expected_delivery_date'	=> date("Y/m/d", strtotime("+ 3 days")),
			'address_id'	 			=> $order_details[0]['address_id'],	
			'basket_id'	 				=> $order_details[0]['basket_id'],	
			'complain'					=> '1',
			'created_by'				=> $order_details[0]['created_by']
		);
		$insert = $this->db->insert('pf_order', $order_data);
		if( $insert ){
				$data['c_status'] = '1';
				$return = $this->db->update('complain', $data, array('complain_id'=>$complain_id));
				echo 1;
		}else{
			echo 0;
		}

	}//function

	public function reject_complain(){
		$complain_id = $this->input->post('complain_id');
		$data['c_status'] = '5';
		$return = $this->db->update('complain', $data, array('complain_id'=>$complain_id));
		if( $return ){
			echo 1;
		}else{
			echo 0;
		}
	}//function

	public function accepted_complain(){
		$q = $this->db
				->select('*')
				->from('complain')
				->join('franchise','complain.c_franchise_id = franchise.franchise_id')
				->where( array( 'c_status' => '1', 'c_franchise_id'=>$this->session->userdata['franchise_id'] ) )
				->get();
		$complain_details = $q->result_array();

		$this->load->view('accepted_complain', array(
				'complain_details'=>$complain_details,
			)
		);
	}//function
	public function rejected_complain(){
		$q = $this->db
				->select('*')
				->from('complain')
				->join('franchise','complain.c_franchise_id = franchise.franchise_id')
				->where( array( 'c_status' => '5', 'c_franchise_id'=>$this->session->userdata['franchise_id'] ) )
				->get();
		$complain_details = $q->result_array();

		$this->load->view('rejected_complain', array(
				'complain_details'=>$complain_details,
			)
		);
	}//function

	/*
	|--------------------------------------------------------------------------
	| Bikers function
	|--------------------------------------------------------------------------
	*/

	public function biker(){
		if( ! $this->session->userdata['franchise_id'] ){
			return redirect('franchise');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_biker = $this->data['no_of_biker_by_franchise'];
		$config['base_url'] = site_url('/franchise/bikers');
		$config['total_rows'] = $no_of_biker;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db
					->order_by('biker_id','ASC')
					->limit($config['per_page'],$this->uri->segment(3))
					->where( array('franchise_id' => $this->session->userdata['franchise_id']) )
					->get('biker');
		$biker_details = $query->result_array();
		$pagination = $this->pagination->create_links();

		$this->load->view('biker', array(
				'biker_details'=>$biker_details,
				'pagination'=>$pagination
			)
		);
	}//function


	public function add_biker(){
		$biker_name = $this->input->post('biker_name');
		$biker_number = $this->input->post('biker_number');
		$biker_phone = $this->input->post('biker_phone');
		$password = base64_encode($this->input->post('password'));
		$q = $this->db->where( array('username'=>$biker_phone) )->get('biker');
		if( $q->num_rows() >0 ){
			$this->session->set_flashdata( 'biker_exist' , 'Biker username Exists !' );
			return redirect('franchise/biker');
		}else{
			$data = array(
			'franchise_id'  => $this->session->userdata['franchise_id'],
			'biker_name'  => $biker_name,
			'biker_number' 	=> $biker_number,
			'username'	=> $biker_phone,
			'password' => $password,
			'created_by'	=> $this->session->userdata['franchise_id'],
		);

		// print_r($data);
		$return = $this->db->insert('biker', $data);
		if( $return ){
			return redirect('franchise/biker');
		}else{
			$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
			return redirect('franchise/biker');
		}
		}
		

	}//function

	public function edit_biker( $biker_id ){
		$query = $this->db
					->where( array('biker_id'=>$biker_id, 'franchise_id' => $this->session->userdata['franchise_id'] ))
					->get('biker');
		$biker_details = $query->result_array();

		$this->load->view('edit_biker', array(
				'biker_details'=>$biker_details,
				
			)
		);
	}//function

	public function edit_biker_to_db(){
		$biker_name = $this->input->post('biker_name');
		$biker_id = $this->input->post('biker_id');
		$biker_number = $this->input->post('biker_number');
		$biker_phone = $this->input->post('biker_phone');

		$data = array(
			'biker_name'  => $biker_name,
			'biker_number' 	=> $biker_number,
			'username'	=> $biker_phone,
			'modified_by'	=> $this->session->userdata['franchise_id'],
		);

		$return = $this->db->update('biker', $data, array( 'biker_id' => $biker_id, 'franchise_id' => $this->session->userdata['franchise_id'] ));
        if( $return ){
        	return redirect("franchise/biker");
        }else{
        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
        	return redirect("franchise/edit_biker/$biker_id");
        }
	}//function

	public function delete_biker(){
		$biker_id = $this->input->post('biker_id');
		$delete = $this->db->delete('biker', array('biker_id' => $biker_id, 'franchise_id' => $this->session->userdata['franchise_id']));
		if($delete){
			echo $biker_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}//function


	public function wasif(){
		$quety = 'SELECT * FROM `pf_order` WHERE `user_id` = 1 AND `franchise_id`=1 AND `cloth_status`="6" GROUP BY `order_code`';
		$result = $this->db->query($quety);
		$sarasi = $result->result_array();
		echo '<pre>';
		print_r($sarasi);
		echo '</pre>';
	}








	/*
	|--------------------------------------------------------------------------
	| The End
	|--------------------------------------------------------------------------
	*/

}//class

