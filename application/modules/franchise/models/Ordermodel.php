<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Ordermodel extends CI_model {

    

    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    public function get_order_by_id( $order_id ){
        $query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->where( array( 'order_id' => $order_id ) )
        ->get();

        $order_detail = $query->result_array();
        return $order_detail;

    }

     public function get_order_by_code( $order_code ){
        $query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('franchise','pf_order.franchise_id = franchise.franchise_id')
        ->join('delivery_address','pf_order.user_id = delivery_address.user_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
        return $order_detail;

    }
    
}//class
