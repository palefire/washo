<?php
	include ('header.php');
  // print_r($bikers);
//Array ( [0] => Array ( [order_id] => 1 [order_code] => washo-20200228-1 [user_id] => 1 [name] => Shirt [service_id] => 1 [cloth_type] => 1 [franchise_id] => 1 [price_per_unit] => 20 [quantity] => 2 [total_price] => 40 [cloth_status] => 0 [payment_mode] => 1 [transaction_id] => [payment_status] => 1 [status] => 1 [order_date] => 2020-03-09 14:33:22 [delivery_date] => [pick_up_biker_id] => [warehouse_id] => [delivery_biker_id] => [created_by] => 1 [created_at] => 2020-03-03 12:33:24 [modified_by] => 1 [service_name] => Wash [service_slug] => wash-0 [svg] => wash.svg ) )
  $quantity = $order_detail[0]['quantity'];
  $price_per_unit = $order_detail[0]['price_per_unit'];
  $total_price = intval( $quantity ) * $price_per_unit;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard / Orders</h1>
            
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Orders Code</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $order_detail[0]['order_code'] ?></div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Price</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_price ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Shipment Status</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $this->data['order_cloth_status'][$order_detail[0]['cloth_status']] ?></div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Service</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $order_detail[0]['service_name'] ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-8">
              <div class="card shadow mb-4">
              
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <h6 class="m-0 font-weight-bold text-primary">Order Detail</h6>
                       <div class="row">
                        <div class="col-lg-6 mb-4">
                          <div class="card bg-info text-white shadow">
                            <div class="card-body">
                              Name
                              <div class="text-white-50 small"><?php echo $order_detail[0]['name']; ?></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6 mb-4">
                          <div class="card bg-secondary text-white shadow">
                            <div class="card-body">
                              Cloth Type
                              <div class="text-white-50 small"><?php echo $this->data['order_cloth_type'][$order_detail[0]['cloth_type']]; ?></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6 mb-4">
                          <div class="card bg-secondary text-white shadow">
                            <div class="card-body">
                              Payment Mode
                              <div class="text-white-50 small">
                                <?php if( $order_detail[0]['payment_mode'] == 1 ){ ?>
                                  Cash On Delivery
                                <?php } ?>
                                <?php if( $order_detail[0]['payment_mode'] == 2 ){ ?>
                                  Online Payment
                                <?php } ?>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6 mb-4">
                          <div class="card bg-info text-white shadow">
                            <div class="card-body">
                              Order Date
                              <div class="text-white-50 small"><?php echo $order_detail[0]['order_date'] ?></div>
                            </div>
                          </div>
                        </div>
                        
                        
                      </div>

                  </div>
                </div>


              </div>
            </div>
            <?php 
            if( $order_detail[0]['cloth_status'] == 0 ){
            ?>
            <div class="col-xl-4 col-md-4 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Action</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                          <form>
                            <fieldset>
                              <div class="form-group">
                                <label for="exampleSelect1">Assign Biker For Pickup</label>
                                <select class="form-control" id="exampleSelect1" name="department" required>
                                  <?php 
                                    foreach( $bikers as $biker ){ 
                                      //Array ( [0] => Array ( [biker_id] => 1 [biker_name] => Sarasij Roy [biker_number] => WB02W6886 [biker_status] => 0 [created_by] => 1 [created_at] => 2020-03-08 00:18:12 [modified_by] => )
                                  ?>
                                  <option value="<?php echo $biker['biker_id'] ?>"><?php echo $biker['biker_name'].'-'.$biker['biker_number'] ?></option>
                                  <?php } ?>
                                 
                                </select>
                              </div>
                              <button type="submit" class="btn btn-primary">Assign</button>
                            </fieldset>
                          </form>

                      </div>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div>

            <?php } ?>

          
          </div>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>