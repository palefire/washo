<?php
  include ('header.php');
  // echo '<pre>';
  // print_r($order_details);
  // echo '</pre>';
  //Array ( [0] => Array ( [0] => Array ( [order_id] => 1 [order_code] => washo-20200228-1 [user_id] => 1 [name] => Shirt [service_id] => 1 [cloth_type] => 1 [franchise_id] => 1 [price_per_unit] => 20 [quantity] => 2 [total_price] => 40 [cloth_status] => 0 [payment_mode] => 1 [transaction_id] => [payment_status] => 1 [status] => 1 [order_date] => 2020-03-09 14:33:22 [delivery_date] => [pick_up_biker_id] => [warehouse_id] => [delivery_biker_id] => [created_by] => 1 [created_at] => 2020-03-03 12:33:24 [modified_by] => 1 [service_name] => Wash [service_slug] => wash-0 [svg] => wash.svg ) [1] => Array ( [order_id] => 2 [order_code] => washo-20200228-1 [user_id] => 1 [name] => Pant [service_id] => 1 [cloth_type] => 1 [franchise_id] => 1 [price_per_unit] => 20 [quantity] => 2 [total_price] => 40 [cloth_status] => 0 [payment_mode] => 1 [transaction_id] => [payment_status] => 1 [status] => 1 [order_date] => 2020-03-13 11:21:41 [delivery_date] => [pick_up_biker_id] => [warehouse_id] => [delivery_biker_id] => [created_by] => 1 [created_at] => 2020-03-03 12:33:24 [modified_by] => 1 [service_name] => Wash [service_slug] => wash-0 [svg] => wash.svg ) ) [1] => Array ( [0] => Array ( [order_id] => 3 [order_code] => washo-20200313-1 [user_id] => 2 [name] => Skirt [service_id] => 1 [cloth_type] => 2 [franchise_id] => 1 [price_per_unit] => 15 [quantity] => 1 [total_price] => 15 [cloth_status] => 0 [payment_mode] => 1 [transaction_id] => [payment_status] => 1 [status] => 1 [order_date] => 2020-03-13 11:36:34 [delivery_date] => [pick_up_biker_id] => [warehouse_id] => [delivery_biker_id] => [created_by] => 1 [created_at] => 2020-03-03 12:33:24 [modified_by] => 1 [service_name] => Wash [service_slug] => wash-0 [svg] => wash.svg ) ) )
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard /Washed</h1>
            
          </div>

        

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
              
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
                      <?php
                        $wash_complete = $this->session->flashdata('wash_complete');
                        $biker_failed = $this->session->flashdata('biker_failed');
                        
                        if( $wash_complete ){
                      ?>
                          <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Great!</h4>
                            <p class="mb-0"><?php echo $wash_complete; ?></p>
                          </div>
                      <?php
                        } 
                        if( $biker_failed ){
                      ?>
                      <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Sorry!</h4>
                            <p class="mb-0"><?php echo $biker_failed; ?></p>
                          </div>
                      <?php  } ?>
                        <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Order Code </th>
                              <th scope="col">Order Date</th>
                              <th scope="col">Action</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                           <?php foreach( $order_details as $order_detail ){ ?>
                            <tr>
                              <td><?php echo $order_detail[0]['order_code'] ?></td>
                              <td><?php echo $order_detail[0]['order_date'] ?></td>
                              <td><a href="<?php echo base_url('franchise/view_washed/').$order_detail[0]['order_code']; ?>" class="btn btn-warning"> View</a></s></td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                      </div>

                  </div>
                </div>


              </div>
            </div>
            

          
          </div>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
  include ('footer.php');
?>