<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($franchise_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard/ Biker</h1>
            
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Bikers List</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Biker</th>
                            <th scope="col">Bike Number</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Bike Status</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $slno = 1;
                          foreach( $biker_details as $biker_detail ){ 
                            if( $biker_detail['biker_status'] == 0 ){
                                $biker_status = 'Idle';
                            }elseif( $biker_detail['biker_status'] == 1 ){
                                $biker_status = 'On Duty';
                            }else{
                              $biker_status = 'Unavailable';
                            }
                          ?>
                          <tr>
                            <th scope="row"><?php echo $slno; ?></th>
                            <td><?php echo $biker_detail['biker_name'] ?></td>
                            <td><?php echo $biker_detail['biker_number'] ?></td>
                            <td><?php echo $biker_detail['username'] ?></td>
                            <td><?php echo $biker_status; ?></td>
                            <td><a href="<?php echo base_url('/franchise/edit_biker/').$biker_detail['biker_id']; ?>" style="color: blue">Edit</a></td>
                            <td><span style="color: red; cursor: pointer" class="delete-biker" data-id="<?php echo $biker_detail['biker_id']; ?>">Delete</span></td>
                          </tr>
                          <?php 
                          $slno++;
                          } 
                          ?>
                        </tbody>
                      </table>
                    </div>
                    <?php echo $pagination; ?>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Biker</h6>
                
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <?php 
                    $insert_service_error = $this->session->flashdata('insert_service_error');
                    $biker_exist = $this->session->flashdata('biker_exist');
                    if( $insert_service_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_service_error; ?></p>
                    </div>
                  <?php }
                  if( $biker_exist ){ 
                  ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $biker_exist; ?></p>
                    </div>
                  <?php  } ?>
                  <?php echo form_open_multipart('franchise/add_biker', array('id'=>'add-product-form') ); ?>
                  <table>
                    <tr>
                      <td><label>Biker Name</label></td>
                      <td><input type="text" name="biker_name" required></td>
                    </tr>
                    <tr>
                      <td><label>Bike Number</label></td>
                      <td><input type="text" name="biker_number" required></td>
                    </tr>
                    <tr>
                      <td><label>Phone Number</label></td>
                      <td><input type="text" name="biker_phone" placeholder="username"required></td>
                    </tr>
                    <tr>
                      <td><label>Password</label></td>
                      <td><input type="password" name="password" required></td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <input type="submit" name="add_biker" value="Add Biker">
                      </td>
                    </tr>
                  </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

<script >
  $(document).ready( function(){
    $('.delete-biker').on('click', function(){
      // var service_id = $(this).data('id');
      var biker_id = $(this).attr('data-id');

     
     if(confirm( 'Do You want to delete the Biker?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('franchise/delete_biker') ?>",
          type : 'POST',
          data : {
            'biker_id' : biker_id
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/genre_list') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>