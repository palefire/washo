
<style>
  .li-area{margin-right:10px;margin-bottom:20px}
  .li-area .single-tab{padding: 10px 20px; background-color: #286090; text-decoration: none; color: #CFCFCF; text-transform: uppercase; font-weight: bold; font-size:12px}
  .li-area .active{background-color: #DB0706; text-transform: uppercase; font-weight: bold; color: #ffffff;}
</style>

<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($due_orders);
  // echo '</pre>';

  $no_today_orders = count($today_orders);
  $no_tomorrow_orders = count($tomorrow_orders);
  $no_dayafter_orders = count($dayafter_orders);
  $no_due_orders = count($due_orders);
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard / Franchise</h1>
            
          </div>

         

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-6 col-lg-6">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Order Notifications</h6>
                 
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <div class="row">


                     
                     <?php if( $pending_order > 0 ){  ?>
                              <div class="col-lg-6 mb-4">
                                <div class="card bg-danger text-white shadow">
                                  <div class="card-body">
                                   <a href="<?php echo base_url('franchise/pending_order'); ?>" style="color:#ffffff"> Pending Order ! (<?php echo $pending_order;  ?>)</a>
                                    <div class="text-white-50 small"><a href="<?php echo base_url('franchise/pending_order'); ?>" style="color:#ffffff">View</a></div>
                                  </div>
                                </div>
                              </div>
                      <?php   }?>

                      <?php if( $assign_warehouse > 0 ){  ?>
                              <div class="col-lg-6 mb-4">
                                <div class="card bg-info text-white shadow">
                                  <div class="card-body">
                                   <a href="<?php echo base_url('franchise/picked_up'); ?>" style="color:#ffffff"> Assign To Warehoues ! (<?php echo $assign_warehouse;  ?>)</a>
                                    <div class="text-white-50 small"><a href="<?php echo base_url('franchise/picked_up'); ?>" style="color:#ffffff">View</a></div>
                                  </div>
                                </div>
                              </div>
                      <?php   }?>

                      <?php if( $collect_from_warehouse > 0 ){  ?>
                              <div class="col-lg-6 mb-4">
                                <div class="card bg-info text-white shadow">
                                  <div class="card-body">
                                   <a href="<?php echo base_url('franchise/in_warehouse'); ?>" style="color:#ffffff"> Collect From Warehoues ! (<?php echo $collect_from_warehouse;  ?>)</a>
                                    <div class="text-white-50 small"><a href="<?php echo base_url('franchise/in_warehouse'); ?>" style="color:#ffffff">View</a></div>
                                  </div>
                                </div>
                              </div>
                      <?php   }?>

                       <?php if( $assign_delivery > 0 ){  ?>
                              <div class="col-lg-6 mb-4">
                                <div class="card bg-danger text-white shadow">
                                  <div class="card-body">
                                   <a href="<?php echo base_url('franchise/washed'); ?>" style="color:#ffffff"> Assign Delivery ! (<?php echo $assign_delivery;  ?>)</a>
                                    <div class="text-white-50 small"><a href="<?php echo base_url('franchise/washed'); ?>" style="color:#ffffff">View</a></div>
                                  </div>
                                </div>
                              </div>
                      <?php   }?>
          





                      </div>
                </div>
              </div>
            </div>

           
          </div>

           <div class="col-xl-6 col-lg-6">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Delivery Notifications</h6>
                 
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     


                    <ul class="nav nav-tabs">
                        <li class="li-area"><a data-toggle="tab" href="#menu1" class="active single-tab">Today( <?php echo $no_today_orders  ?> )</a></li>
                        <li class="li-area"><a data-toggle="tab" href="#menu2" class="single-tab">Tomorrow(<?php echo $no_tomorrow_orders  ?>)</a></li>
                        <li class="li-area"><a data-toggle="tab" href="#menu3" class="single-tab">Day After(<?php echo $no_dayafter_orders  ?>)</a></li>
                        <li class="li-area"><a data-toggle="tab" href="#menu4" class="single-tab">Due (<?php echo $no_due_orders  ?>)</a></li>

                    </ul>
                    <div class="dh-single-all-tab">
                      <div class="tab-content">

                          <div id="menu1" class="tab-pane fade in show active">
                            <div class="table-responsive">
                              <table class="table ">
                                <thead class="thead-dark">
                                  <tr>
                                    <th scope="col" >Code</th>
                                    <th scope="col" >Status</th>
                                   
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach( $today_orders as $p_order ){  ?>
                                    <tr>
                                      <td><a href="<?php echo site_url('franchise/view_order/').$p_order['order_id'] ?>"><?php echo $p_order['order_code']  ?></a></td>
                                      <td><?php echo $this->data['order_cloth_status'][$p_order['cloth_status']]  ?></td>
                                    </tr>
                                  <?php  } ?>
                                </tbody>
                              </table>
                          </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="table-responsive">
                              <table class="table ">
                                <thead class="thead-dark">
                                  <tr>
                                    <th scope="col" >Code</th>
                                    <th scope="col" >Status</th>
                                   
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach( $tomorrow_orders as $p_order ){  ?>
                                    <tr>
                                      <td><a href="<?php echo site_url('franchise/view_order/').$p_order['order_id'] ?>"><?php echo $p_order['order_code']  ?></a></td>
                                      <td><?php echo $this->data['order_cloth_status'][$p_order['cloth_status']]  ?></td>
                                    </tr>
                                  <?php  } ?>
                                </tbody>
                              </table>
                          </div>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <div class="table-responsive">
                              <table class="table ">
                                <thead class="thead-dark">
                                  <tr>
                                    <th scope="col" >Code</th>
                                    <th scope="col" >Status</th>
                                  </tr>
                                </thead>
                                <tbody>
                                   <?php foreach( $dayafter_orders as $p_order ){  ?>
                                    <tr>
                                      <td><a href="<?php echo site_url('franchise/view_order/').$p_order['order_id'] ?>"><?php echo $p_order['order_code']  ?></a></td>
                                      <td><?php echo $this->data['order_cloth_status'][$p_order['cloth_status']]  ?></td>
                                    </tr>
                                  <?php  } ?>
                                </tbody>
                              </table>
                          </div>
                        </div>
                        <div id="menu4" class="tab-pane fade">
                            <div class="table-responsive">
                              <table class="table ">
                                <thead class="thead-dark">
                                  <tr>
                                    <th scope="col" >Code</th>
                                    <th scope="col" >Status</th>
                                  </tr>
                                </thead>
                                <tbody>
                                   <?php foreach( $due_orders as $p_order ){  ?>
                                    <tr>
                                      <td><a href="<?php echo site_url('franchise/view_order/').$p_order['order_id'] ?>"><?php echo $p_order['order_code']  ?></a></td>
                                      <td><?php echo $this->data['order_cloth_status'][$p_order['cloth_status']]  ?></td>
                                    </tr>
                                  <?php  } ?>
                                </tbody>
                              </table>
                          </div>
                        </div>
                        
                      </div>
                    </div>


                   </div>
                </div>
              </div>
            </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>