<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
        if( isset( $this->session->userdata['warehouse_id'] ) ){
        	$q5 = $this->db->where( array('warehouse_id'=>$this->session->userdata['warehouse_id'], 'cloth_status' => '3') )->get('pf_order');
	      	$this->data['no_of_warehouse_orders'] = $q5->num_rows();
        }
        

	}

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		if( isset($this->session->userdata['warehouse_id']) ){
			return redirect('warehouse/dashboard');
			
		}
		$this->load->view('login');
	}

	public function verify_login(){
		$username = $this->input->post('username');
		$password_decode = $this->input->post('password');
		$password = base64_encode( $password_decode );

		$q = $this->db->where( array('username'=>$username, 'password'=>$password) )->get('warehouse');
		if( $q->num_rows() ){
			$warehouse_details =  $q->row();
			// echo $admin_details->admin_id;
			//print_r($admin_details);
			$this->session->set_userdata( 'warehouse_id', $warehouse_details->warehouse_id );
			$this->session->set_userdata( 'warehouse_name', $warehouse_details->warehouse_name );
			
			return redirect('warehouse/dashboard');
		}else{
			$this->session->set_flashdata( 'login_failed' , 'User Name or Password does not match!' );
			return redirect('warehouse');
		}
	}//function

	public function logout(){
		$this->session->unset_userdata('warehouse_id');
		$this->session->unset_userdata('warehouse_name');
	
		
		return redirect('warehouse' );
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function dashboard(){
		if( ! $this->session->userdata['warehouse_id'] ){
			return redirect('warehouse');
			die();
		}
		$this->load->view('warehouse');
	}//function


	/*
	|--------------------------------------------------------------------------
	| Pickup function
	|--------------------------------------------------------------------------
	*/

	public function warehouse_orders(){
		if( ! $this->session->userdata['warehouse_id'] ){
			return redirect('warehouse');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_warehouse_orders = $this->data['no_of_warehouse_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->join('franchise','pf_order.franchise_id = franchise.franchise_id')
		->where( array( 'warehouse_id' => $this->session->userdata['warehouse_id'], 'cloth_status' => '3' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('warehouse_orders', array(
				'order_details'=>$array,
				// 'no_of_warehouse_orders'	=> $no_of_warehouse_orders
			)
		);
	}//function

	public function view_warehouse_orders( $order_code ){
		
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();

		

		$this->load->view('view_warehouse_orders', array(
				'order_detail'	=>$order_detail,
			)
		);
		

	}//function




}//class