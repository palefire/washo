<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

      	$q1 = $this->db->get('pf_order');
	    $no_of_orders = $q1->num_rows();
	    $this->data['no_of_orders'] = $no_of_orders;

    	// $q3 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '2') )->get('pf_order');
     //    $this->data['no_of_picked_up_orders'] = $q3->num_rows();

    	// $q4 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id'], 'cloth_status' => '1') )->get('pf_order');
     //    $this->data['no_of_pickup_assigned_orders'] = $q4->num_rows();

        $q5 = $this->db->where( array( 'cloth_status' => '0') )->get('pf_order'); 
        $this->data['no_of_pending_orders'] = $q5->num_rows();


        $this->load->model('franchise/ordermodel', 'Order');
	}//constructor

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		if( isset($this->session->userdata['admin_id']) ){
			return redirect('admin/dashboard');
			
		}
		$this->load->view('login');
	}//function

	public function verify_login(){
		$username = $this->input->post('username');
		$password_decode = $this->input->post('password');
		$password = base64_encode( $password_decode );

		$q = $this->db->where( array('username'=>$username, 'password'=>$password) )->get('admin');
		if( $q->num_rows() ){
			$admin_details =  $q->row();
			// echo $admin_details->admin_id;
			//print_r($admin_details);
			$this->session->set_userdata( 'admin_id', $admin_details->admin_id );
			$this->session->set_userdata( 'admin_level', $admin_details->level );
			$this->session->set_userdata( 'admin_name', $admin_details->name );
			return redirect('admin/dashboard');
		}else{
			$this->session->set_flashdata( 'login_failed' , 'User Name or Password does not match!' );
			return redirect('admin');
		}
	}//function

	public function logout(){
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('admin_level');
		$this->session->unset_userdata('admin_name');
		
		return redirect('admin' );
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function dashboard(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$this->load->view('admin');
	}//function
	/*
	|--------------------------------------------------------------------------
	| services function
	|--------------------------------------------------------------------------
	*/

	public function services(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_service = $this->data['no_of_service'];
		$config['base_url'] = site_url('/admin/services');
		$config['total_rows'] = $no_of_service;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db->order_by('service_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('service');
		$service_details = $query->result_array();
		$pagination = $this->pagination->create_links();

		$this->load->view('services', array(
				'service_details'=>$service_details,
				'pagination'=>$pagination
			)
		);
	}//function

	public function add_services(){

		$service_name = $this->input->post('service_name');
		
		$service_slug = strtolower($service_name); 
		$service_slug = str_replace(' ', '-', $service_slug); // Replaces all spaces with hyphens.
		$service_slug = preg_replace('/[^A-Za-z0-9\-]/', '', $service_slug); // Removes special chars.
		$service_slug = preg_replace('/-+/', '-', $service_slug); // Replaces multiple hyphens with single one.

		$service_slug_check = $this->data['no_of_service'];
		
		$service_slug = $service_slug.'-'.$service_slug_check;
		$svg = $this->input->post('svg');

		$config['upload_path']          = './uploads/services/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['max_width']            = 4500;
        $config['max_height']           = 3000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('service_img')){
        	$upload_error = $this->upload->display_errors();
            $this->session->set_flashdata( 'upload_error' , $upload_error );
			return redirect('admin/services');
        }else{
        	$data1 =  $this->upload->data();
            $img = $data1["file_name"];
        	$data = array(
				'service_name'  => $service_name,
				'service_slug' 	=> $service_slug,
				'svg'			=> $svg,
				'img'			=> $img,
				'created_by'	=> $this->session->userdata['admin_id'],
			);


			$return = $this->db->insert('service', $data);
			if( $return ){
				return redirect('admin/services');
			}else{
				$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
				return redirect('admin/services');
			}
        }

	}//function

	public function edit_service( $service_id ){
		$query = $this->db->where( array('service_id'=>$service_id) )->get('service');
		$service_details = $query->result_array();

		$this->load->view('edit_service', array(
				'service_details'=>$service_details,
				
			)
		);
	}//function

	public function edit_services_to_db(){
		$service_name = $this->input->post('service_name');
		$service_id = $this->input->post('service_id');
		$svg = $this->input->post('svg');
		$img = $_FILES['service_img']['name'];
		if( empty( $img ) ){
			$data = array(
				'service_name' => $service_name,
				'svg'			=> $svg,
				'modified_by'  => $this->session->userdata['admin_id'],
			);

			$return = $this->db->update('service', $data, array('service_id' => $service_id));
	        if( $return ){
	        	return redirect("admin/services");
	        }else{
	        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
	        	return redirect("admin/edit_service/$service_id");
	        }
		}else{
			$config['upload_path']          = './uploads/services/';
	        $config['allowed_types']        = 'gif|jpg|png|jpeg';
	        $config['max_size']             = 2000;
	        $config['max_width']            = 4500;
	        $config['max_height']           = 3000;
	        $this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('service_img')){
	        	$upload_error = $this->upload->display_errors();
	            $this->session->set_flashdata( 'upload_error' , $upload_error );
				return redirect('admin/services');
	        }else{
	        	$data1 =  $this->upload->data();
            	$img = $data1["file_name"];
	        	$data = array(
					'service_name' => $service_name,
					'svg'			=> $svg,
					'img'			=> $img,
					'modified_by'  => $this->session->userdata['admin_id'],
				);

				$return = $this->db->update('service', $data, array('service_id' => $service_id));
		        if( $return ){
		        	return redirect("admin/services");
		        }else{
		        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
		        	return redirect("admin/edit_service/$service_id");
		        }
	        }
		}

		
	}

	public function delete_service(){
		$service_id = $this->input->post('service_id');
		$delete = $this->db->delete('service', array('service_id' => $service_id));
		if($delete){
			echo $service_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}

	/*
	|--------------------------------------------------------------------------
	| clothes function
	|--------------------------------------------------------------------------
	*/
	public function men_clothes(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}

		$no_of_men_clothes = $this->data['no_of_men_clothes'];
		$config['base_url'] = site_url('/admin/men_clothes');
		$config['total_rows'] = $no_of_men_clothes;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db
		->select('*')
		->from('clothes')
		->join('service','clothes.service_type = service.service_id')
		->where( array( 'cloth_type' => '1' ) )
		->order_by('cloth_id','ASC')
		->limit($config['per_page'],$this->uri->segment(3))
		->get();
		$men_cloth_details = $query->result_array();
		$pagination = $this->pagination->create_links();


		$this->load->view('men_clothes', array(
				'cloth_details'=>$men_cloth_details,
				'pagination'=>$pagination
			)

		);
	}//function

	public function women_clothes(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}

		$no_of_women_clothes = $this->data['no_of_women_clothes'];
		$config['base_url'] = site_url('/admin/women_clothes');
		$config['total_rows'] = $no_of_women_clothes;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db
		->select('*')
		->from('clothes')
		->join('service','clothes.service_type = service.service_id')
		->where( array( 'cloth_type' => '2' ) )
		->order_by('cloth_id','ASC')
		->limit($config['per_page'],$this->uri->segment(3))
		->get();
		$women_cloth_details = $query->result_array();
		$pagination = $this->pagination->create_links();


		$this->load->view('women_clothes', array(
				'cloth_details'=>$women_cloth_details,
				'pagination'=>$pagination
			)

		);
	}//function
	public function household_clothes(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}

		$no_of_household_clothes = $this->data['no_of_household_clothes'];
		$config['base_url'] = site_url('/admin/household_clothes');
		$config['total_rows'] = $no_of_household_clothes;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db
		->select('*')
		->from('clothes')
		->join('service','clothes.service_type = service.service_id')
		->where( array( 'cloth_type' => '3' ) )
		->order_by('cloth_id','ASC')
		->limit($config['per_page'],$this->uri->segment(3))
		->get();
		$household_cloth_details = $query->result_array();
		$pagination = $this->pagination->create_links();


		$this->load->view('households_clothes', array(
				'cloth_details'=>$household_cloth_details,
				'pagination'=>$pagination
			)

		);
	}//function

	public function kids_clothes(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}

		$no_of_kids_clothes = $this->data['no_of_kids_clothes'];
		$config['base_url'] = site_url('/admin/kids_clothes');
		$config['total_rows'] = $no_of_kids_clothes;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db
		->select('*')
		->from('clothes')
		->join('service','clothes.service_type = service.service_id')
		->where( array( 'cloth_type' => '4' ) )
		->order_by('cloth_id','ASC')
		->limit($config['per_page'],$this->uri->segment(3))
		->get();
		$kids_cloth_details = $query->result_array();
		$pagination = $this->pagination->create_links();


		$this->load->view('kids_clothes', array(
				'cloth_details'=>$kids_cloth_details,
				'pagination'=>$pagination
			)

		);
	}//function

	public function add_clothes(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db->get('service');
		$service_details = $query->result_array();
		$this->load->view('add_clothes',array(
			"service_details" => $service_details


		));
	}//function


	public function edit_clothes( $cloth_id ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db->get('service');
		$service_details = $query->result_array();

		$query1 = $this->db->where( array('cloth_id'=> $cloth_id) )->get('clothes');
		$cloth_details = $query1->result_array();

		$this->load->view('edit_clothes',array(
			"service_details" 	=> $service_details,
			"cloth_detail" 		=>$cloth_details

		));
	}//function

	public function update_clothes_to_db(){
		$cloth_id = $this->input->post('cloth_id');
		$cloth_name = $this->input->post('cloth_name');
		$cloth_type = $this->input->post('cloth_type');
		$service_type = $this->input->post('service_type');
		$price = $this->input->post('price');
		$time = $this->input->post('time');
		$svg = $this->input->post('svg');

		

		$data = array(
			'cloth_name'	=> $cloth_name,
			'service_type'	=> $service_type,
			'price'			=> $price,
			'time'			=> $time,
			'icon'			=> $svg,
			'modified_by'	=> $this->session->userdata['admin_id'],
		);

		// print_r($data);

		$return = $this->db->update('clothes', $data, array('cloth_id' => $cloth_id));
		if( $return ){
			if( $cloth_type == 1 ){
				return redirect('admin/men_clothes');	
			}elseif( $cloth_type == 2 ){
				return redirect('admin/women_clothes');
			}elseif( $cloth_type == 3 ){
				return redirect('admin/household_clothes');
			}else{
				return redirect('admin/kids_clothes');
			}
			
		}else{
			$this->session->set_flashdata( 'insert_clothes_error' , 'Database error ! Cannot insert !' );
			return redirect("admin/edit_clothes/$cloth_id");
		}

	}//function

	public function add_clothes_to_db(){
		$cloth_name = $this->input->post('cloth_name');
		$cloth_type = $this->input->post('cloth_type');
		$service_type = $this->input->post('service_type');
		$price = $this->input->post('price');
		$time = $this->input->post('time');
		$svg = $this->input->post('svg');

		// $lower_cloth_name = strtolower($cloth_name);
		// $cloth_slug_0 = str_replace(' ', '-', $lower_cloth_name);
		// $cloth_slug_1 = str_replace("'", "", $cloth_slug_0);
		// $cloth_slug_2 = str_replace("(", "-", $cloth_slug_1);
		// $cloth_slug_3 = str_replace(")", "-", $cloth_slug_2);
		// $cloth_slug_4 = str_replace(":", "", $cloth_slug_3);
		// $cloth_slug_5 = str_replace("&", "and", $cloth_slug_4);
		// $cloth_slug = str_replace('"', '', $cloth_slug_4);


		$cloth_slug = strtolower($cloth_name); 
		$cloth_slug = str_replace(' ', '-', $cloth_slug); // Replaces all spaces with hyphens.
		$cloth_slug = preg_replace('/[^A-Za-z0-9\-]/', '', $cloth_slug); // Removes special chars.
		$cloth_slug = preg_replace('/-+/', '-', $cloth_slug); // Replaces multiple hyphens with single one.

		$cloth_slug_check = $this->data['no_of_clothes'];
		
		$cloth_slug = $cloth_slug.'-'.$cloth_slug_check;

		$data = array(
			'cloth_name'	=> $cloth_name,
			'cloth_slug'	=> $cloth_slug,
			'cloth_type'	=> $cloth_type,
			'service_type'	=> $service_type,
			'price'			=> $price,
			'time'			=> $time,
			'icon'			=> $svg,
			'created_by'	=> $this->session->userdata['admin_id'],
		);

		// print_r($data);

		$return = $this->db->insert('clothes', $data);
		if( $return ){
			if( $cloth_type == 1 ){
				return redirect('admin/men_clothes');	
			}elseif( $cloth_type == 2 ){
				return redirect('admin/women_clothes');
			}elseif( $cloth_type == 3 ){
				return redirect('admin/household_clothes');
			}else{
				return redirect('admin/kids_clothes');
			}
		}else{
			$this->session->set_flashdata( 'insert_clothes_error' , 'Database error ! Cannot insert !' );
			return redirect('admin/add_clothes');
		}
	}//function

	public function delete_cloth(){
		$cloth_id = $this->input->post('cloth_id');
		$delete = $this->db->delete('clothes', array('cloth_id' => $cloth_id));
		if($delete){
			echo $cloth_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}
	/*
	|--------------------------------------------------------------------------
	| Franchise function
	|--------------------------------------------------------------------------
	*/

	public function franchise(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_franchise = $this->data['no_of_franchise'];
		$config['base_url'] = site_url('/admin/franchise');
		$config['total_rows'] = $no_of_franchise;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db->order_by('franchise_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('franchise');
		$franchise_details = $query->result_array();
		$pagination = $this->pagination->create_links();

		$this->load->view('franchise', array(
				'franchise_details'=>$franchise_details,
				'pagination'=>$pagination
			)
		);
	}//function

	public function add_franchise(){
		$franchise_name = $this->input->post('franchise_name');
		$franchise_pin = $this->input->post('franchise_pin');
		$franchise_username = $this->input->post('franchise_username');
		$franchise_password = $this->input->post('franchise_password');
		
		$q = $this->db->where( array('username'=>$franchise_username) )->get('franchise');
		$q1 = $this->db->where( array('franchise_pin'=>$franchise_pin) )->get('franchise');
		if( $q->num_rows() >0 ){
			$this->session->set_flashdata( 'franchise_exist' , 'Franchise Username already exists' );
			return redirect('admin/franchise');
		}elseif($q1->num_rows() >0){
			$this->session->set_flashdata( 'franchise_pin_exist' , 'Franchise Pin already exists' );
			return redirect('admin/franchise');
		}else{
			$data = array(
				'franchise_name'  	=> $franchise_name,
				'franchise_pin' 	=> $franchise_pin,
				'username'			=> $franchise_username,
				'password'			=> base64_encode( $franchise_password ),
				'created_by'		=> $this->session->userdata['admin_id'],
			);


			$return = $this->db->insert('franchise', $data);
			if( $return ){
				return redirect('admin/franchise');
			}else{
				$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
				return redirect('admin/franchise');
			}

		}//if for username check

		
	}//function

	public function edit_franchise( $franchise_id ){
		$query = $this->db->where( array('franchise_id'=>$franchise_id) )->get('franchise');
		$franchise_details = $query->result_array();

		$this->load->view('edit_franchise', array(
				'franchise_details'=>$franchise_details,
				
			)
		);
	}//function

	public function edit_franchise_to_db(){
		$franchise_name = $this->input->post('franchise_name');
		$franchise_id = $this->input->post('franchise_id');
		$franchise_pin = $this->input->post('franchise_pin');

		$data = array(
			'franchise_name'  => $franchise_name,
			'franchise_pin' 	=> $franchise_pin,
			'modified_by'	=> $this->session->userdata['admin_id'],
		);

		$return = $this->db->update('franchise', $data, array('franchise_id' => $franchise_id));
        if( $return ){
        	return redirect("admin/franchise");
        }else{
        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
        	return redirect("admin/edit_franchise/$franchise_id");
        }
	}//function

	public function delete_franchise(){
		$franchise_id = $this->input->post('franchise_id');
		$delete = $this->db->delete('franchise', array('franchise_id' => $franchise_id));
		if($delete){
			echo $franchise_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}//function



	/*
	|--------------------------------------------------------------------------
	| Warehouse function
	|--------------------------------------------------------------------------
	*/

	public function warehouse(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_warehouse = $this->data['no_of_warehouse'];
		$config['base_url'] = site_url('/admin/warehouse');
		$config['total_rows'] = $no_of_warehouse;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		$query =  $this->db->order_by('warehouse_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('warehouse');
		$warehouse_details = $query->result_array();
		$pagination = $this->pagination->create_links();

		$this->load->view('warehouse', array(
				'warehouse_details'=>$warehouse_details,
				'pagination'=>$pagination
			)
		);
	}//function

	public function add_warehouse(){
		$warehouse_name = $this->input->post('warehouse_name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$q = $this->db->where( array('username'=>$username) )->get('warehouse');
		if( $q->num_rows() >0 ){
			$this->session->set_flashdata( 'username_exist' , 'Warehouse With This username Exists' );
			return redirect('admin/warehouse');
		}else{

			$data = array(
			'warehouse_name'  	=> $warehouse_name,
			'username'	 		=> $username,
			'password'			=> base64_encode($password),
			'created_by'		=> $this->session->userdata['admin_id'],
			);

			$return = $this->db->insert('warehouse', $data);
			if( $return ){
				return redirect('admin/warehouse');
			}else{
				$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
				return redirect('admin/warehouse');
			}
		}
		

	}//function

	public function edit_warehouse( $warehouse_id ){
		$query = $this->db->where( array('warehouse_id'=>$warehouse_id) )->get('warehouse');
		$warehouse_details = $query->result_array();

		$this->load->view('edit_warehouse', array(
				'warehouse_details'=>$warehouse_details,
				
			)
		);
	}//function

	public function edit_warehouse_to_db(){
		$warehouse_name = $this->input->post('warehouse_name');
		$warehouse_id = $this->input->post('warehouse_id');
		

		$data = array(
			'warehouse_name'  => $warehouse_name,
			'modified_by'	=> $this->session->userdata['admin_id'],
		);

		$return = $this->db->update('warehouse', $data, array('warehouse_id' => $warehouse_id));
        if( $return ){
        	return redirect("admin/warehouse");
        }else{
        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
        	return redirect("admin/edit_warehouse/$warehouse_id");
        }
	}//function

	public function delete_warehouse(){
		$warehouse_id = $this->input->post('warehouse_id');
		$delete = $this->db->delete('warehouse', array('warehouse_id' => $warehouse_id));
		if($delete){
			echo $warehouse_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}//function



	/*
	|--------------------------------------------------------------------------
	| Order function
	|--------------------------------------------------------------------------
	*/

	public function order_list(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_orders = $this->data['no_of_orders'];
		$config['base_url'] = site_url('/admin/order_list');
		$config['total_rows'] = $no_of_orders;
		$config['per_page'] = 4;

		$this->pagination->initialize($config);
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->order_by('order_id','ASC')
		->limit($config['per_page'],$this->uri->segment(3))
		->get();
		$order_details = $query->result_array();
		$pagination = $this->pagination->create_links();


		// $query1 = $this->db->where( array('franchise_id'=>$this->session->userdata['franchise_id']) )->get('pf_order');
		
		// $no_of_order = $query1->num_rows();

		$this->load->view('order/order', array(
				'order_details'=>$order_details,
				'pagination'=>$pagination,
				'no_of_order'	=> $no_of_orders
			)
		);
	}//function

	public function view_order( $order_id ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->where( array( 'order_id' => $order_id ) )
        ->get();

        $order_detail = $query->result_array();

		

		$this->load->view('order/view_order', array(
				'order_detail'	=>$order_detail,
			)
		);
		

	}//function


	public function pending_order(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_pending_orders = $this->data['no_of_pending_orders'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->join('delivery_address','pf_order.user_id = delivery_address.user_id')
		->where( array( 'cloth_status' => '0' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('order/pending', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_pending_orders
			)
		);
	}//function

	public function view_pending( $order_code ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$order_detail = $this->Order->get_order_by_code( $order_code );

		$this->load->view('order/view_pending', array(
				'order_detail'	=>$order_detail,
			)
		);
		

	}//function


	public function pickup_assigned(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
		->where( array( 'cloth_status' => '1' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}
		$this->load->view('order/assigned_pickup', array(
					'order_details'=>$array,
				)
			);
	}//function

	public function view_pickup_assigned( $order_code ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('order/view_pickup_assigned', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function


	public function picked_up(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'cloth_status' => '2' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('order/picked_up', array(
				'order_details'=>$array,
			)
		);
	}//function


	public function view_picked_up( $order_code ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		$this->load->view('order/view_picked_up', array(
				'order_detail'	=>$order_detail,
			)
		);
		

	}//function

	public function in_warehouse(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}

		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array(  'cloth_status' => '3' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('order/in_warehouse', array(
				'order_details'=>$array,
			)
		);
	}//function

	public function view_in_warehouse( $order_code ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('warehouse','pf_order.warehouse_id = warehouse.warehouse_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('order/view_in_warehouse', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function

	public function washed(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'cloth_status' => '4' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('order/washed', array(
				'order_details'=>$array,
			)
		);
	}//function

	public function view_washed( $order_code ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->join('warehouse','pf_order.warehouse_id = warehouse.warehouse_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		


		$this->load->view('order/view_washed', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function


	public function delivery_assigned(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array('cloth_status' => '5' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('order/assigned_delivery', array(
				'order_details'=>$array,
			)
		);
	}//function

	public function view_delivery_assigned( $order_code ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.pick_up_biker_id = biker.biker_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('order/view_delivery_assigned', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function

	public function complete(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->where( array( 'cloth_status' => '6' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('order/complete', array(
				'order_details'=>$array,
			)
		);
	}//function

	public function view_complete( $order_code ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query =  $this->db
        ->select('*')
        ->from('pf_order')
        ->join('service','pf_order.service_id = service.service_id')
        ->join('biker','pf_order.delivery_biker_id = biker.biker_id')
        ->join('franchise','pf_order.franchise_id = franchise.franchise_id')
        ->join('warehouse','pf_order.warehouse_id = warehouse.warehouse_id')
        ->where( array( 'order_code' => $order_code ) )
        ->get();

        $order_detail = $query->result_array();
		// $order_detail = $this->Order->get_order_by_code( $order_code );


		$this->load->view('order/view_complete', array(
				'order_detail'	=>$order_detail,
				
			)
		);
		

	}//function

	public function complain(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$q = $this->db
				->select('*')
				->from('complain')
				->join('franchise','complain.c_franchise_id = franchise.franchise_id')
				->where( array( 'c_status' => '0' ) )
				->get();
		$complain_details = $q->result_array();

		$this->load->view('order/complain', array(
				'complain_details'=>$complain_details,
			)
		);
	}//function

	public function accept_complain(){
		$complain_id = $this->input->post('complain_id');
		$query =  $this->db
					->select('*')
					->from('complain')
					->where(array( "complain_id"=>$complain_id ))
					->get();
		$complain_details =$query->result_array();
		$order_id = $complain_details[0]['c_order_id'];

		$query2 = $this->db->get('pf_order');
		$no_of_order = $query2->num_rows();
		$order_no = $no_of_order+1;
		$order_code = 'pws-'.$order_no;

		$query1 =  $this->db
						->select('*')
						->from('pf_order')
						->where( array('order_id'=>$order_id))
						->get();
		$order_details = $query1->result_array();

		$order_data = array(
			'order_code'				=> $order_code,
			'user_id'	 				=> $order_details[0]['user_id'],
			'name'						=> $order_details[0]['name'],
			'service_id'				=> $order_details[0]['service_id'],
			'cloth_type'				=> $order_details[0]['cloth_type'],
			'franchise_id'				=> $order_details[0]['franchise_id'],
			'price_per_unit'			=> 0,
			'quantity'	 				=> $order_details[0]['quantity'],
			'total_price'				=> 0,
			'cloth_status'				=> '0',
			'payment_mode'				=> $order_details[0]['payment_mode'],
			'payment_status'			=> '2',
			'status	'	  				=> '1',
			'expected_delivery_date'	=> date("Y/m/d", strtotime("+ 3 days")),
			'address_id'	 			=> $order_details[0]['address_id'],	
			'basket_id'	 				=> $order_details[0]['basket_id'],	
			'complain'					=> '1',
			'created_by'				=> $order_details[0]['created_by']
		);
		$insert = $this->db->insert('pf_order', $order_data);
		if( $insert ){
				$data['c_status'] = '1';
				$return = $this->db->update('complain', $data, array('complain_id'=>$complain_id));
				echo 1;
		}else{
			echo 0;
		}

	}//function

	public function reject_complain(){
		$complain_id = $this->input->post('complain_id');
		$data['c_status'] = '5';
		$return = $this->db->update('complain', $data, array('complain_id'=>$complain_id));
		if( $return ){
			echo 1;
		}else{
			echo 0;
		}
	}//function

	public function accepted_complain(){
		$q = $this->db
				->select('*')
				->from('complain')
				->join('franchise','complain.c_franchise_id = franchise.franchise_id')
				->where( array( 'c_status' => '1' ) )
				->get();
		$complain_details = $q->result_array();

		$this->load->view('order/accepted_complain', array(
				'complain_details'=>$complain_details,
			)
		);
	}//function
	public function rejected_complain(){
		$q = $this->db
				->select('*')
				->from('complain')
				->join('franchise','complain.c_franchise_id = franchise.franchise_id')
				->where( array( 'c_status' => '5' ) )
				->get();
		$complain_details = $q->result_array();

		$this->load->view('order/rejected_complain', array(
				'complain_details'=>$complain_details,
			)
		);
	}//function

	/*
	|--------------------------------------------------------------------------
	| Feedback
	|--------------------------------------------------------------------------
	*/
	public function feedback(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query = $this->db->get('feedback');
		$no_of_feedback = $query->num_rows();
		$config['base_url'] = site_url('/admin/feedback');
		$config['total_rows'] = $no_of_feedback;
		$config['per_page'] = 5;

		$this->pagination->initialize($config);
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query1 =  $this->db
		->select('*')
		->from('feedback')
		->order_by('feedback_id','DESC')
		->limit($config['per_page'],$this->uri->segment(3))
		->get();
		$feedback_details = $query1->result_array();
		$pagination = $this->pagination->create_links();
		$this->load->view('feedback', array(
				'feedback_details'	=>$feedback_details,
				'pagination' => $pagination,
				'no_of_feedback' => $no_of_feedback,
				"no_of_orders" => $this->data['no_of_orders']
				
			)
		);
	}


	/*
	|--------------------------------------------------------------------------
	| About Us
	|--------------------------------------------------------------------------
	*/

	public function about_us(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$q1 = $this->db->get('about_us');
		$about = $q1->result_array();
		$about_us = $about[0]['content'];
		$this->load->view('about_us', array(
				'about_us'	=>$about_us,
			)
		);
	}

	public function manage_about_us(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$about_us = $this->input->post('about_us');
		$data['content'] = $about_us;
		$return = $this->db->update('about_us', $data, array('idddd' => 1));
        if( $return ){
        	$this->session->set_flashdata( 'insert_content' , 'Content Saved successfully !' );
        	return redirect("admin/about_us");
        }else{
        	$this->session->set_flashdata( 'insert_content_error' , 'Database error ! Cannot insert !' );
        	return redirect("admin/about_us");
        }
	}	



	/*
	|--------------------------------------------------------------------------
	|FAQ
	|--------------------------------------------------------------------------
	*/

	public function faq(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$q1 = $this->db->get('faq');
		$faq_details = $q1->result_array();
		
		$this->load->view('faq', array(
				'faq_details'	=>$faq_details,
			)
		);
	}//fun
	public function add_faq(){
		$faq_question = $this->input->post('faq_question');
		$faq_answer = $this->input->post('faq_answer');

		$data = array(
			'faq_question'  	=> $faq_question,
			'faq_answer'	 		=> $faq_answer,
			'created_by'		=> $this->session->userdata['admin_id'],
		);

		$return = $this->db->insert('faq', $data);
		if( $return ){
			return redirect('admin/faq');
		}else{
			$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
			return redirect('admin/faq');
		}
		
	}//fun

	public function edit_faq( $faq_id ){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$query = $this->db->where( array('faq_id'=>$faq_id) )->get('faq');
		$faq_details = $query->result_array();

		$this->load->view('edit_faq', array(
				'faq_details'=>$faq_details,
				
			)
		);
	}//fun
	public function edit_faq_db(){
		$faq_id = $this->input->post('faq_id');
		$faq_question = $this->input->post('faq_question');
		$faq_answer = $this->input->post('faq_answer');
		

		$data = array(
			'faq_question'  	=> $faq_question,
			'faq_answer'	 	=> $faq_answer,
			'modified_by'		=> $this->session->userdata['admin_id'],
		);

		$return = $this->db->update('faq', $data, array('faq_id' => $faq_id));
        if( $return ){
        	return redirect("admin/faq");
        }else{
        	$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
        	return redirect("admin/edit_faq/$faq_id");
        }
	}//fun
	public function delete_faq(){
		$faq_id = $this->input->post('faq_id');
		$delete = $this->db->delete('faq', array('faq_id' => $faq_id));
		if($delete){
			echo $faq_id.' '.'deleted';
		}
		else{
			echo('unable to delete');
		}
	}//fun


	/*
	|--------------------------------------------------------------------------
	|Gallery
	|--------------------------------------------------------------------------
	*/

	public function gallery(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$q1 = $this->db->get('gallery');
		$gallery_details = $q1->result_array();
		
		$this->load->view('gallery', array(
				'gallery_details'	=>$gallery_details,
			)
		);
	}
	public function add_gallery(){
		$gal_title = $this->input->post('gal_title');

		$config['upload_path']          = './uploads/gallery/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['max_width']            = 4500;
        $config['max_height']           = 3000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('gal_image')){
            $upload_error = $this->upload->display_errors();
            $this->session->set_flashdata( 'upload_error' , $upload_error );
			return redirect('admin/gallery');
        }else{
            $data =  $this->upload->data();
            $gal_image = $data["file_name"];

         	 $data1 = array(
				'gal_title'		=> $gal_title,
				'gal_image'		=> $gal_image,
				'created_by'	=> $this->session->userdata['admin_id'],
			);

         	$return = $this->db->insert('gallery', $data1);
			if( $return ){
				return redirect('admin/gallery');
			}else{
				$this->session->set_flashdata( 'insert_error' , 'Database error ! Cannot insert !' );
				return redirect('admin/gallery');
			}

         	
             
            
        }// if of image upload
	}// fun

	public function delete_gallery(){
		$gallery_id = $this->input->post('gallery_id');
		$gal_image = $this->input->post('gal_image');
		$image_path = './uploads/gallery/'.$gal_image;
		$delete = $this->db->delete('gallery', array('gallery_id' => $gallery_id));
		if( $delete ){
			echo $delete;
			$unlink = unlink($image_path);
			echo $unlink;
		}
	}//function

	/*
	|--------------------------------------------------------------------------
	|Offer
	|--------------------------------------------------------------------------
	*/

	public function offer(){
		if( ! $this->session->userdata['admin_id'] ){
			return redirect('admin');
			die();
		}
		$q1 = $this->db->get('offers');
		$offer_details = $q1->result_array();
		
		$this->load->view('offers', array(
				'offer_details'	=>$offer_details,
			)
		);
	}
	public function add_offer(){
		$offer_title = $this->input->post('offer_title');

		$config['upload_path']          = './uploads/offer/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 2000;
        $config['max_width']            = 4500;
        $config['max_height']           = 3000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('offer_image')){
            $upload_error = $this->upload->display_errors();
            $this->session->set_flashdata( 'upload_error' , $upload_error );
			return redirect('admin/offer');
        }else{
            $data =  $this->upload->data();
            $offer_image = $data["file_name"];

         	 $data1 = array(
				'offer_title'		=> $offer_title,
				'offer_image'		=> $offer_image,
				'created_by'		=> $this->session->userdata['admin_id'],
			);

         	$return = $this->db->insert('offers', $data1);
			if( $return ){
				return redirect('admin/offer');
			}else{
				$this->session->set_flashdata( 'insert_error' , 'Database error ! Cannot insert !' );
				return redirect('admin/offer');
			}

         	
             
            
        }// if of image upload
	}// fun

	public function delete_offer(){
		$offers_id = $this->input->post('offers_id');
		$offer_image = $this->input->post('offer_image');
		$image_path = './uploads/offer/'.$offer_image;
		$delete = $this->db->delete('offers', array('offers_id' => $offers_id));
		if( $delete ){
			echo $delete;
			$unlink = unlink($image_path);
			echo $unlink;
		}
	}//function

	/*
	|--------------------------------------------------------------------------
	| The End
	|--------------------------------------------------------------------------
	*/

}//class