<?php
	include ('header.php');
	// print_r($feedback_details);
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Feedback</h1>
            
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Orders</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $no_of_orders;  ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Feedback</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $no_of_feedback;  ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Feedback List</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                  	<div class="table-responsive">
	                   <table class="table">
	                          <thead>
	                            <tr>
	                              <th scope="col">Order Code </th>
	                              <th scope="col">Name</th>
	                              <th scope="col">Feedback</th>
	                              <th scope="col">View</th>
	                            </tr>
	                          </thead>
	                          <tbody>
	                          	<?php 
	                          	foreach ($feedback_details as $feedback_detail){
	                          	?>
	                          	<tr>
	                          		<td ><?php echo $feedback_detail['order_code'];  ?></td>
	                          		<td ><?php echo $feedback_detail['customer_name'];  ?></td>
	                          		<td ><?php echo $feedback_detail['feedback'];  ?></td>
	                          		<td ><a href="<?php echo site_url('admin/view_complete/'.$feedback_detail['order_code']) ?>" class="btn btn-primary"> View</a><td>
	                          	</tr>
	                          	<?php  } ?>
	                          </tbody>
	                      </table>
	                  </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>

           
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>
