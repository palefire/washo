<?php
	include ('header.php');
 ?>
 <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            
          </div>

          <?php 
            $insert_content_error = $this->session->flashdata('insert_content_error');
            $insert_content = $this->session->flashdata('insert_content');
            if( $insert_content_error ){
           ?>
            <div class="alert alert-dismissible alert-warning">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4 class="alert-heading">Check!</h4>
              <p class="mb-0"><?php echo $insert_content_error; ?></p>
            </div>
          <?php } 
          if( $insert_content ){
          ?>
          <div class="alert alert-dismissible alert-success">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4 class="alert-heading">Great!</h4>
              <p class="mb-0"><?php echo $insert_content; ?></p>
            </div>
        <?php } ?>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">About US</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <?php echo form_open_multipart('admin/manage_about_us', array('id'=>'add-product-form') ); ?>
                        <textarea name="about_us" style="width:100%;height:100%"><?php echo $about_us;  ?></textarea>
                        <input type="submit" class="btn btn-primary" style="margin-top:50px;float:right">
                      </form>
                    
                  </div>
                </div>
              </div>
            </div>

          
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

<script >
  $(document).ready( function(){
    $('.delete-franchise').on('click', function(){
      // var service_id = $(this).data('id');
      var franchise_id = $(this).attr('data-id');

     
     if(confirm( 'Do You want to delete the Franchise?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/delete_franchise') ?>",
          type : 'POST',
          data : {
            'franchise_id' : franchise_id
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/genre_list') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>