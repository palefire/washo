<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($franchise_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Franchise List</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Franchise</th>
                            <th scope="col">Pin</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $slno = 1;
                          foreach( $franchise_details as $franchise_detail ){ 
                          ?>
                          <tr>
                            <th scope="row"><?php echo $slno; ?></th>
                            <td><?php echo $franchise_detail['franchise_name'] ?></td>
                            <td><?php echo $franchise_detail['franchise_pin'] ?></td>
                            <td><a href="<?php echo base_url('/admin/edit_franchise/').$franchise_detail['franchise_id']; ?>" style="color: blue">Edit</a></td>
                            <td><span style="color: red; cursor: pointer" class="delete-franchise" data-id="<?php echo $franchise_detail['franchise_id']; ?>">Delete</span></td>
                          </tr>
                          <?php 
                          $slno++;
                          } 
                          ?>
                        </tbody>
                      </table>
                    </div>
                    <?php echo $pagination; ?>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Franchise</h6>
                
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <?php 
                    $insert_service_error = $this->session->flashdata('insert_service_error');
                    $franchise_exist = $this->session->flashdata('franchise_exist');
                    $franchise_pin_exist = $this->session->flashdata('franchise_pin_exist');
                    if( $insert_service_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_service_error; ?></p>
                    </div>
                  <?php } 
                  if( $franchise_exist ){
                  ?>
                   <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $franchise_exist; ?></p>
                    </div>
                  <?php }
                   if( $franchise_pin_exist ){ ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $franchise_pin_exist; ?></p>
                    </div>
                  <?php } ?>
                  <?php echo form_open_multipart('admin/add_franchise', array('id'=>'add-product-form') ); ?>
                  <table>
                    <tr>
                      <td><label>Franchise Name</label></td>
                      <td><input type="text" name="franchise_name" required></td>
                    </tr>
                    <tr>
                      <td><label>Franchise Pin</label></td>
                      <td><input type="text" name="franchise_pin" required></td>
                    </tr>
                    <tr>
                      <td><label>Franchise Username</label></td>
                      <td><input type="text" name="franchise_username" required></td>
                    </tr>
                    <tr>
                      <td><label>Franchise Password</label></td>
                      <td><input type="password" name="franchise_password" required></td>
                    </tr>

                   
                    <tr>
                      <td colspan="2">
                        <input type="submit" name="add_franchise" value="Add Franchise">
                      </td>
                    </tr>
                  </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

<script >
  $(document).ready( function(){
    $('.delete-franchise').on('click', function(){
      // var service_id = $(this).data('id');
      var franchise_id = $(this).attr('data-id');

     
     if(confirm( 'Do You want to delete the Franchise?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/delete_franchise') ?>",
          type : 'POST',
          data : {
            'franchise_id' : franchise_id
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/genre_list') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>