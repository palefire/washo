</div>
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js')?>"></script>

  <script >
  $(document).ready( function(){
    $('.delete-cloth').on('click', function(){
      // var service_id = $(this).data('id');
      var cloth_id = $(this).attr('data-id');

     
     if(confirm( 'Do You want to delete the Cloth?' )){
     	
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/delete_cloth') ?>",
          type : 'POST',
          data : {
            'cloth_id' : cloth_id
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/genre_list') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>
</body>
</html>