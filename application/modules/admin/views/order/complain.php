<?php
  include (APPPATH.'modules/admin/views/header.php');
  // echo '<pre>';
  // print_r($complain_details);
  // echo '</pre>';
  //Array ( [0] => Array ( [complain_id] => 1 [c_order_id] => 1 [c_order_code] => pws-1 [c_name] => Shirt [c_service_id] => 2 [c_cloth_type] => 1 [c_franchise_id] => 1 [c_price] => 15 [c_quantity] => 1 [c_user_id] => 1 [created_by] => 1 [created_at] => 2020-04-23 12:06:28 [modified_by] => ) [1] => Array ( [complain_id] => 2 [c_order_id] => 2 [c_order_code] => pws-1 [c_name] => Pant [c_service_id] => 3 [c_cloth_type] => 1 [c_franchise_id] => 1 [c_price] => 25 [c_quantity] => 1 [c_user_id] => 1 [created_by] => 1 [created_at] => 2020-04-23 12:06:58 [modified_by] => ) )
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard / Complain</h1>
            
          </div>

        

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
              
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
                        <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Order Code </th>
                              <th scope="col">Cloth Name</th>
                              <th scope="col">Franchise Name</th>
                              <th scope="col">Quantity</th>
                              <th scope="col">Price</th>
                              <th scope="col">Action</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                           <?php   foreach( $complain_details as $complain ){ ?>
                            <tr>
                              <td><?php echo $complain['c_order_code']  ?></td>
                              <td><?php echo $complain['c_name']  ?></td>
                              <td><?php echo $complain['franchise_name']  ?></td>
                              <td><?php echo $complain['c_quantity']  ?></td>
                              <td><?php echo $complain['c_price']  ?></td>
                              <td>
                                <span class="btn btn-success accept-complain" data-id=<?php echo $complain['complain_id']  ?> >Accept</span>
                                <span class="btn btn-danger reject-complain" data-id=<?php echo $complain['complain_id']  ?> >Reject</span>
                              </td>
                            </tr>
                           <?php   } ?>
                          </tbody>
                        </table>
                      </div>

                  </div>
                </div>


              </div>
            </div>
            

          
          </div>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
  include (APPPATH.'modules/admin/views/footer.php');
?>
<script>
  $(document).ready( function(){
    $('.reject-complain').on('click', function(e){
      var complain_id = $(this).data('id');
      // alert(complain_id);
      if(confirm( 'Do You want to Reject the Complain Order?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/reject_complain') ?>",
          type : 'POST',
          data : {
            'complain_id' : complain_id
          },
          success: function( data ){
            console.log(data);
            
            if( data == 1 ){
              alert('Complain Order was successfully Rejected');
            }else{
              alert('OOPS! Something Went Wrong. Try after some time. !')
            }
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
    });





     $('.accept-complain').on('click', function(e){
      var complain_id = $(this).data('id');
      if(confirm( 'Do You want to Accept the Complain Order?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/accept_complain') ?>",
          type : 'POST',
          data : {
            'complain_id' : complain_id
          },
          success: function( data ){
            console.log(data);
            
            if( data == 1 ){
              alert('Complain Order was successfully Accepted');
            }else{
              alert('OOPS! Something Went Wrong. Try after some time. !')
            }
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }

    });
  });
</script>