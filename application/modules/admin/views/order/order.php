<?php
	include (APPPATH.'modules/admin/views/header.php');
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard / Orders</h1>
            
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Orders Managed</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $no_of_order ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Orders Completed</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">0</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pending Orders</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                        </div>
                        <div class="col">
                          <div class="progress progress-sm mr-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
              
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
                        <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Order Code </th>
                              <th scope="col">Name</th>
                              <th scope="col">Service</th>
                              <th scope="col">Quantity</th>
                              <th scope="col">Total Price</th>
                              <th scope="col">Action</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                            foreach( $order_details as $order_detail ){ 
                              // print_r($order_details);
                              //Array ( [0] => Array ( [order_id] => 1 [order_code] => washo-20200228-1 [user_id] => 1 [name] => Shirt [service_id] => 1 [cloth_type] => 1 [franchise_id] => 1 [price_per_unit] => 20 [quantity] => 2 [total_price] => 40 [cloth_status] => 0 [payment_mode] => 1 [transaction_id] => [payment_status] => 1 [status] => 1 [order_date] => 2020-03-09 14:33:22 [delivery_date] => [pick_up_biker_id] => [warehouse_id] => [delivery_biker_id] => [created_by] => 1 [created_at] => 2020-03-03 12:33:24 [modified_by] => 1 [service_name] => Wash [service_slug] => wash-0 [svg] => wash.svg ) )
                              $quantity = $order_detail['quantity'];
                              $price_per_unit = $order_detail['price_per_unit'];
                              $total_price = intval( $quantity ) * $price_per_unit;
                            ?>
                              <tr>
                                <td><?php echo $order_detail['order_code']; ?></td>
                                <td><?php echo $order_detail['name']; ?></td>
                                <td><?php echo $order_detail['service_name']; ?></td>
                                <td><?php echo $order_detail['quantity']; ?></td>
                                <td><?php echo $total_price; ?></td>
                                <td><a href="<?php echo base_url('admin/view_order/').$order_detail['order_id']; ?>" class="btn btn-primary">View</a></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <?php echo $pagination ?>

                  </div>
                </div>


              </div>
            </div>

          
          </div>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include (APPPATH.'modules/admin/views/footer.php');
?>