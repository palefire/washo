<?php
  include ('header.php');
   //print_r($cloth_details);
  $CI =& get_instance();
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            <a href="<?php echo base_url('admin/add_clothes') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Add Clothes</a>
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">MHousehold Clothes</h6>
                </div>
             
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Cloth Name</th>
                            <th scope="col">Service</th>
                            <th scope="col">Price</th>
                            <th scope="col">Time</th>
                            <th scope="col">Icon</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          //Array ( [0] => Array ( [cloth_id] => 1 [cloth_name] => Shirt [cloth_slug] => shirt-0 [cloth_type] => 1 [service_type] => 1 [price] => 20 [time] => 96 [created_by] => 1 [created_at] => 2020-03-03 12:33:24 [modified_by] => 1 [service_id] => 1 [service_name] => Wash [service_slug] => wash-0 ) )
                          $slno = 1;
                          foreach( $cloth_details as $cloth_detail ){ 
                           
                          ?>
                          <tr>
                            <th scope="row"><?php echo $slno; ?></th>
                            <td><?php echo $cloth_detail['cloth_name'] ?></td>
                            <td><?php  echo $cloth_detail['service_name']  ?></td>
                            <td><?php echo $cloth_detail['price'] ?></td>
                            <td><?php echo $cloth_detail['time'] ?></td>
                            <td>
                              <?php if( $cloth_detail['icon'] !='' ){  ?>
                              <img src="<?php echo $this->data['svg_path'].$cloth_detail['icon'] ?>" style="height: 22px;">
                              <?php  } ?>
                            </td>
                            <td><a href="<?php  echo base_url('/admin/edit_clothes/').$cloth_detail['cloth_id']; ?>" style="color: blue">Edit</a></td>
                             <td><span style="color: red" class="delete-cloth" data-id="<?php  echo $cloth_detail['cloth_id']; ?>">Delete</span></td>
                          </tr>
                          <?php 
                          $slno++;
                          } 
                          ?>
                        </tbody>
                      </table>
                    </div>
                    <?php echo $pagination; ?>
                  </div>
                </div>
              </div>
            </div>

         
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
  include ('footer.php');
?>