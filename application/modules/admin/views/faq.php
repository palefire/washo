<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($franchise_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">FAQ</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Question</th>
                            <th scope="col">Answer</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $slno = 1;
                          foreach( $faq_details as $faq_detail ){ 
                          ?>
                          <tr>
                            <th scope="row"><?php echo $slno; ?></th>
                            <td><?php echo $faq_detail['faq_question'] ?></td>
                            <td><?php echo $faq_detail['faq_answer'] ?></td>
                            <td><a href="<?php echo base_url('/admin/edit_faq/').$faq_detail['faq_id']; ?>" style="color: blue">Edit</a></td>
                            <td><span style="color: red; cursor: pointer" class="delete-faq" data-id="<?php echo $faq_detail['faq_id']; ?>">Delete</span></td>
                          </tr>
                          <?php 
                          $slno++;
                          } 
                          ?>
                        </tbody>
                      </table>
                    </div>
                    
                  </div>
                </div>
            
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add FAQ</h6>
                
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <?php 
                    $insert_service_error = $this->session->flashdata('insert_service_error');
                    $franchise_exist = $this->session->flashdata('franchise_exist');
                    $franchise_pin_exist = $this->session->flashdata('franchise_pin_exist');
                    if( $insert_service_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_service_error; ?></p>
                    </div>
                  <?php } 
                  if( $franchise_exist ){
                  ?>
                   <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $franchise_exist; ?></p>
                    </div>
                  <?php }
                   if( $franchise_pin_exist ){ ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $franchise_pin_exist; ?></p>
                    </div>
                  <?php } ?>
                  <?php echo form_open_multipart('admin/add_faq', array('id'=>'add-product-form') ); ?>
                  <table>
                    <tr>
                      <td><label>Question</label></td>
                      <td><input type="text" name="faq_question" required></td>
                    </tr>
                    <tr>
                      <td><label>Answer</label></td>
                      <td><textarea name="faq_answer"></textarea></td>
                    </tr>
                    

                   
                    <tr>
                      <td colspan="2">
                        <input type="submit" name="add_faq" value="Add FAQ">
                      </td>
                    </tr>
                  </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

<script >
  $(document).ready( function(){
    $('.delete-faq').on('click', function(){
      // var service_id = $(this).data('id');
      var faq_id = $(this).attr('data-id');

     
     if(confirm( 'Do You want to delete the FAQ?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/delete_faq') ?>",
          type : 'POST',
          data : {
            'faq_id' : faq_id
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/genre_list') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>