<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($service_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
           
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Services List</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Service</th>
                            <th scope="col">Icon</th>
                            <th scope="col">Image</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $slno = 1;
                          foreach( $service_details as $service_detail ){ 
                          ?>
                          <tr>
                            <th scope="row"><?php echo $slno; ?></th>
                            <td><?php echo $service_detail['service_name'] ?></td>
                            <td><img src="<?php echo $this->data['svg_path'].$service_detail['svg'] ?>" style="height: 22px;"></td>
                            <td><img src="<?php echo site_url('/uploads/services/').$service_detail['img'] ?>" style="height: 22px;"></td>
                            <td><a href="<?php echo base_url('/admin/edit_service/').$service_detail['service_id']; ?>" style="color: blue">Edit</a></td>
                            <td><span style="color: red; cursor: pointer;" class="delete-service" data-id="<?php echo $service_detail['service_id']; ?>">Delete</span></td>
                          </tr>
                          <?php 
                          $slno++;
                          } 
                          ?>
                        </tbody>
                      </table>
                    </div>
                    <?php echo $pagination; ?>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Services</h6>
                
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <?php 
                    $insert_service_error = $this->session->flashdata('insert_service_error');
                    $upload_error = $this->session->flashdata('upload_error');
                    if( $insert_service_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_service_error; ?></p>
                    </div>
                  <?php } 
                    if( $upload_error ){
                  ?>
                  <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $upload_error; ?></p>
                    </div>
                <?php  } ?>
                  <?php echo form_open_multipart('admin/add_services', array('id'=>'add-product-form') ); ?> 
                  <!-- <form> -->
                  <table>
                    <tr>
                      <td><label>Service Name</label></td>
                      <td><input type="text" name="service_name" required></td>

                    </tr>
                    <tr>
                      <td><labek> Icon </labek></td>
                      <td>
                        <select name="svg">
                            <option value="placeholder.svg">Select Icon From the list</option>
                            <option value="basket.svg">Basket</option>
                            <option value="bed_sheet.svg">Bed Sheet</option>  
                            <option value="blanket.svg">Blanket</option>
                            <option value="blazer.svg">Blazers </option>
                            <option value="boxer.svg">Boxers   </option>
                            <option value="cap.svg">Cap</option>
                            <option value="clean_cloth.svg">Clean Cloth</option>
                            <option value="curtain.svg">Curtain</option>
                            <option value="dress.svg">Dress</option>
                            <option value="dry_clean.svg">Dry Cleaning</option>
                            <option value="drying.svg">Drying Cloth</option>
                            <option value="hankerchief.svg">Hankerchief</option>
                            <option value="hoody.svg">Hoody</option>
                            <option value="iron.svg">Iron</option>
                            <option value="jacket.svg">Jacket</option>
                            <option value="ladies_pants.svg">Ladies Pants   </option>
                            <option value="bra.svg">Ladies Underwear</option>
                            <option value="leather_jacket.svg">Leather Jacket</option>
                            <option value="leggins.svg">Leggins </option>
                            <option value="long_skirt.svg">Long Skirt</option>
                            <option value="night_gown.svg">Ladies Night Gown  </option>
                            <option value="pants.svg">Pants</option>
                            <option value="pillow.svg">Pillow</option>
                            <option value="pyajama.jpg">Pyajamas    </option>
                            <option value="shirt.svg">Shirt</option>
                            <option value="skirt.svg">Skirt</option>
                            <option value="sofa.svg">Sofa </option>
                            <option value="socks.svg">Socks</option>
                            <option value="steam_wash.svg">Steam wash</option>
                            <option value="suit.svg">Suit</option>
                            <option value="sweater.svg">Sweater</option>
                            <option value="top.svg">Top</option>
                            <option value="towel.svg">Towel</option>
                            <option value="track_pants.svg">Track Pants</option>
                            <option value="tshirt.svg">Tshirt</option>
                            <option value="waistcoat.svg">Waistcoat</option>
                            <option value="wash.svg">Wash</option>
                            <option value="wash_iron.svg">Wash & Iron </option>
                            <option value="washing_machine.svg">Washing machine</option>
                      </select>
                      </td>
                      
                    </tr>
                   <tr>
                     <td><label>Service Image</label></td>
                      <td><input type="file" name="service_img" required></td>
                   </tr>
                    <tr>
                      <td colspan="2">
                        <input type="submit" name="add_service" value="Add Service">
                      </td>
                    </tr>
                  </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- Row -->








             <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Icon List</h6>
                </div>
               
                <div class="row" style="padding: 10px;">
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/wash.svg" style="height: 20px;"> <span style="padding-left: 10px;">Wash</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/iron.svg" style="height: 20px;"> <span style="padding-left: 10px;">Iron</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/wash_iron.svg" style="height: 20px;"> <span style="padding-left: 10px;">Wash & Iron</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/dry_clean.svg" style="height: 20px;"> <span style="padding-left: 10px;">Dry Cleaning</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/steam_wash.svg" style="height: 20px;"> <span style="padding-left: 10px;">Steam Wash</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/washing_machine.svg" style="height: 20px;"> <span style="padding-left: 10px;">Washing Machine</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/basket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Basket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/clean_cloth.svg" style="height: 20px;"> <span style="padding-left: 10px;">Clean Cloth</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/drying.svg" style="height: 20px;"> <span style="padding-left: 10px;">Drying Cloth</span></div>

               
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/shirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Shirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/tshirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Tshirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/pants.svg" style="height: 20px;"> <span style="padding-left: 10px;">Pants</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/ladies_pants.svg" style="height: 20px;"> <span style="padding-left: 10px;">Ladies Pants</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/track_pants.svg" style="height: 20px;"> <span style="padding-left: 10px;">Track Pants</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/boxer.svg" style="height: 20px;"> <span style="padding-left: 10px;">Boxers</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/pyajama.svg" style="height: 20px;"> <span style="padding-left: 10px;">Pyajamas</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/hankerchief.svg" style="height: 20px;"> <span style="padding-left: 10px;">Hankerchief</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/socks.svg" style="height: 20px;"> <span style="padding-left: 10px;">Socks</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/waistcoat.svg" style="height: 20px;"> <span style="padding-left: 10px;">Waistcoat</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/blazer.svg" style="height: 20px;"> <span style="padding-left: 10px;">Blazers</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/jacket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Jacket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/leather_jacket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Leather Jacket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/suit.svg" style="height: 20px;"> <span style="padding-left: 10px;">Suit</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/hoody.svg" style="height: 20px;"> <span style="padding-left: 10px;">Hoody</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/cap.svg" style="height: 20px;"> <span style="padding-left: 10px;">Cap</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/leggins.svg" style="height: 20px;"> <span style="padding-left: 10px;">Leggins</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/bra.svg" style="height: 20px;"> <span style="padding-left: 10px;">Ladies Underwear</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/night_gown.svg" style="height: 20px;"> <span style="padding-left: 10px;">Night Gown</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/dress.svg" style="height: 20px;"> <span style="padding-left: 10px;">Dress</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/skirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Skirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/long_skirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Long Skirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/sweater.svg" style="height: 20px;"> <span style="padding-left: 10px;">Sweater</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/top.svg" style="height: 20px;"> <span style="padding-left: 10px;">Top</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/bed_sheet.svg" style="height: 20px;"> <span style="padding-left: 10px;">Bed Sheet</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/blanket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Blanket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/curtain.svg" style="height: 20px;"> <span style="padding-left: 10px;">Curtain</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/pillow.svg" style="height: 20px;"> <span style="padding-left: 10px;">Pillow</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/sofa.svg" style="height: 20px;"> <span style="padding-left: 10px;">Sofa</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/towel.svg" style="height: 20px;"> <span style="padding-left: 10px;">Towel</span></div>
                    
                   
                    
                </div>
                <div class="card-body">
                  <div class="chart-area">
                    

                  </div>
                </div>
              </div>
            </div>

           
          </div> <!-- Row -->


























            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

<script >
  $(document).ready( function(){
    $('.delete-service').on('click', function(){
      // var service_id = $(this).data('id');
      var service_id = $(this).attr('data-id');

     
     if(confirm( 'Do You want to delete the Service?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/delete_service') ?>",
          type : 'POST',
          data : {
            'service_id' : service_id
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/genre_list') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>