<?php
	include ('header.php');
   // print_r($cloth_detail);
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
           
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Update Clothes</h6>
                
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <?php 
                        $insert_clothes_error = $this->session->flashdata('insert_clothes_error');
                        if( $insert_clothes_error ){
                     ?>
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Check!</h4>
                            <p class="mb-0"><?php echo $insert_clothes_error; ?></p>
                        </div>
                    <?php } 
                    ?>
                    <?php echo form_open_multipart('admin/update_clothes_to_db', array('id'=>'add-product-form') ); ?>
                    <?php 
                    //Array ( [0] => Array ( [cloth_id] => 1 [cloth_name] => Shirt [cloth_slug] => shirt-0 [cloth_type] => 1 [service_type] => 1 [price] => 20 [time] => 96 [created_by] => 1 [created_at] => 2020-03-03 21:44:53 [modified_by] => ) )
                    if( $cloth_detail[0]['cloth_type'] == "1" ){
                      $cloth_type = 'Men';
                      $cloth_type_value = '1';
                    }elseif( $cloth_detail[0]['cloth_type'] == "2" ){
                      $cloth_type = 'Women';
                      $cloth_type_value = '2';
                    }elseif( $cloth_detail[0]['cloth_type'] == "3" ){
                      $cloth_type = 'Household';
                      $cloth_type_value = '3';
                    }else{
                      $cloth_type = 'Kids';
                      $cloth_type_value = '4';
                    }
                    ?>
                      <table>
                        <tr>
                          <input type="hidden" name="cloth_id" value="<?php echo $cloth_detail[0]['cloth_id'] ?>">
                          <td><label>Cloth Name</label></td>
                          <td><input type="text" name="cloth_name" value="<?php echo $cloth_detail[0]['cloth_name'] ?>" required></td>
                        </tr>
                        <tr>
                          <td><label>Cloth Type</label></td>
                          <td>
                            <input type="hidden" name="cloth_type" value="<?php echo $cloth_type_value ?>">
                            <input type="text"  value="<?php echo $cloth_type ?>" readonly>
                            <label style="font-size: 12px;">* Cannot edit Cloth type . Delete the cloth if these field is needed to be edited.</label>

                          </td>
                        </tr>
                        <tr>
                          <td><label>Service Type</label></td>
                          <td>
                            <select name="service_type">
                              <?php
                              foreach($service_details as $service_detail){
                              ?>
                              <option value="<?php echo  $service_detail['service_id']; ?>" <?php if( $service_detail['service_id'] == $cloth_detail[0]['service_type'] ){ echo 'selected'; } ?>><?php echo $service_detail['service_name']?></option>
                              <?php } ?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td><label>Price</label></td>
                          <td><input type="text" name="price" value="<?php echo $cloth_detail[0]['price'] ?>" required></td>
                        </tr>
                        <tr>
                          <td><label>Time Of Service</label></td>
                          <td><input type="text" name="time" value="<?php echo $cloth_detail[0]['time'] ?>" >Hours</td> 
                        </tr>
                        <tr>
                          <td><labek> Icon </labek></td>
                          <td>
                            <select name="svg">

                            <option value="placeholder.svg" <?php if( $cloth_detail[0]['icon'] == 'placeholder.svg' ){ echo 'selected';} ?>>Select Icon From the list</option>
                            <option value="basket.svg" <?php if( $cloth_detail[0]['icon'] == 'basket.svg' ){ echo 'selected';} ?>>Basket</option>
                            <option value="bed_sheet.svg" <?php if( $cloth_detail[0]['icon'] == 'bed_sheet.svg' ){ echo 'selected';} ?>>Bed Sheet</option>  
                            <option value="blanket.svg" <?php if( $cloth_detail[0]['icon'] == 'blanket.svg' ){ echo 'selected';} ?>>Blanket</option>
                            <option value="blazer.svg" <?php if( $cloth_detail[0]['icon'] == 'blazer.svg' ){ echo 'selected';} ?>>Blazers </option>
                            <option value="boxer.svg" <?php if( $cloth_detail[0]['icon'] == 'boxer.svg' ){ echo 'selected';} ?>>Boxers   </option>
                            <option value="cap.svg" <?php if( $cloth_detail[0]['icon'] == 'cap.svg' ){ echo 'selected';} ?>>Cap</option>
                            <option value="clean_cloth.svg" <?php if( $cloth_detail[0]['icon'] == 'clean_cloth.svg' ){ echo 'selected';} ?>>Clean Cloth</option>
                            <option value="curtain.svg"<?php if( $cloth_detail[0]['icon'] == 'curtain.svg' ){ echo 'selected';} ?>>Curtain</option>
                            <option value="dress.svg"<?php if( $cloth_detail[0]['icon'] == 'dress.svg' ){ echo 'selected';} ?>>Dress</option>
                            <option value="dry_clean.svg"<?php if( $cloth_detail[0]['icon'] == 'dry_clean.svg' ){ echo 'selected';} ?>>Dry Cleaning</option>
                            <option value="drying.svg"<?php if( $cloth_detail[0]['icon'] == 'drying.svg' ){ echo 'selected';} ?>>Drying Cloth</option>
                            <option value="hankerchief.svg"<?php if( $cloth_detail[0]['icon'] == 'hankerchief.svg' ){ echo 'selected';} ?>>Hankerchief</option>
                            <option value="hoody.svg"<?php if( $cloth_detail[0]['icon'] == 'hoody.svg' ){ echo 'selected';} ?>>Hoody</option>
                            <option value="iron.svg"<?php if( $cloth_detail[0]['icon'] == 'iron.svg' ){ echo 'selected';} ?>>Iron</option>
                            <option value="jacket.svg"<?php if( $cloth_detail[0]['icon'] == 'jacket.svg' ){ echo 'selected';} ?>>Jacket</option>
                            <option value="ladies_pants.svg"<?php if( $cloth_detail[0]['icon'] == 'ladies_pants.svg' ){ echo 'selected';} ?>>Ladies Pants   </option>
                            <option value="bra.svg"<?php if( $cloth_detail[0]['icon'] == 'bra.svg' ){ echo 'selected';} ?>>Ladies Underwear</option>
                            <option value="leather_jacket.svg"<?php if( $cloth_detail[0]['icon'] == 'leather_jacket.svg' ){ echo 'selected';} ?>>Leather Jacket</option>
                            <option value="leggins.svg"<?php if( $cloth_detail[0]['icon'] == 'leggins.svg' ){ echo 'selected';} ?>>Leggins </option>
                            <option value="long_skirt.svg"<?php if( $cloth_detail[0]['icon'] == 'long_skirt.svg' ){ echo 'selected';} ?>>Long Skirt</option>
                            <option value="night_gown.svg"<?php if( $cloth_detail[0]['icon'] == 'night_gown.svg' ){ echo 'selected';} ?>>Ladies Night Gown  </option>
                            <option value="pants.svg"<?php if( $cloth_detail[0]['icon'] == 'pants.svg' ){ echo 'selected';} ?>>Pants</option>
                            <option value="pillow.svg"<?php if( $cloth_detail[0]['icon'] == 'pillow.svg' ){ echo 'selected';} ?>>Pillow</option>
                            <option value="pyajama.jpg"<?php if( $cloth_detail[0]['icon'] == 'pyajama.svg' ){ echo 'selected';} ?>>Pyajamas    </option>
                            <option value="shirt.svg" <?php if( $cloth_detail[0]['icon'] == 'shirt.svg' ){ echo 'selected';} ?>>Shirt</option>
                            <option value="skirt.svg" <?php if( $cloth_detail[0]['icon'] == 'skirt.svg' ){ echo 'selected';} ?>>Skirt</option>
                            <option value="sofa.svg" <?php if( $cloth_detail[0]['icon'] == 'sofa.svg' ){ echo 'selected';} ?>>Sofa </option>
                            <option value="socks.svg" <?php if( $cloth_detail[0]['icon'] == 'socks.svg' ){ echo 'selected';} ?>>Socks</option>
                            <option value="steam_wash.svg" <?php if( $cloth_detail[0]['icon'] == 'steam_wash.svg' ){ echo 'selected';} ?>>Steam wash</option>
                            <option value="suit.svg" <?php if( $cloth_detail[0]['icon'] == 'suit.svg' ){ echo 'selected';} ?>>Suit</option>
                            <option value="sweater.svg" <?php if( $cloth_detail[0]['icon'] == 'sweater.svg' ){ echo 'selected';} ?>>Sweater</option>
                            <option value="top.svg" <?php if( $cloth_detail[0]['icon'] == 'top.svg' ){ echo 'selected';} ?>>Top</option>
                            <option value="towel.svg" <?php if( $cloth_detail[0]['icon'] == 'towel.svg' ){ echo 'selected';} ?>>Towel</option>
                            <option value="track_pants.svg" <?php if( $cloth_detail[0]['icon'] == 'track_pants.svg' ){ echo 'selected';} ?>>Track Pants</option>
                            <option value="tshirt.svg" <?php if( $cloth_detail[0]['icon'] == 'tshirt.svg' ){ echo 'selected';} ?>>Tshirt</option>
                            <option value="waistcoat.svg" <?php if( $cloth_detail[0]['icon'] == 'waistcoat.svg' ){ echo 'selected';} ?>>Waistcoat</option>
                            <option value="wash.svg" <?php if( $cloth_detail[0]['icon'] == 'wash.svg' ){ echo 'selected';} ?>>Wash</option>
                            <option value="wash_iron.svg" <?php if( $cloth_detail[0]['icon'] == 'wash_iron.svg' ){ echo 'selected';} ?>>Wash & Iron </option>
                            <option value="washing_machine.svg" <?php if( $cloth_detail[0]['icon'] == 'washing_machine.svg' ){ echo 'selected';} ?>>Washing machine</option>
                      </select>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2"><input type="submit" name="add_cloth" value="Save"></td>
                        </tr>
                      </table>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
          
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>