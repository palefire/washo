<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($service_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
           
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit FAQ</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <?php 
                    $insert_service_error = $this->session->flashdata('insert_service_error');
                    if( $insert_service_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_service_error; ?></p>
                    </div>
                  <?php } 
                  ?>
                    <?php echo form_open_multipart('admin/edit_faq_db', array('id'=>'add-product-form') ); ?>
                      <table>
                        <tr>
                          <td><label>Question</label></td>
                          <td>
                            <input type="text" name="faq_question" value="<?php echo $faq_details[0]['faq_question'] ?>">
                            <input type="hidden" name="faq_id" value="<?php echo $faq_details[0]['faq_id'] ?>">
                          </td>
                        </tr>
                        <tr>
                          <td><label>Answer</label></td>
                          <td> <textarea name="faq_answer"><?php echo $faq_details[0]['faq_answer'] ?></textarea></td>
                        </tr>

                       
                        <tr>
                          <td colspan="2">
                            <input type="submit" name="edit_faq" value="Edit FAQ">
                          </td>
                        </tr>
                      </table>
                      </form>
                  </div>
                </div>
              </div>
            </div>

           
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

s