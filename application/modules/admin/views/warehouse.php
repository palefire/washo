<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($franchise_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Warehouse List</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Warehouse Name</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $slno = 1;
                          foreach( $warehouse_details as $warehouse_detail ){ 
                          ?>
                          <tr>
                            <th scope="row"><?php echo $slno; ?></th>
                            <td><?php echo $warehouse_detail['warehouse_name'] ?></td>
                            <td><a href="<?php echo base_url('/admin/edit_warehouse/').$warehouse_detail['warehouse_id']; ?>" style="color: blue">Edit</a></td>
                            <td><span style="color: red; cursor: pointer" class="delete-warehouse" data-id="<?php echo $warehouse_detail['warehouse_id']; ?>">Delete</span></td>
                          </tr>
                          <?php 
                          $slno++;
                          } 
                          ?>
                        </tbody>
                      </table>
                    </div>
                    <?php echo $pagination; ?>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Warhouse</h6>
                
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <?php 
                    $insert_service_error = $this->session->flashdata('insert_service_error');
                    $username_exist = $this->session->flashdata('username_exist');
                    if( $insert_service_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_service_error; ?></p>
                    </div>
                  <?php }
                   if( $username_exist ){
                  ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $username_exist; ?></p>
                    </div>
                 <?php  } ?>
                  <?php echo form_open_multipart('admin/add_warehouse', array('id'=>'add-product-form') ); ?>
                  <table>
                    <tr>
                      <td><label>Warhouse Name</label></td>
                      <td><input type="text" name="warehouse_name" required> </td>
                     </tr>
                     <tr>
                      <td><label>Username</label></td>
                      <td><input type="text" name="username" required> </td>
                    </tr>
                    <tr>
                      <td><label>Password</label></td>
                      <td><input type="text" name="password" required> </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <input type="submit" name="add_biker" value="Add Warehouse">
                      </td>
                    </tr>
                  </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

<script >
  $(document).ready( function(){
    $('.delete-warehouse').on('click', function(){
      // var service_id = $(this).data('id');
      var warehouse_id = $(this).attr('data-id');

     
     if(confirm( 'Are you sure you want to delete the Warehouse?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/delete_warehouse') ?>",
          type : 'POST',
          data : {
            'warehouse_id' : warehouse_id
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/genre_list') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>