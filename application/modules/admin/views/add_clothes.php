<?php
	include ('header.php');

 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
           
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Clothes</h6>
                 
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <?php 
                        $insert_clothes_error = $this->session->flashdata('insert_clothes_error');
                        if( $insert_clothes_error ){
                     ?>
                        <div class="alert alert-dismissible alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Check!</h4>
                            <p class="mb-0"><?php echo $insert_clothes_error; ?></p>
                        </div>
                    <?php } 
                    ?>
                    <?php echo form_open_multipart('admin/add_clothes_to_db', array('id'=>'add-product-form') ); ?>
                      <table>
                        <tr>
                          <td><label>Cloth Name</label></td>
                          <td><input type="text" name="cloth_name" required></td>
                        </tr>
                        <tr>
                          <td><label>Cloth Type</label></td>
                          <td>
                            <select name="cloth_type">
                              <option value="1">Men</option>
                              <option value="2">Women</option>
                              <option value="3">House Hold</option>
                              <option value="4">Kids</option>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td><label>Service Type</label></td>
                          <td>
                            <select name="service_type">
                              <?php
                              foreach($service_details as $service_detail){
                              ?>
                              <option value="<?php echo  $service_detail['service_id']; ?>"><?php echo $service_detail['service_name']?></option>
                              <?php } ?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td><label>Price</label></td>
                          <td><input type="text" name="price" required></td>
                        </tr>
                        <tr>
                          <td><label>Time Of Service</label></td>
                          <td><input type="text" name="time" required>Hours</td> 
                        </tr>
                        <tr>
                          <td><labek> Icon </labek></td>
                          <td>
                            <select name="svg">
                                <option value="placeholder.svg">Select Icon From the list</option>
                                <option value="basket.svg">Basket</option>
                                <option value="bed_sheet.svg">Bed Sheet</option>  
                                <option value="blanket.svg">Blanket</option>
                                <option value="blazer.svg">Blazers </option>
                                <option value="boxer.svg">Boxers   </option>
                                <option value="cap.svg">Cap</option>
                                <option value="clean_cloth.svg">Clean Cloth</option>
                                <option value="curtain.svg">Curtain</option>
                                <option value="dress.svg">Dress</option>
                                <option value="dry_clean.svg">Dry Cleaning</option>
                                <option value="drying.svg">Drying Cloth</option>
                                <option value="hankerchief.svg">Hankerchief</option>
                                <option value="hoody.svg">Hoody</option>
                                <option value="iron.svg">Iron</option>
                                <option value="jacket.svg">Jacket</option>
                                <option value="ladies_pants.svg">Ladies Pants   </option>
                                <option value="bra.svg">Ladies Underwear</option>
                                <option value="leather_jacket.svg">Leather Jacket</option>
                                <option value="leggins.svg">Leggins </option>
                                <option value="long_skirt.svg">Long Skirt</option>
                                <option value="night_gown.svg">Ladies Night Gown  </option>
                                <option value="pants.svg">Pants</option>
                                <option value="pillow.svg">Pillow</option>
                                <option value="pyajama.jpg">Pyajamas    </option>
                                <option value="shirt.svg">Shirt</option>
                                <option value="skirt.svg">Skirt</option>
                                <option value="sofa.svg">Sofa </option>
                                <option value="socks.svg">Socks</option>
                                <option value="steam_wash.svg">Steam wash</option>
                                <option value="suit.svg">Suit</option>
                                <option value="sweater.svg">Sweater</option>
                                <option value="top.svg">Top</option>
                                <option value="towel.svg">Towel</option>
                                <option value="track_pants.svg">Track Pants</option>
                                <option value="tshirt.svg">Tshirt</option>
                                <option value="waistcoat.svg">Waistcoat</option>
                                <option value="wash.svg">Wash</option>
                                <option value="wash_iron.svg">Wash & Iron </option>
                                <option value="washing_machine.svg">Washing machine</option>
                          </select>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2"><input type="submit" name="add_cloth"></td>
                        </tr>
                      </table>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
          
          </div>
            <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Icon List</h6>
                </div>
               
                <div class="row" style="padding: 10px;">
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/wash.svg" style="height: 20px;"> <span style="padding-left: 10px;">Wash</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/iron.svg" style="height: 20px;"> <span style="padding-left: 10px;">Iron</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/wash_iron.svg" style="height: 20px;"> <span style="padding-left: 10px;">Wash & Iron</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/dry_clean.svg" style="height: 20px;"> <span style="padding-left: 10px;">Dry Cleaning</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/steam_wash.svg" style="height: 20px;"> <span style="padding-left: 10px;">Steam Wash</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/washing_machine.svg" style="height: 20px;"> <span style="padding-left: 10px;">Washing Machine</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/basket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Basket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/clean_cloth.svg" style="height: 20px;"> <span style="padding-left: 10px;">Clean Cloth</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/drying.svg" style="height: 20px;"> <span style="padding-left: 10px;">Drying Cloth</span></div>

               
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/shirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Shirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/tshirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Tshirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/pants.svg" style="height: 20px;"> <span style="padding-left: 10px;">Pants</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/ladies_pants.svg" style="height: 20px;"> <span style="padding-left: 10px;">Ladies Pants</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/track_pants.svg" style="height: 20px;"> <span style="padding-left: 10px;">Track Pants</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/boxer.svg" style="height: 20px;"> <span style="padding-left: 10px;">Boxers</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/pyajama.svg" style="height: 20px;"> <span style="padding-left: 10px;">Pyajamas</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/hankerchief.svg" style="height: 20px;"> <span style="padding-left: 10px;">Hankerchief</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/socks.svg" style="height: 20px;"> <span style="padding-left: 10px;">Socks</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/waistcoat.svg" style="height: 20px;"> <span style="padding-left: 10px;">Waistcoat</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/blazer.svg" style="height: 20px;"> <span style="padding-left: 10px;">Blazers</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/jacket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Jacket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/leather_jacket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Leather Jacket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/suit.svg" style="height: 20px;"> <span style="padding-left: 10px;">Suit</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/hoody.svg" style="height: 20px;"> <span style="padding-left: 10px;">Hoody</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/cap.svg" style="height: 20px;"> <span style="padding-left: 10px;">Cap</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/leggins.svg" style="height: 20px;"> <span style="padding-left: 10px;">Leggins</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/bra.svg" style="height: 20px;"> <span style="padding-left: 10px;">Ladies Underwear</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/night_gown.svg" style="height: 20px;"> <span style="padding-left: 10px;">Night Gown</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/dress.svg" style="height: 20px;"> <span style="padding-left: 10px;">Dress</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/skirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Skirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/long_skirt.svg" style="height: 20px;"> <span style="padding-left: 10px;">Long Skirt</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/sweater.svg" style="height: 20px;"> <span style="padding-left: 10px;">Sweater</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/top.svg" style="height: 20px;"> <span style="padding-left: 10px;">Top</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/bed_sheet.svg" style="height: 20px;"> <span style="padding-left: 10px;">Bed Sheet</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/blanket.svg" style="height: 20px;"> <span style="padding-left: 10px;">Blanket</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/curtain.svg" style="height: 20px;"> <span style="padding-left: 10px;">Curtain</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/pillow.svg" style="height: 20px;"> <span style="padding-left: 10px;">Pillow</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/sofa.svg" style="height: 20px;"> <span style="padding-left: 10px;">Sofa</span></div>
                    <div class="col-md-4" style="margin-bottom: 10px"> <img src="<?php echo $this->data['svg_path']; ?>/towel.svg" style="height: 20px;"> <span style="padding-left: 10px;">Towel</span></div>
                    
                   
                    
                </div>
                <div class="card-body">
                  <div class="chart-area">
                    

                  </div>
                </div>
              </div>
            </div>

           
          </div> <!-- Row -->


            </div>

          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>