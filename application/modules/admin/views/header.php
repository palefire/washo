<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Dashboard</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css') ?>">
 
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('admin/dashboard') ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Washo Admin </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item <?php if( $this->router->fetch_method() == 'dashboard' ){ echo 'active'; } ?>">
        <a class="nav-link" href="<?php echo base_url('admin/dashboard') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
          <div style="position:absolute;top:23px;right:50px;background:#fff;border-radius:50%;height:10px;width:10px"></div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Clothes
      </div>

    

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?php if( $this->router->fetch_method() == 'clothes' ){ echo 'active'; } ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          
          <i class="fas fa-fw fa-tshirt"></i>
          <span>Clothes Lists</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Lists</h6>
            <a class="collapse-item" href="<?php echo base_url('admin/men_clothes') ?>">Men </a>
            <a class="collapse-item" href="<?php echo base_url('admin/women_clothes') ?>">Women </a>
            <a class="collapse-item" href="<?php echo base_url('admin/household_clothes') ?>">Households </a>
            <a class="collapse-item" href="<?php echo base_url('admin/kids_clothes') ?>">Kids</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Add Clothes</h6>
            <a class="collapse-item" href="<?php echo base_url('admin/add_clothes') ?>" style="color: green">Add Clothes</a>
          </div>
        </div>
      </li>

       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/services'); ?>">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Services</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Orders
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item
        <?php 
          if( $this->router->fetch_method() == 'order_list' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_order' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'pending_order' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_pending' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'pickup_assigned' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_pickup_assigned' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'picked_up' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_picked_up' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'in_warehouse' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_in_warehouse' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'washed' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_washed' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'delivery_assigned' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_delivery_assigned' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'complete' ){ echo 'active'; } 
          if( $this->router->fetch_method() == 'view_complete' ){ echo 'active'; } 
          
          ?>

      ">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-shopping-cart"></i>
          <span>Orders</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Active Orders:</h6>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'order_list' OR $this->router->fetch_method() == 'view_order'){ echo 'active'; }   ?>" href="<?php echo base_url('admin/order_list'); ?>">Order List</a>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'pending_order' OR $this->router->fetch_method() == 'view_pending'){ echo 'active'; }   ?> " href="<?php echo base_url('admin/pending_order'); ?>">Pending</a>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'pickup_assigned' OR $this->router->fetch_method() == 'view_pickup_assigned'){ echo 'active'; }   ?>" href="<?php echo base_url('admin/pickup_assigned'); ?>">Pickup Assigned</a>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'picked_up' OR $this->router->fetch_method() == 'view_picked_up'){ echo 'active'; }   ?>" href="<?php echo base_url('admin/picked_up'); ?>">Picked Up Order</a>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'in_warehouse' OR $this->router->fetch_method() == 'view_in_warehouse'){ echo 'active'; }   ?>" href="<?php echo base_url('admin/in_warehouse')  ?>">Order with Warehouse</a>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'washed' OR $this->router->fetch_method() == 'view_washed'){ echo 'active'; }   ?>" href="<?php echo base_url('admin/washed')  ?>">Washed Clothes</a>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'delivery_assigned' OR $this->router->fetch_method() == 'view_delivery_assigned'){ echo 'active'; }   ?>"  href="<?php echo base_url('admin/delivery_assigned'); ?>">Delivery Assigned</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Inactive Orders</h6>
            <a class="collapse-item <?php if( $this->router->fetch_method() == 'complete' OR $this->router->fetch_method() == 'view_complete'){ echo 'active'; }   ?>" href="<?php echo base_url('admin/complete'); ?>">Completed Orders</a>

            
          </div>
        </div>
      </li>
       <hr class="sidebar-divider">
       <li class="nav-item <?php if( $this->router->fetch_method() == 'complain' ){ echo 'active'; } ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities1" aria-expanded="true" aria-controls="collapseUtilities">
          
          <i class="fas fa-fw fa-tshirt"></i>
          <span>Complaint Orders</span>
        </a>
        <div id="collapseUtilities1" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Lists</h6>
            <a class="collapse-item" href="<?php echo base_url('admin/complain') ?>">Pending </a>
            <a class="collapse-item" href="<?php echo base_url('admin/accepted_complain') ?>">Accepted </a>
            <a class="collapse-item" href="<?php echo base_url('admin/rejected_complain') ?>">Rejected </a>
        </div>
      </li>

     <hr class="sidebar-divider">
     <div class="sidebar-heading">
        Portals
      </div>
    


      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/franchise'); ?>">
          <i class="fas fa-fw fa-building"></i>
          <span>Franchise</span></a>
      </li>

     

       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/warehouse'); ?>">
          <i class="fas fa-fw fa-warehouse"></i>
          <span>Warehouse</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <div class="sidebar-heading">
        Extra
      </div>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('admin/feedback'); ?>">
          <i class="fas fa-fw fa-warehouse"></i>
          <span>Feedback</span></a>
      </li>
       <hr class="sidebar-divider d-none d-md-block">
      <div class="sidebar-heading">
        Dynamic Content
      </div>
      <li class="nav-item <?php if( $this->router->fetch_method() == 'about_us' ){ echo 'active'; } ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilitiess" aria-expanded="true" aria-controls="collapseUtilities">
          
          <i class="fas fa-fw fa-tshirt"></i>
          <span>Content</span>
        </a>
        <div id="collapseUtilitiess" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Dynamic</h6>
            <a class="collapse-item" href="<?php echo base_url('admin/about_us') ?>">About US </a>
            <a class="collapse-item" href="<?php echo base_url('admin/faq') ?>">FAQ </a>
            <a class="collapse-item" href="<?php echo base_url('admin/gallery') ?>">Gallery </a>
            <a class="collapse-item" href="<?php echo base_url('admin/offer') ?>">Offers</a>
            
        </div>
      </li>

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <!-- <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                
                <span class="badge badge-danger badge-counter">3+</span>
              </a> -->
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
            <!--   <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                
                <span class="badge badge-danger badge-counter">7</span>
              </a> -->
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Message Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                    <div class="small text-gray-500">Emily Fowler · 58m</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                    <div class="status-indicator"></div>
                  </div>
                  <div>
                    <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                    <div class="small text-gray-500">Jae Chun · 1d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                    <div class="status-indicator bg-warning"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                    <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                    <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div>
                    <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                    <div class="small text-gray-500">Chicken the Dog · 2w</div>
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"> <?php if(  isset( $this->session->userdata['admin_name'] )  ){ echo  $this->session->userdata['admin_name']; } ?></span>
                
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo base_url('admin/logout'); ?>" >
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->