<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($franchise_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Offers</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  
                    
                    <div class="row">
                      <?php if( $offer_details != NULL ){  ?>
                      <?php foreach( $offer_details as $offer ){  ?>
                      <div class="col-md-4">
                        <div class="gal-img" style="position:relative; width:100%; height:200px; border:1px solid blue; margin-top:20px">

                          <img src="<?php echo site_url('/uploads/offer/').$offer['offer_image']; ?>" style="position:absolute;top:0;left:0; width:100%;height:170px">
                          <span class="delete-offer" style="position:absolute; bottom:2px; color:red; left:50%; transform:translateX(-50%);cursor:pointer" data-id="<?php echo $offer['offers_id'] ?>" data-image="<?php echo $offer['offer_image'] ?>">Delete</span>
                        </div>
                      </div> 
                      <?php }  ?> 
                      <?php }  ?> 
                    </div>


                 
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Add Offers</h6>
                
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                  <?php 
                    $upload_error = $this->session->flashdata('upload_error');
                    $insert_error = $this->session->flashdata('insert_error');
                    $franchise_pin_exist = $this->session->flashdata('franchise_pin_exist');
                    if( $upload_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $upload_error; ?></p>
                    </div>
                  <?php } 
                  if( $insert_error ){
                  ?>
                   <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_error; ?></p>
                    </div>
                  <?php }
                   if( $franchise_pin_exist ){ ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $franchise_pin_exist; ?></p>
                    </div>
                  <?php } ?>
                  <?php echo form_open_multipart('admin/add_offer', array('id'=>'add-product-form') ); ?>
                  <table>
                    <tr>
                      <td><label>Title</label></td>
                      <td><input type="text" name="offer_title" required></td>
                    </tr>
                    <tr>
                      <td><label>Image</label></td>
                      <td><input type="file" name="offer_image" reqiured></td>
                    </tr>
                    

                   
                    <tr>
                      <td colspan="2">
                        <input type="submit" name="add_offer" value="Add Offer">
                      </td>
                    </tr>
                  </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

<script >
  $(document).ready( function(){
    $('.delete-offer').on('click', function(){
      // var service_id = $(this).data('id');
      var offers_id = $(this).attr('data-id');
      var offer_image = $(this).attr('data-image');

     
     if(confirm( 'Do You want to delete the Image?' )){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('admin/delete_offer') ?>",
          type : 'POST',
          data : {
            'offers_id' : offers_id,
            'offer_image' : offer_image
          },
          success: function( data ){
            console.log(data);
            // window.location = "<?php echo site_url('admin/delete_offer') ?>";
            location.reload();
          },
          error: function(response){
            console.log(response);
          }
        });//ajax
      }
      
      
    });
  } );
</script>