<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($service_details);
  // echo '</pre>';
  // echo $pagination;
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
           
          </div>



          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit Services</h6>
                  
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <?php 
                    $insert_service_error = $this->session->flashdata('insert_service_error');
                    $upload_error = $this->session->flashdata('upload_error');
                    if( $insert_service_error ){
                   ?>
                    <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $insert_service_error; ?></p>
                    </div>
                  <?php } 
                  if( $upload_error ){
                  ?>
                  <div class="alert alert-dismissible alert-warning">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <h4 class="alert-heading">Check!</h4>
                      <p class="mb-0"><?php echo $upload_error; ?></p>
                    </div>
                <?php  } ?>
                    <?php echo form_open_multipart('admin/edit_services_to_db', array('id'=>'add-product-form') ); ?>
                      <table>
                        <tr>
                          <td><label>Service Name</label></td>
                          <td>
                            <input type="text" name="service_name" value="<?php echo $service_details[0]['service_name'] ?>">
                            <input type="hidden" name="service_id" value="<?php echo $service_details[0]['service_id'] ?>">
                          </td>
                        </tr>
                        <tr>
                          <td><label>Change Image</label></td>
                           <td><img src="<?php echo site_url('/uploads/services/').$service_details[0]['img'] ?>" style="height: 30px;"></td>
                          <td>
                            <input type="File" name="service_img" >
                          </td>
                        </tr>
                        <tr>
                          <td><labek> Icon </labek></td>
                          <td>
                            <select name="svg">
                            <option value="placeholder.svg" <?php if( $service_details[0]['svg'] == 'placeholder.svg' ){ echo 'selected';} ?>>Select Icon From the list</option>
                            <option value="basket.svg" <?php if( $service_details[0]['svg'] == 'basket.svg' ){ echo 'selected';} ?>>Basket</option>
                            <option value="bed_sheet.svg" <?php if( $service_details[0]['svg'] == 'bed_sheet.svg' ){ echo 'selected';} ?>>Bed Sheet</option>  
                            <option value="blanket.svg" <?php if( $service_details[0]['svg'] == 'blanket.svg' ){ echo 'selected';} ?>>Blanket</option>
                            <option value="blazer.svg" <?php if( $service_details[0]['svg'] == 'blazer.svg' ){ echo 'selected';} ?>>Blazers </option>
                            <option value="boxer.svg" <?php if( $service_details[0]['svg'] == 'boxer.svg' ){ echo 'selected';} ?>>Boxers   </option>
                            <option value="cap.svg" <?php if( $service_details[0]['svg'] == 'cap.svg' ){ echo 'selected';} ?>>Cap</option>
                            <option value="clean_cloth.svg" <?php if( $service_details[0]['svg'] == 'clean_cloth.svg' ){ echo 'selected';} ?>>Clean Cloth</option>
                            <option value="curtain.svg"<?php if( $service_details[0]['svg'] == 'curtain.svg' ){ echo 'selected';} ?>>Curtain</option>
                            <option value="dress.svg"<?php if( $service_details[0]['svg'] == 'dress.svg' ){ echo 'selected';} ?>>Dress</option>
                            <option value="dry_clean.svg"<?php if( $service_details[0]['svg'] == 'dry_clean.svg' ){ echo 'selected';} ?>>Dry Cleaning</option>
                            <option value="drying.svg"<?php if( $service_details[0]['svg'] == 'drying.svg' ){ echo 'selected';} ?>>Drying Cloth</option>
                            <option value="hankerchief.svg"<?php if( $service_details[0]['svg'] == 'hankerchief.svg' ){ echo 'selected';} ?>>Hankerchief</option>
                            <option value="hoody.svg"<?php if( $service_details[0]['svg'] == 'hoody.svg' ){ echo 'selected';} ?>>Hoody</option>
                            <option value="iron.svg"<?php if( $service_details[0]['svg'] == 'iron.svg' ){ echo 'selected';} ?>>Iron</option>
                            <option value="jacket.svg"<?php if( $service_details[0]['svg'] == 'jacket.svg' ){ echo 'selected';} ?>>Jacket</option>
                            <option value="ladies_pants.svg"<?php if( $service_details[0]['svg'] == 'ladies_pants.svg' ){ echo 'selected';} ?>>Ladies Pants   </option>
                            <option value="bra.svg"<?php if( $service_details[0]['svg'] == 'bra.svg' ){ echo 'selected';} ?>>Ladies Underwear</option>
                            <option value="leather_jacket.svg"<?php if( $service_details[0]['svg'] == 'leather_jacket.svg' ){ echo 'selected';} ?>>Leather Jacket</option>
                            <option value="leggins.svg"<?php if( $service_details[0]['svg'] == 'leggins.svg' ){ echo 'selected';} ?>>Leggins </option>
                            <option value="long_skirt.svg"<?php if( $service_details[0]['svg'] == 'long_skirt.svg' ){ echo 'selected';} ?>>Long Skirt</option>
                            <option value="night_gown.svg"<?php if( $service_details[0]['svg'] == 'night_gown.svg' ){ echo 'selected';} ?>>Ladies Night Gown  </option>
                            <option value="pants.svg"<?php if( $service_details[0]['svg'] == 'pants.svg' ){ echo 'selected';} ?>>Pants</option>
                            <option value="pillow.svg"<?php if( $service_details[0]['svg'] == 'pillow.svg' ){ echo 'selected';} ?>>Pillow</option>
                            <option value="pyajama.jpg"<?php if( $service_details[0]['svg'] == 'pyajama.svg' ){ echo 'selected';} ?>>Pyajamas    </option>
                            <option value="shirt.svg" <?php if( $service_details[0]['svg'] == 'shirt.svg' ){ echo 'selected';} ?>>Shirt</option>
                            <option value="skirt.svg" <?php if( $service_details[0]['svg'] == 'skirt.svg' ){ echo 'selected';} ?>>Skirt</option>
                            <option value="sofa.svg" <?php if( $service_details[0]['svg'] == 'sofa.svg' ){ echo 'selected';} ?>>Sofa </option>
                            <option value="socks.svg" <?php if( $service_details[0]['svg'] == 'basket.svg' ){ echo 'selected';} ?>>Socks</option>
                            <option value="steam_wash.svg" <?php if( $service_details[0]['svg'] == 'steam_wash.svg' ){ echo 'selected';} ?>>Steam wash</option>
                            <option value="suit.svg" <?php if( $service_details[0]['svg'] == 'suit.svg' ){ echo 'selected';} ?>>Suit</option>
                            <option value="sweater.svg" <?php if( $service_details[0]['svg'] == 'sweater.svg' ){ echo 'selected';} ?>>Sweater</option>
                            <option value="top.svg" <?php if( $service_details[0]['svg'] == 'top.svg' ){ echo 'selected';} ?>>Top</option>
                            <option value="towel.svg" <?php if( $service_details[0]['svg'] == 'towel.svg' ){ echo 'selected';} ?>>Towel</option>
                            <option value="track_pants.svg" <?php if( $service_details[0]['svg'] == 'track_pants.svg' ){ echo 'selected';} ?>>Track Pants</option>
                            <option value="tshirt.svg" <?php if( $service_details[0]['svg'] == 'tshirt.svg' ){ echo 'selected';} ?>>Tshirt</option>
                            <option value="waistcoat.svg" <?php if( $service_details[0]['svg'] == 'waistcoat.svg' ){ echo 'selected';} ?>>Waistcoat</option>
                            <option value="wash.svg" <?php if( $service_details[0]['svg'] == 'wash.svg' ){ echo 'selected';} ?>>Wash</option>
                            <option value="wash_iron.svg" <?php if( $service_details[0]['svg'] == 'wash_iron.svg' ){ echo 'selected';} ?>>Wash & Iron </option>
                            <option value="washing_machine.svg" <?php if( $service_details[0]['svg'] == 'washing_machine.svg' ){ echo 'selected';} ?>>Washing machine</option>
                      </select>
                      </td>
                      
                    </tr>

                       
                        <tr>
                          <td colspan="2">
                            <input type="submit" name="edit_service" value="Edit Service">
                          </td>
                        </tr>
                      </table>
                      </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pie Chart -->
            
          </div>



            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>

