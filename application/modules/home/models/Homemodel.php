<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Homemodel extends CI_model {

    private $_table = 'dt_items'; /* TABLE NAME */
    private $_pk = 'id';   /* PRIMARY KEY NAME */

    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    /* ---------------- End Block ---------------- */

    /**
      ----------------------------------------------------------------
      BASIC FUNCTIONS FOR ALL MODELS
      ----------------------------------------------------------------
     * Starts
     * Here
     */

    /**
     * FETCH FUNCTION
     */
    public function fetch_record($per_page = NULL, $offset = 0, $cond = NULL, $order_by = NULL, $order = NULL, $row_only = FALSE, $count_only = FALSE, $return_object = FALSE, $debug = FALSE) {
        $condition = "'1' = '1' " . $cond;
        $order_by = ( $order_by == NULL || $order_by == "" ) ? $this->_pk : $order_by;
        $order = ( $order == NULL || $order == "" ) ? "ASC" : $order;

        $this->db->select($this->_table.".*, dt_stocks.quantity, dt_item_images.image");
        $this->db->join('dt_stocks', 'dt_stocks.item_id = ' . $this->_table . '.unique_id', 'inner');
        $this->db->join('dt_item_images', 'dt_item_images.item_id = ' . $this->_table . '.unique_id', 'inner');
        $this->db->where('dt_item_images.status', '1');
        $this->db->where('dt_stocks.status', '1');
        $this->db->from($this->_table);
        if ($condition <> "" || $condition <> NULL) {
            $this->db->where($condition);
        }
        $this->db->order_by($this->_table . '.' . $order_by, $order);
        $this->db->limit($per_page, $offset);
        $this->db->group_by('dt_item_images.item_id');
        $query = $this->db->get();

        if ($debug == TRUE) {
            die($this->db->last_query());
        }

        if ($return_object == TRUE) {
            if ($query->num_rows() == 0) {
                return NULL;
            } else {
                if ($count_only == TRUE) {
                    return $query->num_rows();
                } else if ($row_only == TRUE) {
                    return $query->row();
                } else {
                    return $query->result();
                }
            }
        } else {
            if ($query->num_rows() == 0) {
                return NULL;
            } else {
                if ($count_only == TRUE) {
                    return $query->num_rows();
                } else if ($row_only == TRUE) {
                    return $query->row_array();
                } else {
                    return $query->result_array();
                }
            }
        }
    }

    /* ----------------- END BLOCK ----------------- */

    /**
     * SAVE FUNCTION
     */
    public function save_record($data_array, $cond = NULL, $debug = FALSE, $_table = NULL) {
        if ($cond == NULL) {
            $this->db->insert($_table, $data_array);
            $id = $this->db->insert_id();
            return $id;
        } else {
            $condition = "'1' = '1' " . $cond;
            $this->db->select("*");
            $this->db->from($_table);
            if ($condition <> "" || $condition <> NULL) {
                $this->db->where($condition);
            }
            $query = $this->db->get();

            if ($debug == TRUE) {
                die($this->db->last_query());
            }

            if ($query->num_rows() == 0) {
                $this->db->insert($_table, $data_array);
                $id = $this->db->insert_id();
                return $id;
            } else {
                return NULL;
            }
        }
    }

    /* ----------------- END BLOCK ----------------- */

    /**
     * UPDATE FUNCTION
     */
    public function update_record($data_array, $cond = NULL, $debug = FALSE) {
        $pk_value = array_shift($data_array);

        if ($cond == NULL) {
            $this->db->where($this->_pk, $pk_value);
            $this->db->update($this->_table, $data_array);
            return $pk_value;
        } else {
            $condition = "'1' = '1' " . $cond;

            $this->db->select("*");
            $this->db->from($this->_table);
            if ($condition <> "" || $condition <> NULL) {
                $this->db->where($condition);
            }
            $this->db->where($this->_pk . "<>", $pk_value);
            $query = $this->db->get();

            if ($debug == TRUE) {
                die($this->db->last_query());
            }

            if ($query->num_rows() == 0) {
                $this->db->where($this->_pk, $pk_value);
                $this->db->update($this->_table, $data_array);
                return $pk_value;
            } else {
                return NULL;
            }
        }
    }

    /* ----------------- END BLOCK ----------------- */

    /**
     * STATUS FUNCTION
     */
    public function satus_record($row_id = NULL, $debug = FALSE) {
        $sql = "UPDATE `" . $this->_table . "` SET `status` = IF( `status` = '0', '1', '0' ) WHERE " . $this->_pk . " = '" . $row_id . "'";
        if ($debug == TRUE) {
            die($sql);
        }
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    /* ----------------- END BLOCK ----------------- */

    /**
     * DELETE FUNCTION
     */
    public function delete_record($row_id = NULL, $debug = FALSE) {
        $sql = "UPDATE `" . $this->_table . "` SET `status` = '5' WHERE " . $this->_pk . " = '" . $row_id . "'";
        if ($debug == TRUE) {
            die($sql);
        }
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    /* ----------------- END BLOCK ----------------- */

    /*
     * *******************************************************************************************************************************
     */

    /**
      ----------------------------------------------------------------
      CUSTOM FUNCTIONS FOR ALL MODELS
      ----------------------------------------------------------------
     * Starts
     * Here
     */
}
