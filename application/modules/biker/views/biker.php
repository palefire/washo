<?php
	include ('header.php');
 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard / Biker</h1>
            
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Pickups</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $no_of_completed_pickup ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-motorcycle fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Deliveries</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $no_of_completed_delivery ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-motorcycle fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Trips</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $total_completed ?></div>
                        </div>
                        <div class="col">
                          
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-motorcycle fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Assigned Job</h6>
                 
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    
                    <?php
                        $pickup_complete = $this->session->flashdata('pickup_complete');
                        $biker_failed = $this->session->flashdata('biker_failed');
                        $delivery_complete = $this->session->flashdata('delivery_complete');
                        
                        if( $pickup_complete ){
                      ?>
                          <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Great!</h4>
                            <p class="mb-0"><?php echo $pickup_complete; ?></p>
                          </div>
                      <?php
                        } 
                        if( $biker_failed ){
                      ?>
                      <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Great!</h4>
                            <p class="mb-0"><?php echo $biker_failed; ?></p>
                          </div>
                      <?php  } 
                        if( $delivery_complete ){
                      ?>
                      <div class="alert alert-dismissible alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4 class="alert-heading">Great!</h4>
                            <p class="mb-0"><?php echo $delivery_complete; ?></p>
                          </div>
                      <?php  } ?>
                        
                      <div class="row">

            
                        <div class="col-xl-6 col-lg-6">


                          <?php if( $no_of_pickup > 0 ){  ?>
                            <div class="col-lg-6 mb-4">
                              <div class="card bg-danger text-white shadow">
                                <div class="card-body">
                                 <a href="<?php echo base_url('biker/pickup'); ?>" style="color:#ffffff"> Pick Up Assigned !</a>
                                  <div class="text-white-50 small"><a href="<?php echo base_url('biker/pickup'); ?>" style="color:#ffffff">View</a></div>
                                </div>
                              </div>
                            </div>
                          <?php   }?>
                        </div>
                        <div class="col-xl-6 col-lg-6">

                          <?php if( $no_of_delivery > 0 ){  ?>
                            <div class="col-lg-6 mb-4">
                              <div class="card bg-info text-white shadow">
                                <div class="card-body">
                                   <a href="<?php echo base_url('biker/delivery'); ?>" style="color:#ffffff"> Delivery Assigned !</a>
                                  <div class="text-white-50 small"><a href="<?php echo base_url('biker/delivery'); ?>" style="color:#ffffff">View</a></div>
                                </div>
                              </div>
                            </div>
                          <?php   }?>


                        </div>
                      </div>










                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="row">
            
                
          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>