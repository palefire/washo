<?php
	include ('header.php');
  // echo '<pre>';
  // print_r($order_details);
  // echo '</pre>';

 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard /Pending Orders</h1>
            
          </div>

        

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
              
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                     <h6 class="m-0 font-weight-bold text-primary">Order List</h6>
                        <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Order Code </th>
                              <th scope="col">Address</th>
                              <th scope="col">Order Date</th>
                              <th scope="col">Action</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                           <?php foreach( $order_details as $order_detail ){ ?>
                            <tr>
                              <td><?php echo $order_detail[0]['order_code'] ?></td>
                              <td>
                                <?php
                                  echo $order_detail[0]['name'].'<br>'.$order_detail[0]['address'].'<br>'.$order_detail[0]['district'].$order_detail[0]['state'].'<br>Pin - '.$order_detail[0]['pin']. '<br>Phone - '.$order_detail[0]['phone']
                                ?>
                                  
                                </td>
                              <td><?php echo $order_detail[0]['order_date'] ?></td>
                              <td><a href="<?php echo base_url('biker/view_delivery/').$order_detail[0]['order_code']; ?>" class="btn btn-warning"> View</a></s></td>
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                      </div>

                  </div>
                </div>


              </div>
            </div>
            

          
          </div>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>