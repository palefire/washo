<?php 
if(isset($categories)){
	foreach ($categories as $value) {
		$category[$value['id']] = $value['name'];
	}
}
if(isset($categories)){
	$i = 0;
	foreach ($categories as $cat) {
		if($cat['parent_id'] == '0'){
			$category_carousel[$i]['name'] 			= $cat['name'];
			$category_carousel[$i]['back_image'] 	= $cat['back_image'] == '' ? '' : site_url('assets/pf-admin/uploads/category/' . $cat['back_image']);
			$category_carousel[$i]['encrypted_id'] 	= $cat['encrypted_id'];
			$i++;
		}
	}
}
?>    
    <!-- banner part start-->
	<section class="pf-home-slide">
		<div id="home-slide" class="carousel slide pf-carousel-slide" data-ride="carousel">
		  <!-- Indicators -->
			<ul class="carousel-indicators">
				<?php if(isset($banner) && count($banner) > 0){$i=0;?>
					<?php for($i = 0; $i < count($banner); $i++){ ?>
					    <li data-target="#home-slide" data-slide-to="<?php echo $i;?>" class="<?php echo $i == 0? 'active' : '';?>"></li>
					<?php }?>
				<?php }?>
		  </ul>
		  <!-- The slideshow -->
		  <div class="carousel-inner">
		  	<?php if(isset($banner) && count($banner) > 0){$i=0;?>
				<?php foreach($banner as $bnr){ ?>
				  	<div class="carousel-item <?php echo $i == 0? 'active' : '';?> pf-carousel-item" style="background-image: url(<?php echo site_url('/assets/images/banner/' . $bnr['image_name']); ?>);">
				    	<div class="pf-carousel-content">
					      	<h4>It has finally started...</h4>
					      	<h2><?php echo $bnr['banner_text'];?></h2>
					        <a href="<?php echo $bnr['banner_link'];?>">Explore Now</a>
				      	</div>
				    </div>
				<?php $i++;}?>
			<?php }?>
		  </div> 
		  <!-- Left and right controls -->
		  <a class="carousel-control-prev" href="#home-slide" data-slide="prev">
		  	<div class="carousel-control-content">
		    	<span class="carousel-control-prev-icon"></span>
		    </div>
		  </a>
		  <a class="carousel-control-next" href="#home-slide" data-slide="next">
		  	<div class="carousel-control-content">
		    	<span class="carousel-control-next-icon"></span>
		    </div>
		  </a>
		</div>
	</section>

	<section class="pf-latest-product">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="pf-latest-product-item">
						<h6>See More About US!</h6>
						<h1>Hello! Welcome To<br><span>My Flower Shop.</span></h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderi.</p>
						<a href="#">Latest Products</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="pf-latest-product-image">
						<img src="https://bouqs-production-weblinc.netdna-ssl.com/product_images/red-roses/Grand/5c43ecc2617070790f002681/detail.jpg?c=1547955394">
					</div>
				</div>
			
			</div>
		</div>
	</section>

	<!--<section class="pf-view-section">
		<div class="container">
			<div class="pf-view-section-content">
				<div class="pf-view-content-background">
					<h6>See More About US!</h6>
					<h1>Hello! Welcome<br> To<br><span>My Shop.</span></h1>
					<a href="#">View Youtube</a>
				</div>
				<div class="pf-view-content-youtube" style="background-image: url(https://ak7.picdn.net/shutterstock/videos/27050707/thumb/1.jpg);">
					<h1></h1>
				</div>
			</div>
		</div>
	</section>-->

	<section class="pf-type-flower">
		<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
		  <div class="carousel-inner">
		  	<?php if(isset($category_carousel)){ $j = 0;?>
		    	<?php while ($j < count($category_carousel)) { $i = 0;?>
		    		<div class="carousel-item <?php echo $j == 0 ? 'active' : '';?>">
						<div class="container">
							<div class="row">
								<?php while ($i < 3) {?>
									<?php if(!isset($category_carousel[$j])){ break;}?>
									<div class="col-md-4">
										<a href="<?php echo site_url('products/' . $category_carousel[$j]['encrypted_id']);?>" style="text-decoration: none; color: #ffffff;" onMouseOver="this.style.color='#ffad46'" onMouseOut="this.style.color='#ffffff'">
											<div class="pf-list-flower-card">
												<div style="background-image: url(<?php echo $category_carousel[$j]['back_image'];?>); background-size: cover;">
													<div  class="p-3" style="background-color: rgb(31, 40, 62, 0.5); height: 250px; text-transform: uppercase;">
														<div style="border: 1px solid #dedfe0; padding: 85px 0;">
															<p class="text-center"><b><?php echo $category_carousel[$j]['name'];?></b></p>
														</div>
													</div>
												</div>
											</div>
										</a>
									</div>
								<?php $i++;
								$j++;}?>
							</div>
						</div>
					</div>
				<?php }?>
			<?php }?>
		  </div>
		  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
		    <div style="background-color:#ee0106; padding:10px 10px 5px; border: 1px solid #000;">
		    	<span class="carousel-control-prev-icon"></span>
		    </div>
		  </a>
		  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
		    <div style="background-color:#ee0106; padding:10px 10px 5px; border: 1px solid #000;">
		    	<span class="carousel-control-next-icon"></span>
		    </div>
		  </a>
		</div>
	</section>
	<section class="pf-sale-soap">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="pf-sale-soap-content">
						<h4>Super Deal Only Today!</h4>
						<h1>Mega Sale Off<br><span>Upto 30%</span></h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud e Duis <br> reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui.</p>
						<a href="#">Shopping Now</a>
					</div>
				</div>
				<div class="col-md-5">
					<div class="pf-sale-soap-flower">
						<img src="https://res.cloudinary.com/bloomnation/c_pad,d_vendor:global:catalog:product:image.png,f_auto,fl_preserve_transparency,q_auto/v1555481146/vendor/5942/catalog/product/8/6/86a179c6c853d8daef647ac9c7e26731_2.jpg">
					</div>
				</div>
				<div class="col-md-2">
					
				</div>
			</div>
		</div>
	</section>

	<section class="pf-our-product">
		<div class="pf-list-flower">
			<div class="pf-list-flower-head">
				<div class="pf-our-product-title"><u>OUR PRODUCTS</u></div>
				<!-- <div class="pf-our-product-list">
					<ul>
						<li>Best Seller</li>
						<li>New Arrival</li>
						<li>Most Wanted</li>
					</ul>
				</div> -->
				<div class="container">
					<div class="row">
						<?php if(isset($lists) && count($lists) > 0){?>
							<?php foreach($lists as $flowers){ ?>
								<div class="col-md-3">
									<?php if(!empty($flowers['image']) && file_exists('./assets/pf-admin/uploads/items/' . $flowers['image'])){
										$path = site_url('/assets/pf-admin/uploads/items/') . $flowers['image'];
									}else{
										$path = site_url('/assets/pf-admin/images/') . 'icon.png';
									}?>
									<div class="pf-list-flower-card" style="background-image: url(<?php echo $path;?>)">
										<?php if($flowers['quantity'] == '0'){?>
											<div class="text-center" style="color: #db0706; margin-top: 45%; background-color: rgb(0, 0, 0, 0.7); padding: 10px; font-size: 20px;">
												<b>OUT OF STOCK</b>
											</div>
										<?php }else{?>
											<div class="pf-list-flower-desc">
												<div class="pf-list-flower-cart">
													<a href="#" id="add_to_cart" product="<?php echo $flowers['encrypted_id'];?>"><img src="<?php echo $image_path;?>img/svg/shopping-cart.svg"></a>
													
												</div>
												<div class="pf-list-flower-view">
													<a href="<?php echo site_url("all_flowers/single_product/") . $flowers['encrypted_id'];?>"><span><img src="<?php echo $image_path;?>img/svg/search.svg"> </span><span>Quick View</span></a>
												</div>
											</div>
										<?php }?>
									</div>
									<div class="pf-list-meta-desc">
										<div class="pf-list-flower-meta-category">
											<h3><?php echo isset($flowers['category_id']) ? $category[$flowers['category_id']]: '';?></h3>
										</div>
										<div class="pf-list-flower-meta-name">
											<h3><?php echo isset($flowers['name']) ? $flowers['name'] : '';?></h3>
											<p></p>
										</div>
										<div class="pf-list-flower-meta-price">
											<?php if($flowers['offer_till'] <> NULL && strtotime($flowers['offer_till']) >= time()){?>
												<h3><strike> &#8377; <?php echo isset($flowers['price']) ? number_format($flowers['price'], 2) : '00.00';?></strike> <span>  &#8377; <?php echo isset($flowers['offer_price']) ? number_format($flowers['offer_price'], 2) : '00.00';?></span></h3>
											<?php }else{?>
												<h3><span>  &#8377; <?php echo isset($flowers['price']) ? number_format($flowers['price'], 2) : '00.00';?></span></h3>
											<?php }?>
										</div>
									</div>
								</div>
							<?php }?>
						<?php }?>
					</div>
				</div> <!-- product list div  -->
			</div>
		</div>
	</section>

	<section class="pf-ullimited-delivery">
		<div class="container">
			<form>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
					    	<input type="text" class="form-control" id="name" name="name" placeholder="Name *" required oninvalid="this.setCustomValidity('Enter a name')" oninput="setCustomValidity('')">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
					    	<input type="email" class="form-control" id="email" name="email" placeholder="Emil ID *" required oninvalid="this.setCustomValidity('Enter an email')" oninput="setCustomValidity('')">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
					    	<textarea class="form-control" id="feedback" name="feedback" rows="5" placeholder="Write a message *" required oninvalid="this.setCustomValidity('Write a message')" oninput="setCustomValidity('')"></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<button type="submit" class="float-right" style="background-color: #db0706; padding: 10px 20px; color: #ffffff; border: none;" name="send">Send</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	