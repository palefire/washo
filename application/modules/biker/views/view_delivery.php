<?php
	include ('header.php');
  // print_r($bikers);
//Array ( [0] => Array ( [order_id] => 1 [order_code] => washo-20200228-1 [user_id] => 1 [name] => Shirt [service_id] => 1 [cloth_type] => 1 [franchise_id] => 1 [price_per_unit] => 20 [quantity] => 2 [total_price] => 40 [cloth_status] => 0 [payment_mode] => 1 [transaction_id] => [payment_status] => 1 [status] => 1 [order_date] => 2020-03-09 14:33:22 [delivery_date] => [pick_up_biker_id] => [warehouse_id] => [delivery_biker_id] => [created_by] => 1 [created_at] => 2020-03-03 12:33:24 [modified_by] => 1 [service_name] => Wash [service_slug] => wash-0 [svg] => wash.svg ) )
  $total_price = 0;
  foreach( $order_detail as $one_order ){
      $quantity = $one_order['quantity'];
      $price_per_unit = $one_order['price_per_unit'];
      $total_price = $total_price + ( intval( $quantity ) * $price_per_unit ); 
  }

 ?>


         <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard / Orders</h1>
            
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Order Code</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $order_detail[0]['order_code'] ?></div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Price</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $total_price ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Shipment Status</div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $this->data['order_cloth_status'][$order_detail[0]['cloth_status']] ?></div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Service</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $order_detail[0]['service_name'] ?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-8">
              <div class="card shadow mb-4">
              
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                    <h6 class="m-0 font-weight-bold text-primary">Order Detail</h6>
                       <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Cloth Name </th>
                              <th scope="col">Cloth Type</th>
                              <th scope="col">Quantity</th>
                              <th scope="col">Price</th>
                              <th scope="col">Payment Mode</th>
                              <th scope="col">Order Date</th>
                             
                              
                            </tr>
                          </thead>
                          <tbody>
                           <?php foreach( $order_detail as $order ){ ?>
                            <tr>
                              <td><?php echo $order['name'] ?></td>
                              <td><?php echo $this->data['order_cloth_type'][ $order['cloth_type'] ]  ?></td>
                              <td><?php echo $order['quantity'] ?></td>
                              <td><?php echo $order['price_per_unit'] ?></td>
                              <td>
                                <?php if( $order['payment_mode'] == 1 ){ ?>
                                  Cash On Delivery
                                <?php } ?>
                                <?php if( $order['payment_mode'] == 2 ){ ?>
                                  Online Payment
                                <?php } ?>
                              </td>
                              <td><?php echo $order['order_date'] ?></td>
                              
                            </tr>
                           <?php } ?>
                          </tbody>
                        </table>
                      </div>

                  </div>
                </div>


              </div>
            </div>
            <?php 
            if( $order_detail[0]['cloth_status'] == 5 ){
            ?>
            <div class="col-xl-4 col-md-4 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Action</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?php
                          $delivery_failed = $this->session->flashdata('delivery_failed');
                          $biker_failed = $this->session->flashdata('biker_failed');
                          
                          if( $delivery_failed ){
                        ?>
                            <div class="alert alert-dismissible alert-warning">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <h4 class="alert-heading">Sorry!</h4>
                              <p class="mb-0"><?php echo $delivery_failed; ?></p>
                            </div>
                        <?php
                          } 
                          if( $biker_failed ){
                        ?>
                        <div class="alert alert-dismissible alert-warning">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <h4 class="alert-heading">Sorry!</h4>
                              <p class="mb-0"><?php echo $biker_failed; ?></p>
                            </div>
                        <?php  } ?>
                          <?php echo form_open_multipart('biker/action/delivery_complete', array() ); ?> 
                            <fieldset>
                              <input type="hidden" name="order_code" value="<?php echo $order_detail[0]['order_code']?>">
                              
                              <div class="form-group">
                                <label for="exampleSelect1">Delivery Complete</label>
                              </div>
                               <div class="form-group">
                                <label for="exampleSelect1">Payment Mode</label>
                                <select name="payment_mode">
                                  <option value="1">Cash</option>
                                  <option value="2">Online</option>
                                </select>
                              </div>
                              <button type="submit" class="btn btn-success">Delivery Complete</button>
                            </fieldset>
                          </form>

                      </div>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div>

            <?php } ?>

          
          </div>

          

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php 
	include ('footer.php');
?>s