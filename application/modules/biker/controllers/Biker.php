<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biker extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        $this->load->model('franchise/ordermodel', 'Order');
        if( isset($this->session->userdata['biker_id']) ){
        	$q6 = $this->db->where( array('pick_up_biker_id'=>$this->session->userdata['biker_id'], 'cloth_status' => '1') )->get('pf_order');
	        $this->data['no_of_pickup'] = $q6->num_rows();

	        $q5 = $this->db->where( array('delivery_biker_id'=>$this->session->userdata['biker_id'], 'cloth_status' => '5') )->get('pf_order');
	        $this->data['no_of_delivery'] = $q5->num_rows();

	        $q4 = $this->db->where( array('pick_up_biker_id'=>$this->session->userdata['biker_id'], 'cloth_status' => '6') )->group_by("order_code")->get('pf_order');
	        $this->data['no_of_completed_pickup'] = $q4->num_rows();

	        $q3 = $this->db->where( array('delivery_biker_id'=>$this->session->userdata['biker_id'], 'cloth_status' => '6') )->group_by("order_code")->get('pf_order');
	        $this->data['no_of_completed_delivery'] = $q3->num_rows();

	        $this->data['total_completed'] = $q3->num_rows() + $q4->num_rows();

        }

	}//constructor

	/*
	|--------------------------------------------------------------------------
	| Index function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		if( isset($this->session->userdata['biker_id']) ){
			return redirect('biker/dashboard');
			
		}
		$this->load->view('login');
	}

	public function verify_login(){
		$username = $this->input->post('username');
		$password_decode = $this->input->post('password');
		$password = base64_encode( $password_decode );

		$q = $this->db->where( array('username'=>$username, 'password'=>$password) )->get('biker');
		if( $q->num_rows() ){
			$biker_details =  $q->row();
			// echo $admin_details->admin_id;
			//print_r($admin_details);
			$this->session->set_userdata( 'biker_id', $biker_details->biker_id );
			$this->session->set_userdata( 'biker_name', $biker_details->biker_name );
			$this->session->set_userdata( 'biker_number', $biker_details->biker_number );
			return redirect('biker/dashboard');
		}else{
			$this->session->set_flashdata( 'login_failed' , 'User Name or Password does not match!' );
			return redirect('biker');
		}
	}//function

	public function logout(){
		$this->session->unset_userdata('biker_id');
		$this->session->unset_userdata('biker_name');
		$this->session->unset_userdata('biker_number');
		
		return redirect('biker' );
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function dashboard(){
		if( ! $this->session->userdata['biker_id'] ){
			return redirect('biker');
			die();
		}
		$no_of_pickup = $this->data['no_of_pickup'];
		$no_of_delivery = $this->data['no_of_delivery'];
		$no_of_completed_pickup = $this->data['no_of_completed_pickup'];
		$no_of_completed_delivery = $this->data['no_of_completed_delivery'];
		$total_completed = $this->data['total_completed'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		// $query =  $this->db
		// ->select('*')
		// ->from('pf_order')
		// ->join('service','pf_order.service_id = service.service_id')
		// ->join('franchise','pf_order.franchise_id = franchise.franchise_id')
		// ->where( array( 'pick_up_biker_id' => $this->session->userdata['biker_id'], 'cloth_status' => '1' ) )
		// ->order_by('order_id','ASC')
		// ->get();
		// $order_details = $query->result_array();
		
		// $inner_loop = 0;
		// $outer_loop = -1;
		// $previous_order_code = '';
		// $array = array();
		// foreach ($order_details as $order_detail) {
		// 	$new_order_code = $order_detail['order_code'];

		// 	if( $new_order_code == $previous_order_code ){ 
		// 		$inner_loop++;
		// 		$previous_order_code = $order_detail['order_code'];
		// 	 }else{
		// 	 	$outer_loop++;
		// 	 	$inner_loop = 0;
		// 	 	$previous_order_code = $order_detail['order_code'];
		// 	 }

		// 	// echo $outer_loop;
		// 	// echo $inner_loop.'</br>';
		// 	$array[$outer_loop][$inner_loop] = $order_detail;


			
		// }
		$this->load->view('biker', array(
			'no_of_pickup'				=> $no_of_pickup,
			'no_of_delivery'			=> $no_of_delivery,
			'no_of_completed_pickup'	=> $no_of_completed_pickup,
			'no_of_completed_delivery'	=> $no_of_completed_delivery,
			'total_completed'			=> $total_completed,
		));
	}//function


	/*
	|--------------------------------------------------------------------------
	| Pickup function
	|--------------------------------------------------------------------------
	*/

	public function pickup(){
		if( ! $this->session->userdata['biker_id'] ){
			return redirect('biker');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_pickup = $this->data['no_of_pickup'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->join('franchise','pf_order.franchise_id = franchise.franchise_id')
		->join('delivery_address','pf_order.user_id = delivery_address.user_id')
		->where( array( 'pick_up_biker_id' => $this->session->userdata['biker_id'], 'cloth_status' => '1' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('pickup', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_pickup
			)
		);
	}//function

	public function view_pickup( $order_code ){
		$order_detail = $this->Order->get_order_by_code( $order_code );

		

		$this->load->view('view_pickup', array(
				'order_detail'	=>$order_detail,
			)
		);
		

	}//function

	/*
	|--------------------------------------------------------------------------
	| Delivery function
	|--------------------------------------------------------------------------
	*/

	public function delivery(){
		if( ! $this->session->userdata['biker_id'] ){
			return redirect('biker');
			die();
		}
		//$query1 = $this->db->get('service');
		$no_of_delivery = $this->data['no_of_delivery'];
		
		// $query =  $this->db->order_by('order_id','ASC')->limit($config['per_page'],$this->uri->segment(3))->get('pf_order');
		$query =  $this->db
		->select('*')
		->from('pf_order')
		->join('service','pf_order.service_id = service.service_id')
		->join('franchise','pf_order.franchise_id = franchise.franchise_id')
		->join('delivery_address','pf_order.user_id = delivery_address.user_id')
		->where( array( 'delivery_biker_id' => $this->session->userdata['biker_id'], 'cloth_status' => '5' ) )
		->order_by('order_id','ASC')
		->get();
		$order_details = $query->result_array();
		
		$inner_loop = 0;
		$outer_loop = -1;
		$previous_order_code = '';
		$array = array();
		foreach ($order_details as $order_detail) {
			$new_order_code = $order_detail['order_code'];

			if( $new_order_code == $previous_order_code ){ 
				$inner_loop++;
				$previous_order_code = $order_detail['order_code'];
			 }else{
			 	$outer_loop++;
			 	$inner_loop = 0;
			 	$previous_order_code = $order_detail['order_code'];
			 }

			// echo $outer_loop;
			// echo $inner_loop.'</br>';
			$array[$outer_loop][$inner_loop] = $order_detail;


			
		}


		$this->load->view('delivery', array(
				'order_details'=>$array,
				'no_of_order'	=> $no_of_delivery
			)
		);
	}//function

	public function view_delivery( $order_code ){
		$order_detail = $this->Order->get_order_by_code( $order_code );

		

		$this->load->view('view_delivery', array(
				'order_detail'	=>$order_detail,
			)
		);
		

	}//function

}//class