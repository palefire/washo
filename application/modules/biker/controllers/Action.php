<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends MY_Controller{
	function __construct(){
        parent::__construct();
    }//contructor

    public function pickup_complete(){
    	$order_code = $this->input->post('order_code');
    	$biker_id = $this->session->userdata['biker_id'];

    	$order_data = array(
    		'cloth_status' => '2',
    	);

    	$return = $this->db->update('pf_order', $order_data, array( 'order_code' => $order_code, 'pick_up_biker_id' => $this->session->userdata['biker_id'] ));
    	if( $return ){
			$this->session->set_flashdata( 'pickup_complete' , 'Pickup Completed Succesfully ! ' );
			return redirect("biker/dashboard");
    	}else{
    		$this->session->set_flashdata( 'pickup_failed' , 'Cannot Upadate the Order! Check After Some Time' );
    		return redirect("biker/view_pickup/$order_code");
    	}
    }//function

    public function delivery_complete(){
        $order_code = $this->input->post('order_code');
        $payment_mode = $this->input->post('payment_mode');
        $biker_id = $this->session->userdata['biker_id'];

        $order_data = array(
            'cloth_status' => '6',
            'payment_status' => '2',
            'payment_mode' => $payment_mode,
            'delivery_date' => date('Y-m-d H:i:s')
        );

       

        $return = $this->db->update('pf_order', $order_data, array( 'order_code' => $order_code, 'delivery_biker_id' => $this->session->userdata['biker_id'] ));
        if( $return ){
            $this->session->set_flashdata( 'delivery_complete' , 'Delivery Completed Succesfully ! ' );
            return redirect("biker/dashboard");
            
        }else{
            $this->session->set_flashdata( 'delivery_failed' , 'Cannot Upadate the Order! Check After Some Time' );
            return redirect("biker/view_delivery/$order_code");
        }
    }//function

}//class