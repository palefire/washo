<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Controller extends MX_Controller {

    public $data;

    function __construct() {
        parent:: __construct();
        date_default_timezone_set("Asia/Kolkata");

        $this->data['svg_path'] = site_url('assets/images/svg/');
        $this->data['image_path'] = site_url('assets/images/');
        $this->data['css_path'] = site_url('assets/css/');
        $this->data['js_path'] = site_url('assets/js/');
        $this->data['front_js_path'] = site_url('assets/front/js/');
        $this->data['front_css_path'] = site_url('assets/front/css/');
        $this->data['front_img_path'] = site_url('assets/front/img/');

        $this->data['fronts_js_path'] = site_url('assets/fronts/js/');
        $this->data['fronts_css_path'] = site_url('assets/fronts/css/');
        $this->data['fronts_img_path'] = site_url('assets/fronts/img/');

        $q1 = $this->db->get('service');
		$no_of_service = $q1->num_rows();
		$this->data['no_of_service'] = $no_of_service;

		$q2 = $this->db->get('clothes');
		$no_of_clothes = $q2->num_rows();
		$this->data['no_of_clothes'] = $no_of_clothes;

        //men clothes number
        $q2 = $this->db->where( array( 'cloth_type' => '1' ) )->get('clothes');
        $no_of_men_clothes = $q2->num_rows();
        $this->data['no_of_men_clothes'] = $no_of_men_clothes;

        $q3 = $this->db->where( array( 'cloth_type' => '2' ) )->get('clothes');
        $no_of_women_clothes = $q3->num_rows();
        $this->data['no_of_women_clothes'] = $no_of_women_clothes;

        $q4 = $this->db->where( array( 'cloth_type' => '3' ) )->get('clothes');
        $no_of_household_clothes = $q4->num_rows();
        $this->data['no_of_household_clothes'] = $no_of_household_clothes;

        $q4 = $this->db->where( array( 'cloth_type' => '4' ) )->get('clothes');
        $no_of_kids_clothes = $q4->num_rows();
        $this->data['no_of_kids_clothes'] = $no_of_kids_clothes;

        $q5 = $this->db->get('franchise');
        $no_of_franchise = $q5->num_rows();
        $this->data['no_of_franchise'] = $no_of_franchise;

        $q6 = $this->db->get('biker');
        $no_of_bikers = $q6->num_rows();
        $this->data['no_of_biker'] = $no_of_bikers;

        $q7 = $this->db->get('warehouse');
        $no_of_warehouse = $q7->num_rows();
        $this->data['no_of_warehouse'] = $no_of_warehouse;

      

        $this->data['order_cloth_status'] = array(
            'Pending', 
            'Cloth Assigned to Biker for Picku-up', 
            'Cloth With Franchise Before Wash',
            'Cloth Deliverd to Warehouse',
            'Cloth With Franchise After Wash',
            'Cloth Assigned to Biker for Delivery',
            'Cloth Deliverd Succesfully'
        );

        $this->data['order_cloth_type'] = array(
            'No Type Selected', 
            'Man',
            'Women',
            'household',
            'kids'
        );

        $this->data['cloth_type'] = array(
            '1' => 'Man',
            '2' => 'Women',
            '3' =>'household',
            '4' =>'kids'
        );


        if( isset($this->session->userdata['user_id']) ){
            $user_id = $this->session->userdata['user_id'];
            $q3 = $this->db->where( array('user_id'=>$user_id, 'status'=>'1' ) )->get('basket');
            $no_of_basket = $q3->num_rows();
            $this->data['no_of_basket'] = $no_of_basket;
        }

        $state = array(
            'Andhra Pradesh',                                              
            'Andaman and Nicobar Islands'   ,                                          
            'Arunachal Pradesh'   ,                               
            'Assam' ,                  
            'Bihar ' ,
            'Chandigarh',                     
            'Chhattisgarh',                      
            'Dadar and Nagar Haveli',                               
            'Daman and Diu',                     
            'Delhi ',            
            'Lakshadweep ' ,                 
            'Puducherry' ,               
            'Goa'   ,       
            'Gujarat',          
            'Haryana ',         
            'Himachal Pradesh',               
            'Jammu and Kashmir',
            'Jharkhand',                                     
            'Karnataka',                                    
            'Kerala',                 
            'Madhya Pradesh',                             
            'Maharashtra',                                   
            'Manipur',                              
            'Meghalaya',                               
            'Mizoram',                            
            'Nagaland',                            
            'Odisha',                         
            'Punjab',                        
            'Rajasthan',                          
            'Sikkim',                      
            'Tamil Nadu',                         
            'Telangana  ',                       
            'Tripura',                    
            'Uttar Pradesh',                          
            'Uttarakhand',                      
            'West Bengal',
        );
        $this->data['state'] = $state;

    }//constructor

    public function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

       return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

   
}